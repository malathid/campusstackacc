package CAMPS.Accounts;

import CAMPS.Connect.DBConnect;

/* @author malathi */
public class fin_subroutine {

    DBConnect con = new DBConnect();

    public String loadledger(int om_id, int parentId, String str, String rolename,int level) {
        String data = "", temp = "";
       try {
            con.getConnection();
            if (parentId == 0) {
                //if ((rolename.contains("Accounts Manager")) || (rolename.contains("CFO"))) {
                data = " <div class=\"body\"><div id=\"ledger_div\">";
                data += "<table class=\"table table-bordered table-striped table-hover dataTable js-exportable\"><thead><tr><th align=\"center\">Delete </th><th align=\"center\">Edit</th>";
                //}
                con.read("SELECT lch_name FROM accounts.ledger_cc_hierarchy WHERE `type`='ledger' AND STATUS>0 ORDER BY `level` DESC");
                while (con.rs.next()) {
                    data += "<th align=\"center\">" + con.rs.getString("lch_name") + "</th>";
                }
                data += "</tr></thead><tbody>";
            }
            temp = str;
            con.read("SELECT t1.lm_id,t1.acc_head,COUNT(t2.lm_id) COUNT FROM accounts.ledger_master AS t1 LEFT OUTER JOIN accounts.ledger_master t2 ON  t1.lm_id=t2.parent_id AND t2.status>0 WHERE t1.lm_id IN(SELECT lm_id FROM accounts.ledger_master WHERE parent_id=" + parentId + " AND om_id=" + om_id + " AND STATUS>0) AND t1.status>0 GROUP BY t1.lm_id ORDER BY t1.acc_head ASC");
            while (con.rs.next()) {
                String lel="";
                if (con.rs.getInt("count") == 0) {
                    for(int j=level;j<4;j++)
                        lel+="<td></td>";
                    //if ((rolename.contains("Accounting Manager")) || (rolename.contains("CFO"))) {
                    data += "<tr><td align=\"center\"><button type=\"button\" name=\"btnDelete\" class=\"btn btn-default waves-effect\" data-acc=\"ledger\" data-id=\"" + con.rs.getString("lm_id") + "\">\n"
                            + "                                    <i class=\"material-icons\" style=\"color: #ff4733; \">delete_forever</i>\n"
                            + "                                </button></td><td align=\"center\"><button type=\"button\" name=\"btnEdit\" class=\"btn btn-default waves-effect\" data-id=\"" + con.rs.getString("lm_id") + "\">\n"
                            + "                                    <i class=\"material-icons\" style=\"color:#8b9a00; \">note_add</i>\n"
                            + "                                </button></td>"+lel;
                    //  }
                    data += "<td>" + con.rs.getString("acc_head") + "</td>";
                    data += str +"</tr>";
                } else {
                    fin_subroutine ar = new fin_subroutine();
                    str = "<td>" + con.rs.getString("acc_head") + "</td>" + str;
                    data += ar.loadledger(om_id, con.rs.getInt("lm_id"), str, rolename,level+1);
                    str = temp;
                }
            }
            if (parentId == 0) {
                data += "</tbody></table>";
                data += "</div></div>";
            }
            con.closeConnection();
        } catch (Exception e) {
            System.out.print(e);
        }
        return data;
    }
    public String cost_center_table(int om_id, int parentId, String str, String rolename, int level) {
        String data = "", temp = "";
        try {
            con.getConnection();
            if (parentId == 0) {
                data = " <div class=\"body\"><div id=\"cc_div\">";
                data += "<table class=\"table table-bordered table-striped table-hover dataTable js-exportable\" id='datatab'><thead><tr><th align=\"center\">Cost Center Head </th>";
//                con.read("SELECT lch_name FROM accounts.ledger_cc_hierarchy WHERE `type`='costcenter' AND STATUS>0 ORDER BY `level` DESC");
//                while (con.rs.next()) {
//                    data += "<th align=\"center\">" + con.rs.getString("lch_name") + "</th>";
//                }
                data += "<th>Cost Center</th><th>Delete</th></tr></thead><tbody>";
            }
            //temp = str;
            //con.read("SELECT t1.ccm_id,t1.ccm_name,COUNT(t2.ccm_id) COUNT FROM accounts.cc_master AS t1 LEFT OUTER JOIN accounts.cc_master t2 ON  t1.ccm_id=t2.parent_id AND t2.status>0 WHERE t1.ccm_id IN(SELECT ccm_id FROM accounts.cc_master WHERE parent_id=" + parentId + " AND om_id=" + om_id + " AND STATUS>0) AND t1.status>0 GROUP BY t1.ccm_id");
            con.read("SELECT  pd.parent_id,cc.`ccm_name` AS parentname, GROUP_CONCAT(CONCAT(pd.ccm_id,':',REPLACE(pd.ccm_name,',',';'))) child FROM (SELECT ccm_id,ccm_name,parent_id,`status` FROM accounts.cc_master ccm  ORDER BY parent_id, ccm_id) pd JOIN (SELECT @pv:='1' FROM accounts.cc_master WHERE ccm_id='1' AND STATUS>0) ini JOIN accounts.cc_master cc ON cc.ccm_id=pd.parent_id AND cc.`status` >0  WHERE FIND_IN_SET(pd.parent_id, @pv) AND LENGTH(@pv := CONCAT(@pv, ',', pd.ccm_id)) AND pd.status>0 GROUP BY pd.parent_id");
            while (con.rs.next()) {

                //  data += "<tr class=\"group\" style=\"background-color: #cce9f0;\"><td colspan=\"2\">"+ con.rs.getString("parentname") +"</td></tr>" ;
                //  data += "<tr style=\"background-color: #cce9f0;\"><td>"+ con.rs.getString("parentname") +"</td><td></td></tr>" ;
                String[] ch = con.rs.getString("child").split(",");
                for (int i = 0; i < ch.length; i++) {
                    String[] chdat = ch[i].split(":");
                    data += "<tr><td>" + con.rs.getString("parentname") + "</td><td>" + chdat[1] + "</td>";
                    data += "<td align=\"center\"><button type=\"button\" name=\"btnDelete\" class=\"btn btn-default waves-effect\" data-acc=\"cc\" data-id=\"" + chdat[0] + "\">\n"
                            + "<i class=\"material-icons\" style=\"color: #ff4733 ; \">delete_forever</i>\n"
                            + "</button></td></tr>";
                }

//                if (con.rs.getInt("count") == 0) {
//                    //if ((rolename.contains("Accounting Manager")) || (rolename.contains("CFO"))) {
//                    data += "<tr><td align=\"center\"><button type=\"button\" name=\"btnDelete\" class=\"btn btn-default waves-effect\" data-acc=\"cc\" data-id=\"" + con.rs.getString("ccm_id") + "\">\n"
//                            + "<i class=\"material-icons\" style=\"color: #ff4733 ; \">delete_forever</i>\n"
//                            + "</button></td>";
                // }
//                    if (level == 1) {
//                        data += "<td>--</td><td>" + con.rs.getString("ccm_name") + "</td>";
//                    } else if (level == 0) {
//                        data += "<td>--</td><td>--</td><td>" + con.rs.getString("ccm_name") + "</td>";
//                    } else {
//                        data += "<td>" + con.rs.getString("ccm_name") + "</td>";
//                    }
//                    data += str + "</tr>";
//                } else {
//                    fin_subroutine fs = new fin_subroutine();
//                    str = "<td>" + con.rs.getString("ccm_name") + "</td>" + str;
//                    data += fs.cost_center_table(om_id, con.rs.getInt("ccm_id"), str, rolename, level + 1);
//                    str = temp;
//                }
            }

//            if (parentId == 0) {
            data += "</tbody></table></div></div>";
//            }
            con.closeConnection();
        } catch (Exception e) {
            System.out.print(e);
        }
        return data;
    }
    public String cost_center_journal(int om_id, int parentId, String str, String rolename, int level) {
        String data = "",optdata="";
        try {
            con.getConnection();
            if (parentId == 0) {
                data = " <div class=\"body\"><div class=\"col-xs-12 col-sm-12 align-left \"><div id=\"cc_div\" class=\"col-xs-12 col-sm-10 align-left \">";
               // data += "<table id='cctable' class=\"table table-bordered table-striped table-hover\"><thead><tr>";
//                con.read("SELECT lch_name FROM accounts.ledger_cc_hierarchy WHERE `type`='costcenter' AND STATUS>0 ORDER BY `level` DESC");
//                while (con.rs.next()) {
//                    data += "<th align=\"center\">" + con.rs.getString("lch_name") + "</th>";
//                }
              //  data += "<th>Cost Center</th><th>Amount</th></tr></thead><tbody>";
            }
            //temp = str;
            //con.read("SELECT t1.ccm_id,t1.ccm_name,COUNT(t2.ccm_id) COUNT FROM accounts.cc_master AS t1 LEFT OUTER JOIN accounts.cc_master t2 ON  t1.ccm_id=t2.parent_id AND t2.status>0 WHERE t1.ccm_id IN(SELECT ccm_id FROM accounts.cc_master WHERE parent_id=" + parentId + " AND om_id=" + om_id + " AND STATUS>0) AND t1.status>0 GROUP BY t1.ccm_id");
            con.read("SELECT  pd.parent_id,cc.`ccm_name` AS parentname, GROUP_CONCAT(CONCAT(pd.ccm_id,':',REPLACE(pd.ccm_name,',',';'))) child FROM (SELECT ccm_id,ccm_name,parent_id,`status` FROM accounts.cc_master ccm  ORDER BY parent_id, ccm_id) pd JOIN (SELECT @pv:='1' FROM accounts.cc_master WHERE ccm_id='1' AND STATUS>0) ini JOIN accounts.cc_master cc ON cc.ccm_id=pd.parent_id AND cc.`status` >0  WHERE FIND_IN_SET(pd.parent_id, @pv) AND LENGTH(@pv := CONCAT(@pv, ',', pd.ccm_id)) AND pd.status>0 GROUP BY pd.parent_id");
            while (con.rs.next()) {

                //  data += "<tr class=\"group\" style=\"background-color: #cce9f0;\"><td colspan=\"2\">"+ con.rs.getString("parentname") +"</td></tr>" ;
                //  data += "<tr style=\"background-color: #cce9f0;\"><td>"+ con.rs.getString("parentname") +"</td><td></td></tr>" ;
                String[] ch = con.rs.getString("child").split(",");
                for (int i = 0; i < ch.length; i++) {
                    String[] chdat = ch[i].split(":");
                    optdata+="<option value='"+chdat[0]+"'>"+con.rs.getString("parentname")+"::"+ chdat[1]+"</option>";
                    //data += "<tr><td>" + con.rs.getString("parentname") + "</td><td>" + chdat[1] + "</td>";
                    
                }
                 
//                if (con.rs.getInt("count") == 0) {
//                    //if ((rolename.contains("Accounting Manager")) || (rolename.contains("CFO"))) {
//                    data += "<tr><td align=\"center\"><button type=\"button\" name=\"btnDelete\" class=\"btn btn-default waves-effect\" data-acc=\"cc\" data-id=\"" + con.rs.getString("ccm_id") + "\">\n"
//                            + "<i class=\"material-icons\" style=\"color: #ff4733 ; \">delete_forever</i>\n"
//                            + "</button></td>";
                // }
//                    if (level == 1) {
//                        data += "<td>--</td><td>" + con.rs.getString("ccm_name") + "</td>";
//                    } else if (level == 0) {
//                        data += "<td>--</td><td>--</td><td>" + con.rs.getString("ccm_name") + "</td>";
//                    } else {
//                        data += "<td>" + con.rs.getString("ccm_name") + "</td>";
//                    }
//                    data += str + "</tr>";
//                } else {
//                    fin_subroutine fs = new fin_subroutine();
//                    str = "<td>" + con.rs.getString("ccm_name") + "</td>" + str;
//                    data += fs.cost_center_table(om_id, con.rs.getInt("ccm_id"), str, rolename, level + 1);
//                    str = temp;
//                }
            }
      //data += "<tr id=\"1\"><td>";
             data+= "<select data-container=\"body\" class=\"form-control show-tick selectpicker\" multiple  data-style=\"btn-col-teal-bg\" data-actions-box=\"true\" data-live-search=\"true\" id=\"cc_select\" name=\"cc_select\" ><option  style=\"display:none\"></option>"+optdata+"</select>";
             data+="</div><div class=\"col-xs-12 col-sm-2 align-left\"><button type=\"button\" name=\"ccReset\" class=\"btn btn-default waves-effect\" onclick=\"load_btn(1)\">"
                    + "<i class=\"material-icons\" style=\"color: #ff4733;font-size:15px !important; \">clear</i> Reset </button></td></div></div>"
                     + "<div class=\"vspace1em\"></div><div class=\"body\">" +
"                            <table id='cctable' class=\"table table-bordered table-striped table-hover\"><thead><tr><th width=\"60%\">Cost Center</th><th width=\"30%\">Amount</th><th width=\"10%\">Delete</th></tr></thead>\n" +
"                                <tbody><tr><td></td><td></td><td></td></tr></tbody>\n" +
"                                <tfoot><tr><th style='align-text:right;'>Total</th><th><input type=\"text\" class=\"form-control currency check\" style=\"width:100%;border: 1px solid #9c27b0;text-align:right;\" readonly data-v-min=\"0\" data-m-dec=\"2\"  data-d-group=\"2\" style='text-align:right;' name=\"cc_jour_tot\" id=\"cc_jour_tot\" value=\"\"></th><th></th></tr></tfoot></table>\n" +
"                        </div>";
// data+= "</td><td align=\"center\"><input type=\"text\" class=\"form-control currency check\" data-v-min=\"0\" data-m-dec=\"2\"  data-d-group=\"2\" style='text-align:right;' name=\"cc_jour[]\" value=\"\"></td></tr>";
        //    data += "<tr id=\"1\"><td><select class=\"form-control show-tick selectpicker \" data-container=\"body\" data-style=\"btn-col-teal-bg\" data-actions-box=\"true\" data-live-search=\"true\" data-id=\"1\" id=\"cc_select_1\" name=\"cc_select\"\">"+optdata+"</select></td><td align=\"center\"><input type=\"text\" class=\"form-control currency check\" data-v-min=\"0\" data-m-dec=\"2\"  data-d-group=\"2\" style='text-align:right;' name=\"cc_jour[]\" value=\"\"></td></tr>";
//   data += "<tr id=\"-100\"><td><select onchange=\"load_cc(-100)\">"+optdata+"</select></td><td align=\"center\"><input type=\"text\" class=\"form-control currency check\" data-v-min=\"0\" data-m-dec=\"2\"  data-d-group=\"2\" style='text-align:right;' name=\"cc_jour[]\" value=\"\"></td></tr>";
//            if (parentId == 0) {
           // data += "</tbody><tfoot><tr><th style='align-text:right;'>Total</th><th><input type=\"text\" class=\"form-control currency check\" data-v-min=\"0\" data-m-dec=\"2\"  data-d-group=\"2\" style='text-align:right;' name=\"cc_jour_tot\" id=\"cc_jour_tot\" value=\"\"></th></tr></tfoot></table></div></div>";
//            }
            con.closeConnection();
        } catch (Exception e) {
            System.out.print(e);
        }
        return data;
    }

    public String tt_staff_table(int om_id, String rolename) {
        String data = "";
        try {
            con.getConnection();
            data = "<div class=\"body\"><div id=\"ttuser_div\">";
            data += "<table class=\"table table-bordered table-striped table-hover dataTable js-exportable\"><thead><tr><th align=\"center\">Delete </th><th align=\"center\">User</th><th align=\"center\">Journal</th></tr></thead><tbody>";
            //con.read("SELECT spv.staffid,spv.staffname,tt.tt_desc,stm.stm_id FROM accounts.staff_tt_mapping stm INNER JOIN camps.staff_personal_view spv ON spv.staff_id=stm.staff_id INNER JOIN accounts.transaction_type tt ON stm.tt_id=tt.tt_id WHERE stm.status=1 AND tt.status=1 AND spv.status='WORKING' AND stm.om_id=" + om_id + " ORDER BY stm.staff_id,stm.tt_id");
            con.read("SELECT sm.staff_id,CONCAT(sm.first_name,' ',sm.`middle_name`,' ',sm.last_name) staffname,tt.tt_desc,stm.stm_id FROM accounts.staff_tt_mapping stm INNER JOIN camps.`staff_master` sm ON sm.staff_id=stm.staff_id INNER JOIN accounts.transaction_type tt ON stm.tt_id=tt.tt_id WHERE stm.status=1 AND tt.status=1 AND sm.working_status='working' AND stm.om_id=" + om_id + " ORDER BY stm.staff_id,stm.tt_id");
            while (con.rs.next()) {
                data += "<tr><td align=\"center\"><button type=\"button\" name=\"btnDelete\" class=\"btn btn-default waves-effect\" data-acc=\"tt\" data-id=\"" + con.rs.getString("stm_id") + "\">\n"
                        + "<i class=\"material-icons\" style=\"color: #ff4733 ; \">delete_forever</i></button></td>";
                data += "<td>" + con.rs.getString("staff_id") + " - " + con.rs.getString("staffname") + "</td><td>" + con.rs.getString("tt_desc") + "</td></tr>";
                //if ((rolename.contains("Accounting Manager")) || (rolename.contains("CFO"))) {

                // }
            }
            data += "</tbody></table></div></div>";
            con.closeConnection();
        } catch (Exception e) {
            System.out.print(e);
        }
        return data;
    }

    public String loadledger_tt_map1(int om_id, String rolename, int tt) {
        String data = "", debit = "", credit = "";
        try {
            con.getConnection();
            con.read("SELECT DISTINCT otlm.otlm_id,tt.tt_desc,otlm.lm_id,sm.staff_id,CONCAT(sm.first_name,' ',sm.`middle_name`,' ',sm.last_name) staffname,otlm.type,lm.acc_head FROM accounts.om_tt_lm_mapping otlm JOIN accounts.transaction_type tt ON tt.tt_id=otlm.tt_id AND otlm.status>0 AND tt.tt_id=" + tt + " AND tt.status>0 LEFT JOIN camps.staff_master sm ON sm.staff_id= otlm.staff_id JOIN accounts.ledger_master lm ON lm.lm_id=otlm.lm_id AND lm.status>0 WHERE otlm.om_id=" + om_id + "  ORDER BY lm.acc_head");
            while (con.rs.next()) {
                if (con.rs.getString("type").equals("debit")) {
                    debit += "<tr><td>" + con.rs.getString("acc_head");
                    if (con.rs.getInt("staff_id") != 0) {
                        debit += " -- (" + con.rs.getString("staffname") + "  -- " + con.rs.getString("staff_id") + ")</td>";
                    }
                    // if ((rolename.contains("Accounting Manager")) || (rolename.contains("CFO"))) {
                    debit += "<td><button type=\"button\" name=\"btnDelete\" class=\"btn btn-default waves-effect\" data-acc=\"jl\" data-id=\"" + con.rs.getString("otlm_id") + "\">\n"
                            + "                                    <i class=\"material-icons\" style=\"color: #ff4733; \">delete_forever</i>\n"
                            + "                                </button></td></tr>";
                } else if (con.rs.getString("type").equals("credit")) {
                    credit += "<tr><td>" + con.rs.getString("acc_head");
                    if (con.rs.getInt("staff_id") != 0) {
                        credit += " -- (" + con.rs.getString("staffname") + "  -- " + con.rs.getString("staff_id") + ")</td>";
                    }
                    // if ((rolename.contains("Accounting Manager")) || (rolename.contains("CFO"))) {
                    credit += "<td><button type=\"button\" name=\"btnDelete\" class=\"btn btn-default waves-effect\" data-acc=\"jl\" data-id=\"" + con.rs.getString("otlm_id") + "\">\n"
                            + "                                    <i class=\"material-icons\" style=\"color: #ff4733 ; \">delete_forever</i>\n"
                            + "                                </button></td></tr>";
                } else if (con.rs.getString("type").equals("both")) {
                    credit += "<tr><td>" + con.rs.getString("acc_head") + "</td>";
                    if (con.rs.getInt("staff_id") != 0) {
                        credit += " -- (" + con.rs.getString("staffname") + "  -- " + con.rs.getString("staff_id") + ")</td>";
                    }
                    //  if ((rolename.contains("Accounting Manager")) || (rolename.contains("CFO"))) {
                    credit += "<td><button type=\"button\" name=\"btnDelete\" class=\"btn btn-default waves-effect\" data-acc=\"jl\" data-id=\"" + con.rs.getString("otlm_id") + "\">\n"
                            + "                                    <i class=\"material-icons\" style=\"color: #ff4733; \">delete_forever</i>\n"
                            + "                                </button></td></tr>";
                    //   }

                    debit += "<tr><td>" + con.rs.getString("acc_head") + "</td>";
                    if (con.rs.getInt("staff_id") != 0) {
                        debit += " -- (" + con.rs.getString("staffname") + "  -- " + con.rs.getString("staff_id") + ")</td>";
                    }
                    //  if ((rolename.contains("Accounting Manager")) || (rolename.contains("CFO"))) {
                    debit += "<td><button type=\"button\" name=\"btnDelete\" class=\"btn btn-default waves-effect\" data-acc=\"jl\" data-id=\"" + con.rs.getString("otlm_id") + "\">\n"
                            + "                                    <i class=\"material-icons\" style=\"color: #ff4733 ; \">delete_forever</i>\n"
                            + "                                </button></td></tr>";
                }
            }
            data += " <div class=\"col-xs-12 col-sm-12 align-center \" ><div class=\"col-xs-12 col-sm-6 align-left \" >\n";
            data += "<table class=\"table table-bordered table-striped table-hover dataTable js-exportable\"><thead><tr><th align=\"center\">Debit </th><th align=\"center\">Delete</th></thead><tbody>" + debit + "</tbody></table></div>";
            data += "<div class=\"col-xs-12 col-sm-6 align-left\">";
            data += "<table class=\"table table-bordered table-striped table-hover dataTable js-exportable\"><thead><tr><th align=\"center\">Credit </th><th align=\"center\">Delete</th></thead><tbody>" + credit + "</tbody></table></div></div>";
            con.closeConnection();
        } catch (Exception e) {
            System.out.print(e);
        }
        return data;
    }

    public String loadledger_cc(int om_id, String rolename) {
        String data = "", temp = "";
        try {
            con.getConnection();
           data = "<div class=\"body\"><div id=\"ttuser_div\">";
            data += "<table class=\"table table-bordered table-striped table-hover dataTable js-exportable\"><thead><tr><th align=\"center\">Ledger </th><th align=\"center\">Cost Center - Staff</th></tr></thead><tbody>";
            
//            con.read1("SELECT lm_id FROM accounts.ledger_master WHERE cc=1 AND om_id=" + om_id + " AND STATUS=1");
//            while (con.rs1.next()) {
//                con.read("SELECT GROUP_CONCAT('<td>',acc_head,'</td>' ORDER BY T1.lvl DESC SEPARATOR '') val,lm_id FROM (SELECT @current AS current,(SELECT @current := parent_id FROM accounts.ledger_master WHERE lm_id = current) AS parent_id,@level := @level + 1 AS lvl FROM (SELECT @current := " + con.rs1.getString("lm_id") + ", @level := 0) a,accounts.ledger_master m WHERE @current <> 0) T1 JOIN accounts.ledger_master T2 ON T1.current = T2.lm_id ORDER BY T1.lvl DESC;");
//                while (con.rs.next()) {
//                    data += "<tr>" + con.rs.getString("val");
//                    if ((rolename.contains("Accounting Manager")) || (rolename.contains("CFO"))) {
//                        data += "<td><img src=\"../../Images/delete.png\" onclick=\"delete_staff_tt(" + con.rs.getString("lm_id") + ")\"></td>";
//                    }
//                    data += "</tr>";
//                }
//            }
            //    con.read1("SELECT DISTINCT oclm_id,oclm.ccm_id,cc.ccm_name,lm.acc_head,oclm.lm_id,oclm.staff_id,spv.staffname FROM accounts.om_cc_lm_mapping oclm JOIN accounts.`cc_master` cc ON cc.ccm_id=oclm.ccm_id AND oclm.status>0 AND cc.status>0 JOIN camps.staff_personal_view spv ON spv.staff_id= oclm.staff_id JOIN accounts.`ledger_master` lm ON lm.lm_id=oclm.lm_id AND lm.status>0 WHERE oclm.om_id=" + om_id + " ORDER BY oclm.oclm_id");
            con.read("SET SESSION group_concat_max_len = 1000000000");
            con.read1("SELECT b.oclm_id,b.lm_id,b.acc_head,CONCAT('<p>',b.res,'</p>') details FROM (SELECT a.oclm_id,a.lm_id,a.acc_head, GROUP_CONCAT(IF(a.staff_id!=0,CONCAT('<strong style=\"color: #09196a; \">',a.ccm_name,'</strong>'),CONCAT('<strong style=\"color: #09196a;\">',a.ccm_name,'</strong>','<button type=\"button\" name=\"btnDelete\" style=\"outline: none;border: 0;box-shadow: none; background-color: transparent;\" data-acc=\"lcc\" data-id=\"',a.oclm_id,'\"><i class=\"material-icons\" style=\"color: #ff4733;\">delete_forever</i></button>')),IF(a.staff_id!=0,CONCAT('<p>',a.staff,'</p>'),a.staff) SEPARATOR '') res FROM (SELECT oclm.oclm_id,oclm.lm_id,lm.`acc_head`, oclm.`ccm_id`,cc.`ccm_name`,oclm.staff_id,IF(oclm.staff_id!=0,GROUP_CONCAT(oclm.`staff_id`,'-',spv.`first_name`,'<button type=\"button\" name=\"btnDelete\" style=\" outline: none;border: 0;box-shadow: none;background-color: transparent;\" data-acc=\"lcc\" data-id=\"',oclm.oclm_id,'\"><i class=\"material-icons\" style=\"color: #ff4733 ; \">delete_forever</i></button>' SEPARATOR ''),'') staff FROM `accounts`.`om_cc_lm_mapping` oclm INNER JOIN accounts.cc_master cc ON cc.ccm_id = oclm.ccm_id  INNER JOIN accounts.ledger_master lm ON lm.lm_id = oclm.lm_id LEFT JOIN camps.staff_master spv ON spv.staff_id = oclm.staff_id WHERE oclm.`status`>0 AND oclm.om_id = "+om_id+" AND cc.status > 0 GROUP BY oclm.lm_id,oclm.ccm_id)a GROUP BY a.lm_id)b ORDER BY b.acc_head");
            while (con.rs1.next()) {
                data += "<tr><td>" + con.rs1.getString("acc_head")+"</td>";
//                    if(con.rs1.getInt("staff_id")!=0){
                data += "<td>" + con.rs1.getString("details") + "</td></tr>";
//                    }
//                    if ((rolename.contains("Accounting Manager")) || (rolename.contains("CFO"))) {
//                        data += "<td>&nbsp;&nbsp;&nbsp;<img src=\"../../Images/delete.png\" onclick=\"delete_oclm(" + con.rs1.getString("oclm_id") + ")\">";
//                    }    
            }
            data += "</tbody></table></div></div>";
            con.closeConnection();
        } catch (Exception e) {
            System.out.print(e);
        }
        return data;
    }
}
