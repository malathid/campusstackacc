/**
 * @author malathi
 */
package CAMPS.Accounts;

import CAMPS.Connect.DBConnect;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class fin_accounts extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        DBConnect con = new DBConnect();
        try (PrintWriter out = response.getWriter()) {
            try {
                con.getConnection();
                String data = "";
                if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("loadcc")) {
                    data = "<option value='' selected><--Select--></option>";
                    con.read1("SELECT otlm.otlm_id,IFNULL(CONCAT(sm.first_name,' ',sm.`middle_name`,' ',sm.last_name,'-',sm.staff_id),'') staff,GROUP_CONCAT(DISTINCT  IF(otlm.type='debit',otlm.otlm_id,'') SEPARATOR '') debit,GROUP_CONCAT(DISTINCT IF(otlm.type='credit',otlm.otlm_id,'') SEPARATOR '')credit FROM accounts.om_tt_lm_mapping otlm LEFT JOIN camps.`staff_master` sm ON sm.staff_id=otlm.`staff_id` WHERE otlm.om_id='" + request.getParameter("om_id") + "' AND otlm.tt_id=" + request.getParameter("tt_id") + " AND otlm.status>0 AND otlm.lm_id=" + request.getParameter("lm_id") + " GROUP BY otlm.staff_id");
                    while (con.rs1.next()) {
                        data += "<option value='" + con.rs1.getString("otlm_id") + "' data-debit='" + con.rs1.getString("debit") + "'  data-credit='" + con.rs1.getString("credit") + "' >" + con.rs1.getString("staff") + "</option>";
                    }
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("loadcredit")) {
                int i = 1, order = 0, cm_id = 0, ocount = 1, j = 0, mm_id = 0;
                double total = 0, amount = 0, ag_amount = 0;
                String lm_id = "", bill = "", data1 = "", apex_no = "", otlm_id = "";
                String apexdata = "";
                data = "<table class=\"tbl\" width='100%' >";
                if (request.getParameter("lm_id").equalsIgnoreCase("undefined")) {
                    otlm_id = request.getParameter("otlm");
                } else {
                    otlm_id = request.getParameter("lm_id");
                }
                con.read("SELECT DISTINCT a.apex_no FROM (SELECT bm.`bm_id`, bm.apex_no,bm.`amount` FROM accounts.bill_master bm INNER JOIN accounts.bill_type bt ON bt.bt_id=bm.bt_id  AND bm.staff_id=(SELECT DISTINCT otlm.staff_id FROM accounts.om_tt_lm_mapping otlm  WHERE otlm.otlm_id=" + otlm_id + " AND otlm.STATUS>0)  AND bm.lm_id=(SELECT DISTINCT otlm.lm_id FROM accounts.om_tt_lm_mapping otlm WHERE otlm.otlm_id=" + otlm_id + " AND otlm.STATUS>0) AND ((bt.type=1 && bm.status=2) || (bt.type=2 && bm.status=4)))a LEFT JOIN (SELECT bp.bm_id,SUM(bp.`amount`) amount FROM `accounts`.`bill_payment` bp WHERE bp.`status`>0 GROUP BY bp.`bm_id` )b ON a.bm_id=b.bm_id AND  a.amount<>b.amount");
                while (con.rs.next()) {
                    con.read1("SELECT msc_id2,mm_id,order_no2,mt_id,cm_id,order_no1 FROM (SELECT amm1.*,amm2.*,@id1:=@id1+1 AS order_no1 FROM (SELECT @id1:=0)s1,(SELECT msc.*,amm.* FROM (SELECT msc_id2,mm_id,mdesc,amt_approved,order_no2 FROM(SELECT msc1.*,msc2.mm_id mm_id2,msc2.msc_id msc_id2,@id:=@id+1 AS order_no2 FROM (SELECT @id:=0)s,(SELECT * FROM accounts.`apex_minutes_sub_cat`  WHERE msc_id=" + con.rs.getString("apex_no") + ")msc1 JOIN accounts.`apex_minutes_sub_cat` msc2 ON msc1.mm_id=msc2.mm_id ORDER BY msc2.msc_id )a WHERE a.msc_id2=" + con.rs.getString("apex_no") + ") msc JOIN (SELECT mm_id mm_id1,mt_id,cm_id FROM accounts.`apex_minutes_master` ) amm ON amm.mm_id1=msc.mm_id)amm1 JOIN (SELECT mm_id mm_id2,mt_id mt_id2,cm_id cm_id2 FROM accounts.`apex_minutes_master`) amm2 ON amm2.mt_id2=amm1.mt_id AND amm2.cm_id2=amm1.cm_id ORDER BY amm2.mm_id2 ) b WHERE mm_id2=mm_id1");
                    if (con.rs1.next()) {
                        apex_no = con.rs1.getString("mt_id") + (char) (con.rs1.getInt("cm_id") + 64) + con.rs1.getString("order_no1") + (char) (96 + (con.rs1.getInt("order_no2")));
                        apexdata += "<option value=\"" + con.rs.getString("apex_no") + "\">" + apex_no + " &nbsp --- (" + con.rs.getString("apex_no") + ")</option>";
                    }
                }
                data += "<tr><td align='center'><b>Select Apex</b></td><td><select id='apxmsc_cr_" + otlm_id + "' name='jtm'  class=\"validate[required]\" onchange='load_cr_billing(" + otlm_id + ")'><option selected disabled>--Select--</option>" + apexdata + " </select> </td></tr><tr><td colspan='2' id='displaybill_cr_" + otlm_id + "' name='displaybill_cr' ></td></tr></table>";
            } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("load_cr_billing")) {
                int i = 1, order = 0, cm_id = 0, ocount = 1, j = 0, mm_id = 0;
                double total = 0, amount = 0, ag_amount = 0;
                String lm_id = "", bill = "", data1 = "", apex_no = "", otlm_id = "";
                // con.read("SELECT bm.bm_id,lm.lm_id,bm.jtm_id,lm.acc_head,bt.type,IFNULL(bm.bill_no,'-')bill_no,DATE_FORMAT(bm.bill_date,'%d-%m-%Y')bill_date,bm.bill_detail,IFNULL(bm.settle_days,'-')settle_days,bm.apex_no,bm.bt_id,bt_name,bm.amount,bm.status FROM accounts.bill_master bm INNER JOIN accounts.bill_type bt ON bt.bt_id=bm.bt_id INNER JOIN accounts.ledger_master lm ON lm.lm_id=bm.lm_id WHERE  bm.`apex_no`=" + request.getParameter("msc_id") + " AND ((bt.type=1 && bm.status=2) || (bt.type=2 && bm.status=4))");
                con.read("SELECT bm.bm_id,lm.lm_id,bm.jtm_id,lm.acc_head,bt.type,IFNULL(bm.bill_no,'-')bill_no,DATE_FORMAT(bm.bill_date,'%d-%m-%Y')bill_date,bm.bill_detail,IFNULL(bm.settle_days,'-')settle_days,bm.apex_no,bm.bt_id,bt_name,bm.amount,bm.status,bm.`staff_id` FROM accounts.bill_master bm INNER JOIN accounts.bill_type bt ON bt.bt_id=bm.bt_id INNER JOIN accounts.ledger_master lm ON lm.lm_id=bm.lm_id WHERE  bm.`apex_no`=" + request.getParameter("msc_id") + " AND ((bt.type=1 && bm.status=2) || (bt.type=2 && bm.status=4)) AND bm.lm_id=(SELECT DISTINCT otlm.lm_id FROM accounts.om_tt_lm_mapping otlm WHERE otlm.otlm_id='" + request.getParameter("otlm") + "' AND otlm.STATUS>0) AND bm.staff_id=(SELECT DISTINCT otlm.staff_id FROM accounts.om_tt_lm_mapping otlm  WHERE otlm.otlm_id='" + request.getParameter("otlm") + "' AND otlm.STATUS>0) ");
                data = "<table class=\"tbl\" width='100%' ><tr><th>Bill.No</th><th>Bill.Date</th><th>Description</th><th>Settle.Days</th><th>Type</th><th>Total Amount</h><th>Check In</th><th>Against Bill Amount</th></tr>";
                data += "<tr><td colspan='8' align='center'><b>Unprocessed Bill</b></td></tr>";
                while (con.rs.next()) {
                    total = con.rs.getDouble("amount");
                    if (con.rs.getString("status").equalsIgnoreCase("2")) {
                        data += "<tr><td>" + con.rs.getString("bill_no") + "</td><td>" + con.rs.getString("bill_date") + "</td><td>" + con.rs.getString("bill_detail") + "</td><td align='center'>" + con.rs.getString("settle_days") + "</td><td>" + con.rs.getString("bt_name") + "</td>";     //<td>" + con.rs.getString("amount") + "</td>
                        con.read1("SELECT bed.bem_id,bem.field_name,bem.value_type,bed.amt_value FROM accounts.bill_extradetails bed INNER JOIN accounts.bill_extramaster bem ON bed.bem_id=bem.bem_id WHERE bed.bm_id=" + con.rs.getString("bm_id") + " AND bed.STATUS>0 AND bem.status>0 AND bem.value_type !='info' ");
                        data += "<td hidden><table class='tbl' width='100%'>";
                        while (con.rs1.next()) {
                            amount = parseDouble(con.rs.getString("amount")) * (parseDouble(con.rs1.getString("amt_value")) / 100);
                            data += "<tr><td>" + con.rs1.getString("field_name") + "</td><td>" + amount + "</td></tr>";
                            total += amount;
                        }
                        data += "</table></td><td align='right'>" + String.format("%.2f", total) + "</td>";
                        if (con.rs.getString("type").equalsIgnoreCase("2")) { //(con.rs.getString("jtm_id") != null) {
                            data += "<td align='center'><input type=\"checkbox\" name=\"process_advance" + "_" + request.getParameter("otlm") + "\" id=\"checkbill" + con.rs.getString("bm_id") + "\"  value=" + con.rs.getString("bm_id") + "_" + String.format("%.2f", total) + "  data-amt='" + total + "' onchange='display_amt(" + con.rs.getString("bm_id") + "," + String.format("%.2f", total) + ")'></td><td><input type='text' id='againstbill" + con.rs.getString("bm_id") + "' ' name='against_bill_'" + con.rs.getString("bm_id") + "' onblur='display_amt(" + con.rs.getString("bm_id") + ",this.value)' ></td></tr>";
                        } else {
                            data += "<td align='center'><input type=\"checkbox\" name=\"unprocess_bill" + "_" + request.getParameter("otlm") + "\" id=\"checkbill" + con.rs.getString("bm_id") + "\"  value=" + con.rs.getString("bm_id") + "_" + String.format("%.2f", total) + "  data-amt='" + String.format("%.2f", total) + "' ></td></tr>";
                        }
                        i++;
                    } else {
                        con.read2("SELECT SUM(bp.amount)amount FROM accounts.`bill_payment` bp LEFT JOIN accounts.`bill_master` bm ON bm.bm_id=bp.bm_id WHERE bp.bm_id=" + con.rs.getString("bm_id") + " AND bp.status>0");
                        if (con.rs2.next()) {
                            ag_amount = con.rs2.getDouble("amount");
                        }
                        ag_amount = Math.round(total * 100.0) / 100.0 - Math.round(ag_amount * 100.0) / 100.0;
                        if ((ag_amount) > 0) {
                            data1 += "<tr><td align='center'>" + con.rs.getString("bill_no") + "</td><td>" + con.rs.getString("bill_date") + "</td><td>" + con.rs.getString("bill_detail") + "</td><td align='center'>" + con.rs.getString("settle_days") + "</td><td>" + con.rs.getString("bt_name") + "</td>";     //<td>" + con.rs.getString("amount") + "</td>
                            con.read1("SELECT bed.bem_id,bem.field_name,bem.value_type,bed.amt_value FROM accounts.bill_extradetails bed INNER JOIN accounts.bill_extramaster bem ON bed.bem_id=bem.bem_id WHERE bed.bm_id=" + con.rs.getString("bm_id") + " AND bed.STATUS>0 AND bem.status>0 AND bem.value_type !='info' ");
                            //     data1 += "<td hidden><table class='tbl' width='100%'>";
//                            while (con.rs1.next()) {
//                                if ((con.rs1.getString("value_type")).equalsIgnoreCase("percentage")) {
//                                    con.read2("SELECT bim_name,VALUE FROM accounts.`bill_info_master` WHERE bim_id=" + con.rs1.getString("bem_id"));
//                                    if (con.rs2.next()) {
//                                        amount = parseDouble(con.rs.getString("amount")) * (parseDouble(con.rs1 .getString("amt_value")) / 100);
//                                    }
//                                    data1 += "<tr><td>" + con.rs1.getString("field_name") + "-" + con.rs2.getString("bim_name") + "</td><td>" + amount + "</td></tr>";
//                                    total += amount;
//                                } else {
//                                    data1 += "<tr><td>" + con.rs1.getString("field_name") + "</td><td>" + con.rs1.getString("amt_value") + "</td></tr>";
//                                    total += parseDouble(con.rs1.getString("amt_value"));
//                                }
//                            }

                            //   data1 += "</table></td>";
                            data1 += "<td align='right'>" + String.format("%.2f", ag_amount) + "</td>";
                            if (con.rs.getString("type").equalsIgnoreCase("2")) { //(con.rs.getString("jtm_id") != null) {
                                data1 += "<td align='center'><input type=\"checkbox\" name=\"process_advance" + "_" + request.getParameter("otlm") + "\"  id=\"checkbill" + con.rs.getString("bm_id") + "\"  value=" + con.rs.getString("bm_id") + "_" + String.format("%.2f", ag_amount) + "  data-amt='" + String.format("%.2f", ag_amount) + "' onchange='display_amt(" + con.rs.getString("bm_id") + "," + ag_amount + ")'></td><td><input type='text' id='againstbill" + con.rs.getString("bm_id") + "' ' name='against_bill_" + con.rs.getString("bm_id") + "' onblur='display_amt(" + con.rs.getString("bm_id") + ",this.value)'></td></tr>";
                            } else {
                                data1 += "<td align='center'><input type=\"checkbox\" name=\"unprocess_bill" + "_" + request.getParameter("otlm") + "\"  id=\"checkbill" + con.rs.getString("bm_id") + "\"  value=" + con.rs.getString("bm_id") + "_" + String.format("%.2f", ag_amount) + "  data-amt='" + String.format("%.2f", ag_amount) + "' ></td></tr>";
                            }
                        }
                        i++;
                    }
                }
                data += "<tr><td colspan='8' align='center'><b>Processed Advance Bills</b></td></tr>" + data1 + "</table>";
            } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("loaddebit")) {
                int i = 1, order = 0, cm_id = 0, ocount = 1, j = 0, mm_id = 0;
                double total = 0, amount = 0, ag_amount = 0;
                String lm_id = "", bill = "", data1 = "", apex_no = "", otlm_id = "";
                String apexdata = "";
                data = "<table class=\"tbl\" width='100%' >";
                if (request.getParameter("lm_id").equalsIgnoreCase("undefined")) {
                    otlm_id = request.getParameter("otlm");
                } else {
                    otlm_id = request.getParameter("lm_id");
                }
                con.read("SELECT DISTINCT a.apex_no FROM (SELECT bm.`bm_id`, bm.apex_no,bm.`amount` FROM accounts.bill_master bm INNER JOIN accounts.bill_type bt ON bt.bt_id=bm.bt_id  AND bm.staff_id=(SELECT DISTINCT otlm.staff_id FROM accounts.om_tt_lm_mapping otlm  WHERE otlm.otlm_id=" + otlm_id + " AND otlm.STATUS>0)  AND bm.lm_id=(SELECT DISTINCT otlm.lm_id FROM accounts.om_tt_lm_mapping otlm WHERE otlm.otlm_id=" + otlm_id + " AND otlm.STATUS>0) AND ((bt.type=1 && bm.status=4) || (bt.type=2 && bm.status=2)))a LEFT JOIN (SELECT bp.bm_id,SUM(bp.`amount`) amount FROM `accounts`.`bill_payment` bp WHERE bp.`status`>0 GROUP BY bp.`bm_id` )b ON a.bm_id=b.bm_id AND  a.amount<>b.amount");
                while (con.rs.next()) {
                    con.read1("SELECT msc_id2,mm_id,order_no2,mt_id,cm_id,order_no1 FROM (SELECT amm1.*,amm2.*,@id1:=@id1+1 AS order_no1 FROM (SELECT @id1:=0)s1,(SELECT msc.*,amm.* FROM (SELECT msc_id2,mm_id,mdesc,amt_approved,order_no2 FROM(SELECT msc1.*,msc2.mm_id mm_id2,msc2.msc_id msc_id2,@id:=@id+1 AS order_no2 FROM (SELECT @id:=0)s,(SELECT * FROM accounts.`apex_minutes_sub_cat`  WHERE msc_id=" + con.rs.getString("apex_no") + ")msc1 JOIN accounts.`apex_minutes_sub_cat` msc2 ON msc1.mm_id=msc2.mm_id ORDER BY msc2.msc_id )a WHERE a.msc_id2=" + con.rs.getString("apex_no") + ") msc JOIN (SELECT mm_id mm_id1,mt_id,cm_id FROM accounts.`apex_minutes_master` ) amm ON amm.mm_id1=msc.mm_id)amm1 JOIN (SELECT mm_id mm_id2,mt_id mt_id2,cm_id cm_id2 FROM accounts.`apex_minutes_master`) amm2 ON amm2.mt_id2=amm1.mt_id AND amm2.cm_id2=amm1.cm_id ORDER BY amm2.mm_id2 ) b WHERE mm_id2=mm_id1");
                    if (con.rs1.next()) {
                        apex_no = con.rs1.getString("mt_id") + (char) (con.rs1.getInt("cm_id") + 64) + con.rs1.getString("order_no1") + (char) (96 + (con.rs1.getInt("order_no2")));
                        apexdata += "<option value=\"" + con.rs.getString("apex_no") + "\">" + apex_no + " &nbsp  --- (" + con.rs.getString("apex_no") + ")</option>";
                    }
                }
                data += "<tr><td align='center'><b>Select Apex</b></td><td><select id='apxmsc_dr_" + otlm_id + "' name='jtm'  class=\"validate[required]\" onchange='load_dr_billing(" + otlm_id + ")'><option selected disabled>--Select--</option>" + apexdata + " </select> </td></tr><tr><td colspan='2' id='displaybill_dr_" + otlm_id + "' name='displaybill_dr' ></td></tr></table>";
            } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("load_dr_billing")) {
                int i = 1, order = 0, cm_id = 0, mm_id = 0;
                double total = 0, amount = 0, ag_amount = 0;
                String lm_id = "", bill = "", data1 = "", data2 = "", apex_no = "", otlm_id = "";
                String datains = "";
                data = "<table class=\"tbl\" width='100%' ><tr><th>Bill.No</th><th>Bill.Date</th><th>Description</th><th>Settle.Days</th><th>Type</th><th>Total Amount</h><th>Check In</th><th>Against Bill Amount</th></tr>";
                data += "<tr><td colspan='8' align='center'><b>Unprocessed Advance Bills</b></td></tr>";
                con.read("SELECT bm.bm_id,lm.lm_id,bm.jtm_id,lm.acc_head,bt.type,IFNULL(bm.bill_no,'-')bill_no,DATE_FORMAT(bm.bill_date,'%d-%m-%Y')bill_date,bm.bill_detail,IFNULL(bm.settle_days,'-')settle_days,bm.apex_no,bm.bt_id,bt_name,bm.amount,bm.status FROM accounts.bill_master bm INNER JOIN accounts.bill_type bt ON bt.bt_id=bm.bt_id INNER JOIN accounts.ledger_master lm ON lm.lm_id=bm.lm_id WHERE  bm.`apex_no`=" + request.getParameter("msc_id") + " AND ((bt.type=1 && bm.status=4) || (bt.type=2 && bm.status=2)) AND bm.lm_id=(SELECT DISTINCT otlm.lm_id FROM accounts.om_tt_lm_mapping otlm WHERE otlm.otlm_id='" + request.getParameter("otlm") + "' AND otlm.STATUS>0) AND bm.staff_id=(SELECT DISTINCT otlm.staff_id FROM accounts.om_tt_lm_mapping otlm  WHERE otlm.otlm_id='" + request.getParameter("otlm") + "' AND otlm.STATUS>0)");
                while (con.rs.next()) {
                    total = con.rs.getDouble("amount");
                    if (con.rs.getString("status").equalsIgnoreCase("2")) {
                        data += "<tr><td align='center'>" + con.rs.getString("bill_no") + "</td><td>" + con.rs.getString("bill_date") + "</td><td>" + con.rs.getString("bill_detail") + "</td><td align='center'>" + con.rs.getString("settle_days") + "</td><td>" + con.rs.getString("bt_name") + "</td>";     //<td>" + con.rs.getString("amount") + "</td>
                        data += "<td align='right'>" + String.format("%.2f", total) + "</td>";
                        if (con.rs.getString("type").equalsIgnoreCase("1")) {//(con.rs.getString("jtm_id") != null) {
                            data += "<td align='center'><input type=\"checkbox\"   class='class1'  name=\"process_bill" + "_" + request.getParameter("otlm") + "\" id=\"checkbill" + con.rs.getString("bm_id") + "\"  value=" + con.rs.getString("bm_id") + "_" + String.format("%.2f", total) + "  data-amt='" + String.format("%.2f", total) + "' onchange='display_amt(" + con.rs.getString("bm_id") + "," + String.format("%.2f", total) + ")'></td></tr><td><input type='text' id='againstbill" + con.rs.getString("bm_id") + "' ' name='against_bill_" + con.rs.getString("bm_id") + "' onblur='display_amt(" + con.rs.getString("bm_id") + ",this.value)'></td></tr>";
                        } else {
                            data += "<td align='center'><input type=\"checkbox\"   class='class1'  name=\"unprocess_advance" + "_" + request.getParameter("otlm") + "\" id=\"checkbill" + con.rs.getString("bm_id") + "\"  value=" + con.rs.getString("bm_id") + "_" + String.format("%.2f", total) + "  data-amt='" + String.format("%.2f", total) + "' ></td></tr>";
                        }
                        i++;
                    } else {
                        con.read2("SELECT SUM(bp.amount)amount FROM accounts.`bill_payment` bp LEFT JOIN accounts.`bill_master` bm ON bm.bm_id=bp.bm_id WHERE bp.bm_id=" + con.rs.getString("bm_id") + " AND bp.status>0");
                        if (con.rs2.next()) {
                            ag_amount = con.rs2.getDouble("amount");
                        }
                        con.read1("SELECT bed.bem_id,bem.field_name,bem.value_type,bed.amt_value FROM accounts.bill_extradetails bed INNER JOIN accounts.bill_extramaster bem ON bed.bem_id=bem.bem_id WHERE bed.bm_id=" + con.rs.getString("bm_id") + " AND bed.STATUS>0 AND bem.status>0 AND bem.value_type !='info' ");
                        datains += "<td hidden><table class='tbl' width='100%'>";
                        while (con.rs1.next()) {
                            if ((con.rs1.getString("value_type")).equalsIgnoreCase("percentage")) {
                                con.read2("SELECT bim_name,VALUE FROM accounts.`bill_info_master` WHERE bim_id=" + con.rs1.getString("bem_id"));
                                if (con.rs2.next()) {
                                    amount = parseDouble(con.rs.getString("amount")) * (parseDouble(con.rs1.getString("amt_value")) / 100);
                                }
                                datains += "<tr><td>" + con.rs1.getString("field_name") + "-" + con.rs2.getString("bim_name") + "</td><td>" + String.format("%.2f", amount) + "</td></tr>";
                                total += amount;
                            } else if ((con.rs1.getString("value_type")).equalsIgnoreCase("amount")||(con.rs1.getString("value_type")).equalsIgnoreCase("numeric")) {
                                datains += "<tr><td>" + con.rs1.getString("field_name") + "</td><td>" + con.rs1.getString("amt_value") + "</td></tr>";
                                total += parseDouble(con.rs1.getString("amt_value"));
                            }
                        }
                        datains += "</table></td>";
                        ag_amount = Math.round(total * 100.0) / 100.0 - Math.round(ag_amount * 100.0) / 100.0;
                        if (ag_amount > 0) {
                            data1 += "<tr><td>" + con.rs.getString("bill_no") + "</td><td>" + con.rs.getString("bill_date") + "</td><td>" + con.rs.getString("bill_detail") + "</td><td>" + con.rs.getString("settle_days") + "</td><td>" + con.rs.getString("bt_name") + "</td>";     //<td>" + con.rs.getString("amount") + "</td>
                            data1 += datains + "<td align='right'> " + String.format("%.2f", ag_amount) + "</td>";
                            if (con.rs.getString("type").equalsIgnoreCase("1")) { //(con.rs.getString("jtm_id") != null) {
                                data1 += "<td align='center'><input type=\"checkbox\"   class='class2' name=\"process_bill" + "_" + request.getParameter("otlm") + "\" id=\"checkbill" + con.rs.getString("bm_id") + "\"  value=" + con.rs.getString("bm_id") + "_" + String.format("%.2f", ag_amount) + "  data-amt='" + String.format("%.2f", ag_amount) + "' onchange='display_amt(" + con.rs.getString("bm_id") + "," + String.format("%.2f", ag_amount) + ")'></td><td><input type='text' id='againstbill" + con.rs.getString("bm_id") + "' ' name='against_bill_" + con.rs.getString("bm_id") + "' onblur='display_amt(" + con.rs.getString("bm_id") + ",this.value)'></td></tr>";
                            } else {
                                data1 += "<td align='center'><input type=\"checkbox\"   class='class2' name=\"unprocess_advance" + "_" + request.getParameter("otlm") + "\" id=\"checkbill" + con.rs.getString("bm_id") + "\"  value=" + con.rs.getString("bm_id") + "_" + String.format("%.2f", ag_amount) + "  data-amt='" + String.format("%.2f", ag_amount) + "' ></td></tr>";
                            }
                        } else {
                            datains = "";
                        }
                        i++;
                    }
                }
                data += "<tr><td colspan='8' align='center'><b>Processed Bills</b></td></tr>" + data1 + "</table>";
            }   else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("loadlm")) {
                    String debit_only_lm = "", credit_only_lm = "", other_lm = "", tt_type = "", opt = "<option value='' selected><--Select--></option>";
                    //   con.read1("SELECT tt.trans_type,lm.lm_id,lm.acc_head,GROUP_CONCAT(IF(otlm.type='debit',otlm.otlm_id,'') SEPARATOR '') debit,GROUP_CONCAT(IF(otlm.type='credit',otlm.otlm_id,'') SEPARATOR '')   credit,IF(lim.lim_name='Yes',1,0) attach_doc FROM accounts.om_tt_lm_mapping otlm INNER JOIN accounts.ledger_master lm ON lm.lm_id=otlm.lm_id AND otlm.om_id='" + request.getParameter("om_id") + "' AND otlm.tt_id=" + request.getParameter("tt_id") + " AND otlm.status>0 LEFT JOIN  accounts.lm_extradetails le ON le.lm_id=lm.lm_id AND le.lem_id=7 AND le.status>0 LEFT JOIN accounts.lm_info_master lim ON le.lem_id=lim.lem_id INNER JOIN accounts.transaction_type tt ON tt.tt_id=otlm.tt_id GROUP BY lm.lm_id");
                    con.read1("SELECT tt.trans_type,otlm.`otlm_id`,IF(otlm.staff_id=0,0,1) staff,lm.lm_id,`accounts`.`getledgercode`(lm.`lm_id`) lm_code,lm.acc_head,IF(otlm.type='debit',otlm.otlm_id,'') debit,IF(otlm.type='credit',otlm.otlm_id,'') credit,IF(lim.lim_name='Yes',1,0) attach_doc FROM accounts.om_tt_lm_mapping otlm INNER JOIN accounts.ledger_master lm ON lm.lm_id=otlm.lm_id AND otlm.om_id='" + request.getParameter("om_id") + "' AND otlm.tt_id=" + request.getParameter("tt_id") + " AND otlm.status>0 LEFT JOIN  accounts.lm_extradetails le ON le.lm_id=lm.lm_id AND le.lem_id=7 AND le.lm_value=3 AND le.status>0 LEFT JOIN accounts.lm_info_master lim ON le.lem_id=lim.lem_id INNER JOIN accounts.transaction_type tt ON tt.tt_id=otlm.tt_id GROUP BY lm.lm_id");
                    while (con.rs1.next()) {
                        tt_type = con.rs1.getString("trans_type");
                        if (!con.rs1.getString("credit").equalsIgnoreCase("")) {
                            credit_only_lm += "<option value='" + con.rs1.getString("otlm_id") + "'  data-lm='" + con.rs1.getString("lm_id") + "' data-debit='" + con.rs1.getString("debit") + "'  data-credit='" + con.rs1.getString("credit") + "' data-ad='" + con.rs1.getString("attach_doc") + "' data-staff='" + con.rs1.getString("staff") + "' >" + con.rs1.getString("lm_code") + " - " + con.rs1.getString("acc_head") + "</option>";
                        }
                        if (!con.rs1.getString("debit").equalsIgnoreCase("")) {
                            debit_only_lm += "<option value='" + con.rs1.getString("otlm_id") + "'  data-lm='" + con.rs1.getString("lm_id") + "' data-debit='" + con.rs1.getString("debit") + "'  data-credit='" + con.rs1.getString("credit") + "' data-ad='" + con.rs1.getString("attach_doc") + "' data-staff='" + con.rs1.getString("staff") + "' >" + con.rs1.getString("lm_code") + " - " + con.rs1.getString("acc_head") + "</option>";
                        }
                        other_lm += "<option value='" + con.rs1.getString("otlm_id") + "'  data-lm='" + con.rs1.getString("lm_id") + "' data-debit='" + con.rs1.getString("debit") + "'  data-credit='" + con.rs1.getString("credit") + "' data-ad='" + con.rs1.getString("attach_doc") + "' data-staff='" + con.rs1.getString("staff") + "' >" + con.rs1.getString("lm_code") + " - " + con.rs1.getString("acc_head") + "</option>";

                    }
                    if (tt_type.equalsIgnoreCase("Debit")) {
                        data += "<script> $('#jtm_1').html(\"" + opt + debit_only_lm + "\");$('#jtm_2').html(\"" + opt + other_lm + "\");</script>";
                    } else if (tt_type.equalsIgnoreCase("Credit")) {
                        data += "<script>$('#jtm_1').html(\"" + opt + credit_only_lm + "\"); $('#jtm_2').html(\"" + opt + other_lm + "\");</script>";
                    } else {
                        data += "<script> $('#jtm_1').html(\"" + opt + other_lm + "\");$('#jtm_2').html(\"" + opt + other_lm + "\");</script>";
                    }
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("loadtrans")) {
                    String st_date = "", end_date = "", tt_desc = "", pre_st_date = "";
                    String credit_only_lm = "", other_lm = "", debit_only_lm = "", tt_type = "", partyoption = "";
                    con.read("SELECT CONCAT('<option value=\"',tt.tt_id,'\">',tt.tt_desc,'</option>') opt FROM accounts.transaction_type tt INNER JOIN accounts.staff_tt_mapping stm ON stm.staff_id='" + session.getAttribute("ss_id") + "' AND stm.tt_id=tt.tt_id AND stm.status>0 AND tt.status>0 AND stm.om_id=" + request.getParameter("om_id") + "");
                    data += "<div class=\"col-xs-12 col-sm-12\"><div class=\"col-xs-12 col-sm-4 align-left\">"
                            + "<div class=\"form-group form-float\"><label class=\"form-label\" style=\"font-weight: normal;color:#aaa;\">Transaction Type</label><div class=\"form-line\">"
                            + "<select class=\"form-control selectpicker show-tick\" data-style=\"btn-col-teal-bg\" data-actions-box=\"true\" name=\"tt_id\" id=\"tt_id\"  data-live-search=\"true\" required onchange=\"loadlm()\"><option value=''><---Select---></option>";
                    while (con.rs.next()) {
                        data += con.rs.getString("opt");
                    }
                    data += " </select></div></div></div>";
                    data += "<div class=\"col-xs-12 col-sm-4 align-left\">"
                            + "<div class=\"form-group form-float\"><label class=\"form-label\" style=\"font-weight: normal;color:#aaa;\">Transaction Date</label><div class=\"form-line\">";
                    con.read("SELECT DATE_FORMAT(a.start_date,'%d-%m-%Y') start_date,DATE_FORMAT(a.end_date,'%d-%m-%Y') end_date,DATE_FORMAT(IF(NOW() BETWEEN a.start_date AND a.end_date,NOW(),a.start_date), '%d-%m-%Y') cur FROM accounts.accounting_year a WHERE a.ay_id=" + request.getParameter("ay_id"));
                    int i_count = 1;
                    if (con.rs.next()) {
                        data += "<input type=\"text\" class=\"form-control\" name=\"tt_date\" id=\"tt_date\" required readOnly value=\" " + con.rs.getString("cur") + "\">";
                        // data += "<script>$(\"#tt_date\").bootstrapMaterialDatePicker({  time: false, clearButton: true, format : 'DD-MM-YYYY', changeMonth: true, changeYear: true,currentDate: '" + con.rs.getString("cur") + "', minDate: '" + con.rs.getString("start_date") + "', maxDate: '" + con.rs.getString("end_date") + "'});</script>";
                    }
                    data += "</div></div></div></div>";
                    data += "<div class=\"col-xs-12 col-sm-12\"><div class=\"col-xs-12 col-sm-8 align-left\">"
                            + "<div class=\"form-group form-float\"><label class=\"form-label\" style=\"font-weight: normal;color:#aaa;\">Narration</label><div class=\"form-line\">"
                            + "<textarea class=\"form-control\" rows=\"2\" id=\"narration\"></textarea></div></div></div></div>";
                    data += "<div class='col-xs-12 col-sm-12 align-left' style='padding-left: 30px;padding-right: 30px;'><div class='table-responsive'>\n"
                            + "                                <table width='100%' id='jm_detail' class='table-bordered'><thead><tr><th width='26%'>Ledger</th><th width='25%'>Add Details</th><th width='5%'>Details</th><th width='12%'>Debit</th><th width='12%'>Credit</th><th width='20%'>Remarks</th><th></th></tr></thead>\n"
                            + "                                    <tbody>";
//                            data+= "<tr id='1'><td style='vertical-align: top; padding: 5px;' ><select id='jtm_1' name='jtm' style='width:70%' onchange='load_cc(1)' class='form-control selectpicker show-tick' data-live-search='true'><option value=''>--Select Journal--</option></select>\n"
//                            + "                                                <div> <table  id='ccdiv_1'></table></div></td> <td style='vertical-align: top; padding: 5px;' >   <select id='party_1' name='party' style='width:70%' class='form-control selectpicker show-tick' data-live-search='true'><option value=''>--Select party--</option></select>\n"
//                            + "                                                <input type='hidden' id='otlm_1' name='otlm' data-ad=''/><div id='chqdiv_1'></div></td><td style='vertical-align: top; padding: 5px;' >\n"
//                            + "                                                <button type=\"button\" style='width:100%;background-color: #eaecd5;border: 1px solid #4caf50;' class=\"btn\" onclick=\"load_btn(1)\" data-book-id=\"44444\" name='chq' id='chq_1' data-toggle=\"modal\">CC</button>\n"
//                            + "                                                <div class='align-center' id='icondiv_1'></div></td> </td><td style='vertical-align: top;padding: 5px;'><input type='text' id='jdebit1' style='width:100%;border: 1px solid #2196f3;text-align:right; ' name='jdebit' class='currency form-control' data-v-min='0' data-m-dec='2'  data-d-group='2'/><table class='tbl' id='debitbill1' ></table></td>\n"
//                            + "                                            <td style='vertical-align: top;padding: 5px;'><input type='text'  id='jcredit1' name='jcredit' style='width:100%;border: 1px solid #2196f3;text-align:right; ' class='currency form-control' data-v-min='0' data-m-dec='2'  data-d-group='2'/> <table class='tbl' id='creditbill1' ></table></td>\n"
//                            + "                                        </tr></tbody><tfoot align=\"right\"><tr><th style='border:0;'></th><th style='border:0;'></th><th style='border:0;'></th><th style='vertical-align: top;padding: 5px;'><input type='text' style='width:100%;border: 1px solid #9c27b0;' class='currency form-control' readonly name='totdebit' id='totdebit'/></th><th style='vertical-align: top;padding: 5px;'><input type='text' style='width:100%;border: 1px solid #9c27b0;' class='currency form-control' readonly  name='totcredit' id='totcredit' class='validate[required,equals[totdebit]]' /> </th></tr></tfoot>\n"
//                            + "                                </table></div></div>";
                    con.read("SELECT jm.desc,accounts.getledgercode(lm.lm_id) lcode,lm.acc_head,IFNULL(CONCAT(sm.first_name,' ',sm.`middle_name`,' ',sm.last_name,'-',sm.staff_id),'') staff,DATE_FORMAT(jm.doje,'%d-%m-%Y') doje,jtm.otlm_id,jtm.jtm_id,jtm.remark,jtm.type,jtm.amount,jtm.chq_no,IF(jtm.chq_date='00-00-00','',DATE_FORMAT(jtm.chq_date,'%d-%m-%y'))chq_date,jtm.br_no,IF(jtm.br_date='00-00-00','',DATE_FORMAT(jtm.br_date,'%d-%m-%y'))br_date,otlm.tt_id,tt.trans_type,otlm.lm_id,IF(jtm.type='credit',CONCAT('value=\"',jtm.amount,'\"'),CONCAT(' readonly')) credit,IF(jtm.type='debit',CONCAT('value=\"',jtm.amount,'\"'),CONCAT(' readonly')) debit, IF(le.lm_id!='',1,0) attach_doc FROM accounts.journal_master_" + request.getParameter("ay_id") + " jm INNER JOIN accounts.journal_trans_master_" + request.getParameter("ay_id") + " jtm ON jtm.jm_id=jm.jm_id AND jtm.status>0 AND  jm.jm_id='" + request.getParameter("jm_id") + "' INNER JOIN accounts.om_tt_lm_mapping otlm ON otlm.otlm_id=jtm.otlm_id INNER JOIN accounts.ledger_master lm ON lm.lm_id=otlm.lm_id INNER JOIN accounts.`transaction_type` tt ON tt.tt_id=otlm.tt_id LEFT JOIN accounts.lm_extradetails le ON le.lem_id=7 AND le.lm_value=3 AND le.status>0 AND le.lm_id=lm.lm_id LEFT JOIN camps.`staff_master` sm ON sm.staff_id=otlm.staff_id ORDER BY jtm.jtm_id");

                    if (con.rs.next()) {
                        //  con.read1("SELECT lm.lm_id,lm.acc_head,GROUP_CONCAT(IF(otlm.type='debit',otlm.otlm_id,'') SEPARATOR '') debit,GROUP_CONCAT(IF(otlm.type='credit',otlm.otlm_id,'') SEPARATOR '')   credit,IF(lim.lim_name='Yes',1,0) attach_doc FROM accounts.om_tt_lm_mapping otlm INNER JOIN accounts.ledger_master lm ON lm.lm_id=otlm.lm_id AND otlm.om_id='" + request.getParameter("om_id") + "' AND otlm.tt_id=" + con.rs.getString("tt_id") + " AND otlm.status>0 LEFT JOIN  accounts.lm_extradetails le ON le.lm_id=lm.lm_id AND le.lem_id=7 AND le.status>0 LEFT JOIN accounts.lm_info_master lim ON le.lem_id=lim.lem_id GROUP BY lm.lm_id");
                        //con.read1("SELECT lm.lm_id,`accounts`.`getledgercode`(lm.`lm_id`) lm_code,otlm.otlm_id,otlm.`tt_id`,IFNULL(CONCAT(sm.first_name,' ',sm.`middle_name`,' ',sm.last_name,'-',sm.staff_id),'')staff,lm.acc_head,GROUP_CONCAT(DISTINCT  IF(otlm.type='debit',otlm.otlm_id,'') SEPARATOR '') debit,GROUP_CONCAT(DISTINCT IF(otlm.type='credit',otlm.otlm_id,'') SEPARATOR '')credit,IF(lim.lim_name='Yes',1,0) attach_doc,trans_type FROM accounts.om_tt_lm_mapping otlm INNER JOIN accounts.ledger_master lm ON lm.lm_id=otlm.lm_id AND otlm.om_id='" + request.getParameter("om_id") + "' AND otlm.tt_id=" + con.rs.getString("tt_id") + " AND otlm.status>0 LEFT JOIN  accounts.lm_extradetails le ON le.lm_id=lm.lm_id AND le.lem_id=7  AND le.lm_value=3 AND le.status>0 LEFT JOIN accounts.lm_info_master lim ON le.lem_id=lim.lem_id LEFT JOIN camps.`staff_master` sm ON sm.staff_id=otlm.staff_id INNER JOIN accounts.transaction_type tt ON tt.tt_id=otlm.tt_id GROUP BY lm_id,otlm.staff_id");
                        con.read2("SET SESSION group_concat_max_len = 1000000000");
                        String query1 = "SELECT a.lm_id,a.lm_code, a.acc_head,a.trans_type,CONCAT('[',GROUP_CONCAT('{\"otlm_id\":\"',a.otlm_id,'\",\"staff\":\"',a.staff,'\",\"debit\":\"',a.debit,'\",\"credit\":\"',a.credit,'\",\"doc\":\"',a.attach_doc,'\",\"transtype\":\"',a.trans_type,'\"}'),']') jarray FROM (SELECT lm.lm_id,`accounts`.`getledgercode`(lm.`lm_id`) lm_code,otlm.otlm_id,otlm.`tt_id`,IFNULL(CONCAT(sm.first_name,' ',sm.`middle_name`,' ',sm.last_name,'-',sm.staff_id),'')staff,lm.acc_head,GROUP_CONCAT(DISTINCT  IF(otlm.type='debit',otlm.otlm_id,'') SEPARATOR '') debit,GROUP_CONCAT(DISTINCT IF(otlm.type='credit',otlm.otlm_id,'') SEPARATOR '')credit,IF(lim.lim_name='Yes',1,0) attach_doc,trans_type FROM accounts.om_tt_lm_mapping otlm INNER JOIN accounts.ledger_master lm ON lm.lm_id=otlm.lm_id AND otlm.om_id='" + request.getParameter("om_id") + "' AND otlm.tt_id=" + con.rs.getString("tt_id") + " AND otlm.status>0 LEFT JOIN  accounts.lm_extradetails le ON le.lm_id=lm.lm_id AND le.lem_id=7  AND le.lm_value=3 AND le.status>0 LEFT JOIN accounts.lm_info_master lim ON le.lem_id=lim.lem_id LEFT JOIN camps.`staff_master` sm ON sm.staff_id=otlm.staff_id INNER JOIN accounts.transaction_type tt ON tt.tt_id=otlm.tt_id GROUP BY lm_id,otlm.staff_id)a GROUP BY a.lm_id";
                        con.read1(query1);
                        tt_desc = con.rs.getString("remark");
                        int j = 0;
                        while (con.rs1.next()) {
                            tt_type = con.rs.getString("trans_type");
                            JSONArray jarr = new JSONArray(con.rs1.getString("jarray"));
                            JSONObject jsonobject = jarr.getJSONObject(0);
                            if (!jsonobject.getString("credit").equalsIgnoreCase("")) {
                                credit_only_lm += "<option value='" + jsonobject.getString("otlm_id") + "' data-lm='" + con.rs1.getString("lm_id") + "' data-debit='" + jsonobject.getString("debit") + "'  data-credit='" + jsonobject.getString("credit") + "' data-ad='" + jsonobject.getString("doc") + "' data-staff='" + con.rs1.getString("staff_id") + "' >" + con.rs1.getString("lm_code") + " - " + con.rs1.getString("acc_head") + "</option>";
                            } else if (!jsonobject.getString("debit").equalsIgnoreCase("")) {
                                debit_only_lm += "<option value='" + jsonobject.getString("otlm_id") + "' data-lm='" + con.rs1.getString("lm_id") + "' data-debit='" + jsonobject.getString("debit") + "'  data-credit='" + jsonobject.getString("credit") + "' data-ad='" + jsonobject.getString("doc") + "' data-staff='" + con.rs1.getString("staff_id") + "' >" + con.rs1.getString("lm_code") + " - " + con.rs1.getString("acc_head") + "</option>";
                            } else {
                                other_lm += "<option value='" + jsonobject.getString("otlm_id") + "' data-lm='" + con.rs1.getString("lm_id") + "' data-debit='" + jsonobject.getString("debit") + "'  data-credit='" + jsonobject.getString("credit") + "' data-ad='" + jsonobject.getString("doc") + "' data-staff='" + con.rs1.getString("staff_id") + "' >" + con.rs1.getString("lm_code") + " - " + con.rs1.getString("acc_head") + "</option>";
                            }
                            j++;
                            for (int i = 1; i < jarr.length(); i++) {
                                JSONObject jsono = jarr.getJSONObject(i);
                                partyoption = "<option value='" + jsonobject.getString("otlm_id") + "' data-lm='" + con.rs1.getString("lm_id") + "' data-debit='" + jsonobject.getString("debit") + "'  data-credit='" + jsonobject.getString("credit") + "' data-ad='" + jsonobject.getString("doc") + "' data-staff='" + con.rs1.getString("staff_id") + "' >" + (jsonobject.getString("staff").equalsIgnoreCase("--0") ? "" : "" + jsonobject.getString("staff")) + "</option>";
                            }
                        }
                        if (tt_type.equalsIgnoreCase("Debit")) {
                            data += "<tr id='" + i_count + "'><td style='vertical-align: top; padding: 5px;' ><select id='jtm_" + i_count + "' data-container=\"body\" name='jtm' style=\"width:70%;\" onchange='load_cc(" + i_count + ")' class='form-control selectpicker dropdown show-tick' data-dropup-auto=\"false\" data-live-search='true'>" + debit_only_lm + "</select><input type='hidden' id='otlm_" + i_count + "' name='otlm' value='" + con.rs.getString("otlm_id") + "' data-ad='" + con.rs.getString("attach_doc") + "'/><div><table  id='ccdiv_" + i_count + "'></table></div></td> <td style='vertical-align: top; padding: 5px;'>   <select id='party_" + i_count + "' name='party' style='width:70%' data-container=\"body\" class='form-control selectpicker dropdown' data-live-search='true' data-dropup-auto=\"false\"><option value=''>--</option>" + partyoption + "</select>"
                                    + "<div id='chqdiv_" + i_count + "'></div></td><td style='vertical-align: top; padding: 5px;' ><input type='hidden' id='jtm_" + i_count + "' name='tm' value='" + con.rs.getString("jtm_id") + "' /><button type=\"button\" style='width:100%;background-color: #fbedc6;border: 1px solid #dbbd64;' class=\"btn\" onclick=\"load_btn(" + i_count + ")\" data-book-id=\"44444\" name='chq' id='chq_" + i_count + "' data-toggle=\"modal\">CC</button>\n"
                                    + "<div class='align-center' id='icondiv_" + i_count + "'></div></td><td style='vertical-align: top;padding: 5px;'><input type='text' id='jdebit" + i_count + "' style='width:100%;border: 1px solid #2196f3;text-align:right; ' name='jdebit' " + con.rs.getString("debit") + " class='currency form-control' data-v-min='0' data-m-dec='2'  data-d-group='2'/><table class='tbl' id='debitbill" + i_count + "' ></table></td><td>\n"
                                    + "<td style='vertical-align: top;padding: 5px;'><input type='text'  id='jcredit" + i_count + "' name='jcredit' " + con.rs.getString("credit") + " style='width:100%;border: 1px solid #2196f3;text-align:right; ' class='currency form-control' data-v-min='0' data-m-dec='2'  data-d-group='2'/> <table class='tbl' id='creditbill" + i_count + "' ></table></td><td style='vertical-align: top;padding: 5px;'><input type='text' style='width:100%;border: 1px  solid #67d3c9' class=\"form-control\" rows=\"1\" id=\"remark" + i_count + "'\" name='remark' /></td>"
                                    + "<td></td></tr>";
                        } else if (tt_type.equalsIgnoreCase("Credit")) {
                            data += "<tr id='" + i_count + "'><td style='vertical-align: top; padding: 5px;' ><select id='jtm_" + i_count + "' name='jtm' data-container=\"body\" style=\"width:70%;\" onchange='load_cc(" + i_count + ")' class='form-control selectpicker dropdown show-tick' data-dropup-auto=\"false\" data-live-search='true'>" + credit_only_lm + "</select><input type='hidden' id='otlm_" + i_count + "' name='otlm' value='" + con.rs.getString("otlm_id") + "' data-ad='" + con.rs.getString("attach_doc") + "'/><div> <table  id='ccdiv_" + i_count + "'></table></div></td> <td style='vertical-align: top; padding: 5px;'>   <select id='party_" + i_count + "' name='party' style='width:70%' data-container=\"body\" class='form-control selectpicker dropdown' data-live-search='true' data-dropup-auto=\"false\"><option value=''>--</option>" + partyoption + "</select>"
                                    + "<div id='chqdiv_" + i_count + "'></div></td><td style='vertical-align: top; padding: 5px;' ><input type='hidden' id='jtm_" + i_count + "' name='tm' value='" + con.rs.getString("jtm_id") + "' /><button type=\"button\" style='width:100%;background-color: #fbedc6;border: 1px solid #dbbd64;' class=\"btn\" onclick=\"load_btn(" + i_count + ")\" data-book-id=\"44444\" name='chq' id='chq_" + i_count + "' data-toggle=\"modal\">CC</button>\n"
                                    + "<div class='align-center' id='icondiv_" + i_count + "'></div></td> <td style='vertical-align: top;padding: 5px;'><input type='text' id='jdebit" + i_count + "' style='width:100%;border: 1px solid #2196f3;text-align:right; ' name='jdebit' " + con.rs.getString("debit") + " class='currency form-control' data-v-min='0' data-m-dec='2'  data-d-group='2'/><table class='tbl' id='debitbill" + i_count + "' ></table></td>\n"
                                    + "<td style='vertical-align: top;padding: 5px;'><input type='text'  id='jcredit" + i_count + "' name='jcredit' " + con.rs.getString("credit") + " style='width:100%;border: 1px solid #2196f3;text-align:right; ' class='currency form-control' data-v-min='0' data-m-dec='2'  data-d-group='2'/> <table class='tbl' id='creditbill" + i_count + "' ></table></td><td style='vertical-align: top;padding: 5px;'><input type='text' style='width:100%;border: 1px  solid #67d3c9' class=\"form-control\" rows=\"1\" id=\"remark" + i_count + "'\" name='remark'/></td>"
                                    + "<td></td></tr>";
                        } else {
                            data += "<tr id='" + i_count + "'><td style='vertical-align: top; padding: 5px;' ><select id='jtm_" + i_count + "' name='jtm' data-container=\"body\" style=\"width:70%;\" onchange='load_cc(" + i_count + ")' class='form-control selectpicker dropdown show-tick' data-dropup-auto=\"false\" data-live-search='true'>" + other_lm + "</select><input type='hidden' id='otlm_" + i_count + "' name='otlm' value='" + con.rs.getString("otlm_id") + "' data-ad='" + con.rs.getString("attach_doc") + "'/><div> <table  id='ccdiv_" + i_count + "'></table></div></td> <td style='vertical-align: top; padding: 5px;'>   <select id='party_" + i_count + "' name='party' style='width:70%' data-container=\"body\" class='form-control selectpicker dropdown' data-live-search='true' data-dropup-auto=\"false\"><option value=''>--</option>" + partyoption + "</select>"
                                    + "<div id='chqdiv_" + i_count + "'></div></td><td style='vertical-align: top; padding: 5px;' ><input type='hidden' id='jtm_" + i_count + "' name='tm' value='" + con.rs.getString("jtm_id") + "' /><button type=\"button\" style='width:100%;background-color: #fbedc6;border: 1px solid #dbbd64;' class=\"btn\" onclick=\"load_btn(" + i_count + ")\" data-book-id=\"44444\" name='chq' id='chq_" + i_count + "' data-toggle=\"modal\">CC</button>\n"
                                    + "<div class='align-center' id='icondiv_" + i_count + "'></div></td> <td style='vertical-align: top;padding: 5px;'><input type='text' id='jdebit" + i_count + "' style='width:100%;border: 1px solid #2196f3;text-align:right; ' name='jdebit' " + con.rs.getString("debit") + " class='currency form-control' data-v-min='0' data-m-dec='2'  data-d-group='2'/><table class='tbl' id='debitbill" + i_count + "' ></table></td>\n"
                                    + "<td style='vertical-align: top;padding: 5px;'><input type='text'  id='jcredit" + i_count + "' name='jcredit' " + con.rs.getString("credit") + " style='width:100%;border: 1px solid #2196f3;text-align:right; ' class='currency form-control' data-v-min='0' data-m-dec='2'  data-d-group='2'/> <table class='tbl' id='creditbill" + i_count + "' ></table></td><td style='vertical-align: top;padding: 5px;'><input type='text' style='width:100%;border: 1px  solid #67d3c9' class=\"form-control\" rows=\"1\" id=\"remark" + i_count + "'\" name='remark'/></td>"
                                      + "<td></td></tr>";
                        }
                        i_count++;
                    } else {
                        data += "<tr id='" + i_count + "'><td style='vertical-align: top; padding: 5px;' ><select id='jtm_" + i_count + "' name='jtm' data-container=\"body\" style='width:70%' onchange='load_cc(" + i_count + ")' class='form-control selectpicker dropdown show-tick' data-dropup-auto=\"false\" data-live-search='true'><option value=''>--Select Journal--</option></select>\n"
                                + "                                                <div> <table  id='ccdiv_" + i_count + "'></table></div></td> <td style='vertical-align: top; padding: 5px;' >   <select id='party_" + i_count + "' name='party' style='width:70%' data-container=\"body\" class='form-control selectpicker dropdown' data-live-search='true' data-dropup-auto=\"false\"><option value=''>--</option></select>\n"
                                + "                                                <input type='hidden' id='otlm_" + i_count + "' name='otlm' data-ad=''/><div id='chqdiv_" + i_count + "'></div></td><td style='vertical-align: top; padding: 5px;' >\n"
                                + "                                                <button type=\"button\" style='width:100%;background-color: #fbedc6;border: 1px solid #dbbd64;' class=\"btn\" onclick=\"load_btn(" + i_count + ")\" data-book-id=\"44444\" name='chq' id='chq_" + i_count + "' data-toggle=\"modal\">CC</button>\n"
                                + "                                                <div class='align-center' id='icondiv_" + i_count + "'></div></td> </td><td style='vertical-align: top;padding: 5px;'><input type='text' id='jdebit" + i_count + "' style='width:100%;border: 1px solid #2196f3;text-align:right; ' name='jdebit' class='currency form-control' data-v-min='0' data-m-dec='2'  data-d-group='2'/><table class='tbl' id='debitbill" + i_count + "' ></table></td>\n"
                                + "                                            <td style='vertical-align: top;padding: 5px;'><input type='text'  id='jcredit" + i_count + "' name='jcredit' style='width:100%;border: 1px solid #2196f3;text-align:right; ' class='currency form-control' data-v-min='0' data-m-dec='2'  data-d-group='2'/> <table class='tbl' id='creditbill" + i_count + "' ></table></td><td style='vertical-align: top;padding: 5px;'><input type='text' style='width:100%;border: 1px  solid #67d3c9' class=\"form-control\" id=\"remark" + i_count + "'\" name='remark'/></td>"
                                 + "<td></td></tr>";
                        i_count++;
                    }
                    while (con.rs.next()) {
                        if (tt_type.equalsIgnoreCase("Debit")) {
                            data += "<tr id='" + i_count + "'><td style='vertical-align: top; padding: 5px;' >" + con.rs.getString("lcode") + "-" + con.rs.getString("acc_head") + "<input type='hidden' id='otlm_" + i_count + "' name='otlm' value='" + con.rs.getString("otlm_id") + "' data-ad='" + con.rs.getString("attach_doc") + "'/><div> <table  id='ccdiv_" + i_count + "'></table></div></td> <td style='vertical-align: top; padding: 5px;'> " + con.rs.getString("staff")
                                    + "<div id='chqdiv_" + i_count + "'></div></td><td style='vertical-align: top; padding: 5px;' ><input type='hidden' id='jtm_" + i_count + "' name='tm' value='" + con.rs.getString("jtm_id") + "' /><button type=\"button\" style='width:100%;background-color: #fbedc6;border: 1px solid #dbbd64;' class=\"btn\" onclick=\"load_btn(" + i_count + ")\" data-book-id=\"44444\" name='chq' id='chq_" + i_count + "' data-toggle=\"modal\">CC</button>\n"
                                    + "<div class='align-center' id='icondiv_" + i_count + "'></div></td><td style='vertical-align: top;padding: 5px;'><input type='text' id='jdebit" + i_count + "' style='width:100%;border: 1px solid #2196f3;text-align:right; ' name='jdebit' " + con.rs.getString("debit") + " class='currency form-control' data-v-min='0' data-m-dec='2'  data-d-group='2'/><table class='tbl' id='debitbill" + i_count + "' ></table></td>\n"
                                    + "<td style='vertical-align: top;padding: 5px;'><input type='text'  id='jcredit" + i_count + "' name='jcredit' " + con.rs.getString("credit") + " style='width:100%;border: 1px solid #2196f3;text-align:right; '  class='currency form-control' data-v-min='0' data-m-dec='2'  data-d-group='2'/> <table class='tbl' id='creditbill" + i_count + "' ></table></td><td style='vertical-align: top;padding: 5px;'><input type='text' style='width:100%;border: 1px  solid #67d3c9' class=\"form-control\" id=\"remark" + i_count + "'\" name='remark'/></td>"
                                      + "<td style='padding: 2px 2px;border: 0px;' align=\"center\"><button type=\"button\" name=\"btnDeleterow\" class=\"btn btn-default waves-effect\" style='padding: 0px 0px;' onclick='deleterow(" + i_count + ")'><i class=\"material-icons\" style=\"color: #ff4733; \">delete_forever</i></button></td></tr>";
                        } else if (tt_type.equalsIgnoreCase("Credit")) {
                            data += "<tr id='" + i_count + "'><td style='vertical-align: top; padding: 5px;' >" + con.rs.getString("lcode") + "-" + con.rs.getString("acc_head") + "<input type='hidden' id='otlm_" + i_count + "' name='otlm' value='" + con.rs.getString("otlm_id") + "' data-ad='" + con.rs.getString("attach_doc") + "'/><div> <table  id='ccdiv_" + i_count + "'></table></div></td> <td style='vertical-align: top; padding: 5px;'> " + con.rs.getString("staff")
                                    + "<div id='chqdiv_" + i_count + "'></div></td><td style='vertical-align: top; padding: 5px;' ><input type='hidden' id='jtm_" + i_count + "' name='tm' value='" + con.rs.getString("jtm_id") + "' /><button type=\"button\" style='width:100%;background-color: #fbedc6;border: 1px solid #dbbd64;' class=\"btn\" onclick=\"load_btn(" + i_count + ")\" data-book-id=\"44444\" name='chq' id='chq_" + i_count + "' data-toggle=\"modal\">CC</button>\n"
                                    + "<div class='align-center' id='icondiv_" + i_count + "'></div></td><td style='vertical-align: top;padding: 5px;'><input type='text' id='jdebit" + i_count + "' style='width:100%;border: 1px solid #2196f3;text-align:right; ' name='jdebit' " + con.rs.getString("debit") + " class='currency form-control' data-v-min='0' data-m-dec='2'  data-d-group='2'/><table class='tbl' id='debitbill" + i_count + "' ></table></td>\n"
                                    + "<td style='vertical-align: top;padding: 5px;'><input type='text'  id='jcredit" + i_count + "' name='jcredit' " + con.rs.getString("credit") + " style='width:100%;border: 1px solid #2196f3;text-align:right; '  class='currency form-control' data-v-min='0' data-m-dec='2'  data-d-group='2'/> <table class='tbl' id='creditbill" + i_count + "' ></table></td><td style='vertical-align: top;padding: 5px;'><input type='text' style='width:100%;border: 1px  solid #67d3c9' class=\"form-control\" id=\"remark" + i_count + "'\" name='remark'/></td>"
                                      + "<td style='padding: 2px 2px;border: 0px;' align=\"center\"><button type=\"button\" name=\"btnDeleterow\" class=\"btn btn-default waves-effect\" style='padding: 0px 0px;' onclick='deleterow(" + i_count + ")'><i class=\"material-icons\" style=\"color: #ff4733; \">delete_forever</i></button></td></tr>";
                        } else {
                            data += "<tr id='" + i_count + "'><td style='vertical-align: top; padding: 5px;' >" + con.rs.getString("lcode") + "-" + con.rs.getString("acc_head") + "<input type='hidden' id='otlm_" + i_count + "' name='otlm' value='" + con.rs.getString("otlm_id") + "' data-ad='" + con.rs.getString("attach_doc") + "'/><div> <table  id='ccdiv_" + i_count + "'></table></div></td> <td style='vertical-align: top; padding: 5px;'> " + con.rs.getString("staff")
                                    + "<div id='chqdiv_" + i_count + "'></div></td><td style='vertical-align: top; padding: 5px;' ><input type='hidden' id='jtm_" + i_count + "' name='tm' value='" + con.rs.getString("jtm_id") + "' /><button type=\"button\" style='width:100%;background-color: #fbedc6;border: 1px solid #dbbd64;' class=\"btn\" onclick=\"load_btn(" + i_count + ")\" data-book-id=\"44444\" name='chq' id='chq_" + i_count + "' data-toggle=\"modal\">CC</button>\n"
                                    + "<div class='align-center' id='icondiv_" + i_count + "'></div></td><td style='vertical-align: top;padding: 5px;'><input type='text' id='jdebit" + i_count + "' style='width:100%;border: 1px solid #2196f3;text-align:right; ' name='jdebit' " + con.rs.getString("debit") + " class='currency form-control' data-v-min='0' data-m-dec='2'  data-d-group='2'/><table class='tbl' id='debitbill" + i_count + "' ></table></td>\n"
                                    + "<td style='vertical-align: top;padding: 5px;'><input type='text'  id='jcredit" + i_count + "' name='jcredit' " + con.rs.getString("credit") + " style='width:100%;border: 1px solid #2196f3;text-align:right; '  class='currency form-control' data-v-min='0' data-m-dec='2'  data-d-group='2'/> <table class='tbl' id='creditbill" + i_count + "' ></table></td><td style='vertical-align: top;padding: 5px;'><input type='text' style='width:100%;border: 1px  solid #67d3c9' class=\"form-control\" id=\"remark" + i_count + "'\" name='remark' /></td>"
                                      + "<td style='padding: 2px 2px;border: 0px;' align=\"center\"><button type=\"button\" name=\"btnDeleterow\" class=\"btn btn-default waves-effect\" style='padding: 0px 0px;' onclick='deleterow(" + i_count + ")'><i class=\"material-icons\" style=\"color: #ff4733; \">delete_forever</i></button></td></tr>";
                        }
                        i_count++;
                    }
                    data += "<tr id='" + i_count + "'><td style='vertical-align: top; padding: 5px;' ><select id='jtm_" + i_count + "' name='jtm' data-container=\"body\" style='width:70%' onchange='load_cc(" + i_count + ")' class='form-control selectpicker dropdown show-tick' data-dropup-auto=\"false\" data-live-search='true'><option value=''>--Select Journal--</option></select>\n"
                            + "                                                <div> <table  id='ccdiv_" + i_count + "'></table></div></td> <td style='vertical-align: top; padding: 5px;' >   <select id='party_" + i_count + "' name='party' style='width:70%' data-container=\"body\" class='form-control selectpicker dropdown' data-live-search='true' data-dropup-auto=\"false\"><option value=''>--</option></select>\n"
                            + "                                                <input type='hidden' id='otlm_" + i_count + "' name='otlm' data-ad=''/><div id='chqdiv_" + i_count + "'></div></td><td style='vertical-align: top; padding: 5px;' >\n"
                            + "                                                <button type=\"button\" style='width:100%;background-color: #fbedc6;border: 1px solid #dbbd64;' class=\"btn\" onclick=\"load_btn(" + i_count + ")\" data-book-id=\"44444\" name='chq' id='chq_" + i_count + "' data-toggle=\"modal\">CC</button>\n"
                            + "                                                <div class='align-center' id='icondiv_" + i_count + "'></div></td> </td><td style='vertical-align: top;padding: 5px;'><input type='text' id='jdebit" + i_count + "' style='width:100%;border: 1px solid #2196f3;text-align:right; ' name='jdebit' class='currency form-control' readonly data-v-min='0' data-m-dec='2'  data-d-group='2'/><table class='tbl' id='debitbill" + i_count + "' ></table></td>\n"
                            + "                                            <td style='vertical-align: top;padding: 5px;'><input type='text'  id='jcredit" + i_count + "' name='jcredit' style='width:100%;border: 1px solid #2196f3;text-align:right; ' class='currency form-control' readonly data-v-min='0' data-m-dec='2'  data-d-group='2'/> <table class='tbl' id='creditbill" + i_count + "' ></table></td><td style='vertical-align: top;padding: 5px;'><input type='text' style='width:100%;border: 1px  solid #67d3c9' class=\"form-control\" id=\"remark" + i_count + "'\" name='remark' aria-invalid=\"false\"/></td>\n"
                             + "<td></td></tr>";
                    data += "</tbody><tfoot align=\"right\"><tr><th style='border:0;'></th><th style='border:0;'></th><th style='border:0;'></th><th style='vertical-align: top;padding: 5px;'><input type='text' style='width:100%;border: 1px solid #9c27b0;text-align:right;' class='currency form-control' data-v-min=\"0\" data-m-dec=\"2\" data-d-group=\"2\" aria-invalid=\"false\" readonly name='totdebit' id='totdebit'/></th><th style='vertical-align: top;padding: 5px;'><input type='text' style='width:100%;border: 1px solid #9c27b0;text-align:right;' class='currency form-control' data-v-min=\"0\" data-m-dec=\"2\" data-d-group=\"2\" aria-invalid=\"false\" readonly  name='totcredit' id='totcredit' class='validate[required,equals[totdebit]]' /> </th></tr></tfoot></table></div></div>";
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("loadjtm")) {
                    data = " <select class=\"form-control show-tick selectpicker\" data-style=\"btn-col-teal-bg\" data-actions-box=\"true\" data-live-search=\"true\" name=\"jm_id\" id=\"jm_id\" onchange=\"loadtrans(-100)\" >";
                    con.read1("SELECT CONCAT('<option value=\"',jm.jm_id,'\">','[',jm.jm_id,'](',DATE_FORMAT(jm.doje,'%d-%m-%Y'),')',jm.desc,'</option>') AS opt FROM accounts.journal_master_" + request.getParameter("ay_id") + " jm INNER JOIN accounts.journal_trans_master_" + request.getParameter("ay_id") + " jtm ON jtm.jm_id=jm.jm_id AND jtm.status>0 INNER JOIN accounts.om_tt_lm_mapping otlm ON otlm.otlm_id=jtm.otlm_id AND otlm.status>0 WHERE jm.status=1 AND otlm.om_id='" + request.getParameter("om_id") + "'  GROUP BY jm.jm_id");
                    data += "<option value=\"\" selected><--New Transaction--></option>";
                    data += "<option data-divider=\"true\"></option>";
                    while (con.rs1.next()) {
                        data += con.rs1.getString("opt");
                    }
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("load_division")) {
                    data = "<option value='-1' selected disabled>--select--</option>";
                    con.read("SELECT om.om_id,om.title FROM accounts.organization_master om INNER JOIN accounts.acc_div_incharge adi ON om.om_id=adi.om_id AND adi.staff_id='" + session.getAttribute("ss_id") + "' AND adi.status=1 AND adi.div_type='accounts' AND om.status=1 AND om.accounting_division=1");
                    while (con.rs.next()) {
                        con.read1("SELECT om_id,title FROM accounts.organization_master WHERE FIND_IN_SET(om_id,(SELECT GROUP_CONCAT(a.tem) FROM (SELECT (SELECT @id :=parent_id FROM accounts.organization_master WHERE om_id IN(@id)) tem FROM (SELECT @id:=" + con.rs.getString("om_id") + ") tem JOIN accounts.organization_master ON @id <> 0 ) a)) AND parent_id=0");
                        if (con.rs1.next()) {
                            data += "<option value=\"" + con.rs.getString("om_id") + "\" data-omid=\"" + con.rs1.getString("om_id") + "\">" + con.rs.getString("title") + " - " + con.rs1.getString("title") + "</option>";
                        }
                    }
                    data += "</select>";
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("loadledger")) {
                    fin_subroutine fs = new fin_subroutine();
                    data = fs.loadledger(Integer.parseInt(request.getParameter("om_id")), 0, "", session.getAttribute("roles").toString(), 0);
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("delete_ledger")) {
                    con.update("UPDATE accounts.ledger_master SET STATUS=0 WHERE lm_id=" + request.getParameter("lm_id"));
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("edit_ledger")) {
                    int i = 1;
                    String lm = request.getParameter("lm_id");
                    con.read("SELECT acc_head FROM accounts.`ledger_master` WHERE lm_id=" + lm);
                    data = "<div class=\"table-responsive\">";
                    if (con.rs.next()) {
                        data += "<h4 align=\"center\"> Edit: Ledger -- " + con.rs.getString("acc_head") + " </h4>";
                    }
                    data += "<table class=\"table table-bordered table-striped table-hover \">";
                    con.read2("SELECT lm_id FROM accounts.lm_extradetails WHERE lm_id=" + lm);
                    if (con.rs2.next()) {
                        con.read("SELECT lem.lem_id,lem.field_type,lem.field_name,lem.value_type,lem.lem_order,IFNULL(led.lm_value,'')lm_value FROM (SELECT lem_id,`field_name`,`field_type`,`value_type`,`lem_order`,`status` FROM accounts.`lm_extramaster` WHERE STATUS>0)lem LEFT JOIN (SELECT led_id,lm_id,lem_id,lm_value FROM accounts.`lm_extradetails` WHERE lm_id=" + request.getParameter("lm_id") + "  AND STATUS>0)led ON lem.lem_id=led.lem_id ORDER BY lem.lem_order");
                        while (con.rs.next()) {
                            if (con.rs.getString("field_type").equalsIgnoreCase("text")) {
                                data += "<tr><td><b>" + con.rs.getString("field_name") + "</b></td><td><input type='text' id='ftype" + con.rs.getString("lem_id") + "' name='ftype_" + con.rs.getString("lem_id") + "' value='" + con.rs.getString("lm_value") + "' data-lm=\"" + lm + "\" ></td></tr>";
                            } else {
                                data += "<tr><td><b>" + con.rs.getString("field_name") + "</b></td><td><select  id='stype" + con.rs.getString("lem_id") + "' name='stype_" + con.rs.getString("lem_id") + "' data-lm=\"" + lm + "\"  ><option deselect >--Select--</option>";
                                con.read1("SELECT lim_id,lim_name,display,VALUE FROM accounts.lm_info_master WHERE lem_id='" + con.rs.getString("lem_id") + "' AND STATUS>0");
                                while (con.rs1.next()) {
                                    if (con.rs1.getString("display").equalsIgnoreCase("name")) {
                                        if (con.rs.getString("lm_value").equalsIgnoreCase(con.rs1.getString("lim_id"))) {
                                            data += "<option value=" + con.rs1.getString("lim_id") + " selected>" + con.rs1.getString("lim_name") + "</option>";
                                        } else {
                                            data += "<option value=" + con.rs1.getString("lim_id") + ">" + con.rs1.getString("lim_name") + "</option>";
                                        }
                                    } else {
                                        if (con.rs.getString("lm_value").equalsIgnoreCase(con.rs1.getString("lim_id"))) {
                                            data += "<option value=" + con.rs1.getString("lim_id") + " selected>" + con.rs1.getString("value") + "</option>";
                                        } else {
                                            data += "<option value=" + con.rs1.getString("lim_id") + ">" + con.rs1.getString("value") + "</option>";
                                        }
                                    }
                                }
                                data += "</select></td></tr>";
                            }
                            i++;
                        }
                        data += "<tr><td><input type='text' name='text' value='" + i + "' hidden><input type='hidden' name='lm' value=" + lm + "></td></tr></table>";
                    } else {
                        con.read("SELECT lem.lem_id,lem.field_type,lem.field_name,lem.value_type,lem.lem_order,IFNULL(led.lm_value,'')lm_value FROM (SELECT lem_id,`field_name`,`field_type`,`value_type`,`lem_order`,`status` FROM accounts.`lm_extramaster` WHERE STATUS>0)lem LEFT JOIN (SELECT led_id,lm_id,lem_id,lm_value FROM accounts.`lm_extradetails` WHERE lm_id=" + request.getParameter("lm_id") + "  AND STATUS>0)led ON lem.lem_id=led.lem_id ORDER BY lem.lem_order");
                        while (con.rs.next()) {
                            if (con.rs.getString("field_type").equalsIgnoreCase("text")) {
                                data += "<tr><td><b>" + con.rs.getString("field_name") + "</b></td><td><input type='text' id='ftype" + con.rs.getString("lem_id") + "'  name='ftype_" + con.rs.getString("lem_id") + "' data-lm=\"" + lm + "\" ></td></tr>";
                            } else {
                                data += "<tr><td><b>" + con.rs.getString("field_name") + "</b></td><td><select  id='stype" + con.rs.getString("lem_id") + "'  name='stype_" + con.rs.getString("lem_id") + "' data-lm=\"" + lm + "\" ><option deselect>--Select--</option>";
                                con.read1("SELECT lim_id,lim_name,display,VALUE FROM accounts.lm_info_master WHERE lem_id='" + con.rs.getString("lem_id") + "' AND STATUS>0");
                                while (con.rs1.next()) {
                                    if (con.rs1.getString("display").equalsIgnoreCase("name")) {
                                        data += "<option value=" + con.rs1.getString("lim_id") + ">" + con.rs1.getString("lim_name") + "</option>";
                                    } else {
                                        data += "<option value=" + con.rs1.getString("lim_id") + ">" + con.rs1.getString("value") + "</option>";
                                    }
                                }
                                data += "</select></td></tr>";
                            }
                            i++;
                        }
                        data += "<input type='hidden' name='lm' value=" + lm + "><input type='text' name='text' value=" + i + " hidden></table></div>";
                    }
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("update_ledger")) {
                    String ftype = request.getParameter("jsdata");
                    String lm = request.getParameter("lm_id");
                    JSONObject jobj;
                    JSONArray jsa = new JSONArray(ftype);
                    for (int i = 0; i < jsa.length(); i++) {
                        jobj = (JSONObject) jsa.get(i);
                        con.insert("INSERT INTO accounts.lm_extradetails(lm_id,lem_id,lm_value,STATUS,inserted_by,inserted_date,updated_by,updated_date) (SELECT '" + request.getParameter("lm_id") + "','" + jobj.getString("id") + "','" + jobj.getString("val") + "','1','" + session.getAttribute("ss_id") + "',NOW(),'" + session.getAttribute("ss_id") + "',NOW()) ON DUPLICATE KEY UPDATE  lm_value=VALUES(lm_value),STATUS=1,inserted_by=VALUES(inserted_by),inserted_date=VALUES(inserted_date),updated_by=' " + session.getAttribute("ss_id") + " ',updated_date=NOW()");
                    }
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("cost_create_journal")) {
                    fin_subroutine fs = new fin_subroutine();
                    data = fs.cost_center_journal(parseInt(request.getParameter("om_id")), 0, "", session.getAttribute("roles").toString(), 0);
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("cost_create_table")) {
                    fin_subroutine fs = new fin_subroutine();
                    data = fs.cost_center_table(parseInt(request.getParameter("om_id")), 0, "", session.getAttribute("roles").toString(), 0);
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("delete_cc")) {
                    con.update("UPDATE accounts.cc_master SET STATUS=0 WHERE ccm_id=" + request.getParameter("cc_id"));
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("save_cc")) {
                    con.insert("INSERT INTO accounts.cc_master(ccm_name,`ccm_desc`,parent_id,store_cc,account_cc,om_id,STATUS,inserted_by,inserted_date)VALUES('" + request.getParameter("cc_name") + "','" + request.getParameter("cc_desc") + "'," + request.getParameter("p_cc") + "," + request.getParameter("store_cc") + "," + request.getParameter("acc_cc") + "," + request.getParameter("om_id") + ",1,'" + session.getAttribute("ss_id") + "',NOW())");
                    data = "Inserted Successfully";
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("tt_staff_table")) {
                    fin_subroutine fs = new fin_subroutine();
                    data = fs.tt_staff_table(parseInt(request.getParameter("om_id")), session.getAttribute("roles").toString());
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("tt_staff_data")) {

                    data = "<div class=\"col-xs-12 col-sm-4 align-left \"><div class=\"form-group form-float\">"
                            + "<label class=\"form-label\" style=\"font-weight: normal;color:#aaa;\">Transaction Type</label>\n"
                            + "<div class=\"form-line\"> <select class=\"form-control selectpicker show-tick\" name='tt_staff' id='tt_staff' data-live-search=\"true\" required><option  style=\"display:none\"></option>\n";
                    con.read(" SELECT tt_id,tt_desc FROM accounts.transaction_type WHERE STATUS=1");
                    while (con.rs.next()) {
                        data += "<option value=\"" + con.rs.getString("tt_id") + "\">" + con.rs.getString("tt_desc") + "</option>";
                    }
                    data += "</select>";
                    data += "</div></div></div>\n"
                            + "<div class=\"col-xs-12 col-sm-4 align-left \">\n"
                            + "<div class=\"form-group form-float\">\n"
                            + "<label class=\"form-label\" style=\"font-weight: normal;color:#aaa;\">User</label>"
                            + "<div class=\"form-line\">\n"
                            + " <select  class=\"form-control selectpicker show-tick\" name ='staff' id ='staff' data-live-search=\"true\" required multiple >";
                    con.read("SELECT staff_id,CONCAT(first_name,' ',`middle_name`,' ',last_name) staffname FROM camps.staff_master  WHERE working_status='working'  AND sc_id IN(1,2)");
                    while (con.rs.next()) {
                        data += "<option value=\"" + con.rs.getString("staff_id") + "\">" + con.rs.getString("staff_id") + "-" + con.rs.getString("staffname") + "</option>";
                    }
                    data += "</select></div></div></div>"
                            + "<div class=\"col-xs-12 col-sm-4 align-center \" style=\"padding-top: 10px\"><button class=\"btn waves-effect btn-col-teal-bg\" type=\"button\" id=\"ttuserSave\"><bold>Save</bold></button> <button class=\"btn  waves-effect btn-col-teal-bg\" id=\"ttreset\" type=\"button\"><bold>Reset</bold></button></div></div>";
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("tt_staff_save")) {
                    String staff_id[] = request.getParameter("staff").split(",");
                    for (int i = 0; i < staff_id.length; i++) {
                        con.insert("INSERT IGNORE INTO accounts.staff_tt_mapping(tt_id,om_id,staff_id,STATUS,inserted_by,inserted_date)(SELECT tt.tt_id," + request.getParameter("om_id") + ",'" + staff_id[i] + "',1,'" + session.getAttribute("ss_id") + "',NOW() FROM accounts.transaction_type tt WHERE tt.tt_id IN(" + request.getParameter("tt_id") + "))");
                    }
                    data = "Inserted Successfully";
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("delete_staff_tt")) {
                    con.update("UPDATE accounts.staff_tt_mapping SET STATUS=0 WHERE stm_id=" + request.getParameter("id"));
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("ledger_tt_lm_map")) {
                    con.read("SELECT lm.lm_id,lm.acc_head,`accounts`.`getledgercode`(lm.`lm_id`) lm_code FROM accounts.ledger_master lm LEFT OUTER JOIN accounts.ledger_master lm1 ON lm.lm_id=lm1.parent_id WHERE lm1.parent_id IS NULL  AND lm.om_id=" + request.getParameter("om_id") + " AND lm.status>0 ");
                    while (con.rs.next()) {
                        data += "<option value=\"" + con.rs.getString("lm_id") + "\">" + con.rs.getString("lm_code") + "-" + con.rs.getString("acc_head") + "</option>";
                    }
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("staff_tt_lm_map")) {
                    con.read("SELECT staff_id,CONCAT(first_name,' ',`middle_name`,' ',last_name) staffname FROM camps.staff_master  WHERE working_status='working'  AND sc_id IN(1,2)");
                    while (con.rs.next()) {
                        data += "<option value=\"" + con.rs.getString("staff_id") + "\">" + con.rs.getString("staff_id") + "-" + con.rs.getString("staffname") + "</option>";
                    }
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("load_ledger_tt_map_report1")) {
                    fin_subroutine fs = new fin_subroutine();
                    data = fs.loadledger_tt_map1(Integer.parseInt(request.getParameter("om_id")), session.getAttribute("roles").toString(), Integer.parseInt(request.getParameter("tt")));
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("delete_tt_lm_map")) {
                    con.update("UPDATE accounts.om_tt_lm_mapping SET STATUS=0 WHERE otlm_id=" + request.getParameter("id"));
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("ledger_tt_mappping")) {
                    con.insert("INSERT INTO accounts.om_tt_lm_mapping(om_id,tt_id,lm_id,staff_id,TYPE,STATUS,inserted_by,inserted_date)( SELECT " + request.getParameter("dm_id") + "," + request.getParameter("tt_id") + ",lm_id,IFNULL(s.`staff_id`,'0'),'credit',1,'" + session.getAttribute("ss_id") + "',NOW() FROM accounts.ledger_master LEFT JOIN camps.staff_master s ON s.`staff_id` IN ('" + request.getParameter("staff2").replaceAll(",", "','") + "') WHERE lm_id IN(" + request.getParameter("lm_id2") + "))ON DUPLICATE KEY UPDATE STATUS=1,updated_by=VALUES(updated_by),updates_date=NOW() ");
                    con.insert("INSERT INTO accounts.om_tt_lm_mapping(om_id,tt_id,lm_id,staff_id,TYPE,STATUS,inserted_by,inserted_date)( SELECT " + request.getParameter("dm_id") + "," + request.getParameter("tt_id") + ",lm_id,IFNULL(s.`staff_id`,'0'),'debit',1,'" + session.getAttribute("ss_id") + "',NOW() FROM accounts.ledger_master LEFT JOIN camps.staff_master s ON s.`staff_id` IN ('" + request.getParameter("staff1").replaceAll(",", "','") + "') WHERE lm_id IN(" + request.getParameter("lm_id1") + "))ON DUPLICATE KEY UPDATE STATUS=1,updated_by=VALUES(updated_by),updates_date=NOW() ");
                    data = "Mapping done Successfully";
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("load_ledger_cc")) {
                    data = "<div class=\"col-xs-12 col-sm-4 align-left \"><div class=\"form-group form-float\">"
                            + "<label class=\"form-label\" style=\"font-weight: normal;color:#aaa;\">Ledger</label>\n"
                            + "<div class=\"form-line\"> <select class=\"form-control selectpicker show-tick\" id=\"ledger_cc_entry\" name=\"ledger_cc_entry\" data-live-search=\"true\" required><option  style=\"display:none\"></option>\n";
                    con.read("SELECT lm.lm_id,lm.acc_head FROM accounts.ledger_master lm LEFT OUTER JOIN accounts.ledger_master lm1 ON lm.lm_id=lm1.parent_id  WHERE lm1.parent_id IS NULL  AND lm.om_id=" + request.getParameter("om_id") + " AND lm.status>0");
                    while (con.rs.next()) {
                        data += "<option value=\"" + con.rs.getString("lm_id") + "\">" + con.rs.getString("acc_head") + "</option>";
                    }
                    data += "</select></div></div></div>\n"
                            + "<div class=\"col-xs-12 col-sm-4 align-left \">\n"
                            + "<div class=\"form-group form-float\">\n"
                            + "<label class=\"form-label\" style=\"font-weight: normal;color:#aaa;\">Cost Center</label>"
                            + "<div class=\"form-line\">\n"
                            + " <select  class=\"form-control selectpicker show-tick\" id=\"costcenter_cc_entry\" name=\"costcenter_cc_entry\" size=10 data-live-search=\"true\" required multiple >";
                    con.read("SELECT  pd.parent_id,cc.`ccm_name` AS parentname, GROUP_CONCAT(CONCAT(pd.ccm_id,':',REPLACE(pd.ccm_name,',',';'))) child FROM (SELECT ccm_id,ccm_name,parent_id,`status` FROM accounts.cc_master ccm  ORDER BY parent_id, ccm_id) pd JOIN (SELECT @pv:='1' FROM accounts.cc_master WHERE ccm_id='1' AND STATUS>0) ini JOIN accounts.cc_master cc ON cc.ccm_id=pd.parent_id AND cc.`status` >0  WHERE FIND_IN_SET(pd.parent_id, @pv) AND LENGTH(@pv := CONCAT(@pv, ',', pd.ccm_id)) AND pd.status>0 GROUP BY pd.parent_id");
                    while (con.rs.next()) {
                        data += "<optgroup label=\"" + con.rs.getString("parentname") + "\">";
                        String[] ch = con.rs.getString("child").split(",");
                        for (int i = 0; i < ch.length; i++) {
                            String[] chdat = ch[i].split(":");
                            data += "<option value=\"" + chdat[0] + "\">" + chdat[1] + "</option>";
                        }
                        data += "</optgroup>";
                    }
                    data += "</select></div></div></div>";
                    data += "<div class=\"col-xs-12 col-sm-4 align-left \">\n"
                            + "<div class=\"form-group form-float\">\n"
                            + "<label class=\"form-label\" style=\"font-weight: normal;color:#aaa;\">Staff</label>"
                            + "<div class=\"form-line\">\n"
                            + " <select  class=\"form-control selectpicker show-tick\" name='staff_id_cc' id='staff_id_cc' data-live-search=\"true\" required multiple >";
                    con.read("SELECT staff_id,CONCAT(first_name,' ',`middle_name`,' ',last_name) staffname FROM camps.staff_master  WHERE working_status='working'  AND sc_id IN(1,2)");
                    while (con.rs.next()) {
                        data += "<option value=\"" + con.rs.getString("staff_id") + "\">" + con.rs.getString("staff_id") + "-" + con.rs.getString("staffname") + "</option>";
                    }
                    data += "</select></div></div></div>";
                    data += "<div class=\"col-xs-12 col-sm-12 align-center \" style=\"padding-top: 10px\"><button class=\"btn waves-effect btn-col-teal-bg\" id=\"cclmSave\" type=\"button\" >Save</button> <button class=\"btn  waves-effect btn-col-teal-bg\" id=\"cclmreset\" type=\"button\">Reset</button></div>";
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("load_ledger_cc_table")) {
                    fin_subroutine fs = new fin_subroutine();
                    data = fs.loadledger_cc(Integer.parseInt(request.getParameter("om_id")), session.getAttribute("roles").toString());
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("add_cc_ledger")) {
                    // con.update("INSERT INTO accounts.om_cc_lm_mapping (om_id,ccm_id,lm_id,STATUS,inserted_by,inserted_date,updated_by,updated_date) SELECT '" + request.getParameter("dm_id") + "',cm.ccm_id,lm.lm_id,1,'" + ses.getAttribute("userId") + "',NOW(),'" + ses.getAttribute("userId") + "',NOW() FROM accounts.cc_master cm JOIN accounts.ledger_master lm WHERE lm.lm_id IN (" + request.getParameter("lm_id") + ") AND cm.ccm_id IN (" + request.getParameter("cc_id") + ") ON DUPLICATE KEY UPDATE STATUS=1, updated_by=values(updated_by),updated_date=now()");
                    con.update("INSERT INTO accounts.om_cc_lm_mapping (om_id,ccm_id,lm_id,staff_id,STATUS,inserted_by,inserted_date,updated_by,updated_date) SELECT '" + request.getParameter("om_id") + "',cm.ccm_id,lm.lm_id,IFNULL(s.`staff_id`,'0'),1,'" + session.getAttribute("ss_id") + "',NOW(),'" + session.getAttribute("ss_id") + "',NOW() FROM accounts.cc_master cm JOIN accounts.ledger_master lm LEFT JOIN camps.staff_master s ON s.`staff_id` IN('" + request.getParameter("staff").replaceAll(",", "','") + "') WHERE lm.lm_id IN (" + request.getParameter("lm_id") + ") AND cm.ccm_id IN (" + request.getParameter("cc_id") + ") ON DUPLICATE KEY UPDATE STATUS=1, updated_by=values(updated_by),updated_date=now()");
                    data = "Submited Successfully";
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("delete_oclm")) {
                    con.update("UPDATE accounts.om_cc_lm_mapping oclm SET oclm.status=0,oclm.updated_by='" + session.getAttribute("ss_id") + "',oclm.updated_date=NOW() WHERE oclm.oclm_id='" + request.getParameter("id") + "'");
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("dyna_dataload")) {
                    con.read("SELECT lm.lm_id,lm.acc_head FROM accounts.ledger_master lm LEFT OUTER JOIN accounts.ledger_master lm1 ON lm.lm_id=lm1.parent_id  WHERE lm1.parent_id IS NULL  AND lm.om_id=3 AND lm.status>0 limit " + request.getParameter("n") + ",200");
                    while (con.rs.next()) {
                        data += "<p>" + con.rs.getString("lm_id") + "-" + con.rs.getString("acc_head") + "</p>";
                    }
                }
                out.print(data);
            } catch (Exception e) {
                out.print(e);
            } finally {
                try {
                    con.closeConnection();
                } catch (SQLException ex) {
                }
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (JSONException ex) {
            Logger.getLogger(fin_accounts.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (JSONException ex) {
            Logger.getLogger(fin_accounts.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
