/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CAMPS.curriculum;

import CAMPS.Common.HeaderAndFooter;
import CAMPS.Common.HeaderFooterwithPageNo;
import CAMPS.Connect.DBConnect;
import CAMPS.studentAcad.student_list;
import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;   

/**
 *
 * @author Administrator
 */
public class curriculum_report extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        DBConnect db = new DBConnect();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        PrintWriter out = response.getWriter();
        try {
            db.getConnection();
            if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("load_branch")) {
                db.read("SELECT bm.branch_id,CONCAT(IFNULL(mp.programme_code,''),' ',bm.branch_name) branch_name FROM camps.master_branch bm INNER JOIN camps.master_programme mp ON mp.programme_id=bm.programme_id  WHERE bm.status=1 AND mp.programme_level in(" + request.getParameter("degree_level") + ")");
                while (db.rs.next()) {
                    out.print("<option value=\"" + db.rs.getString("branch_id") + "\">" + db.rs.getString("branch_name") + "</option>");
                }
            } else if (request.getParameter("curriculum_report") != null && request.getParameter("curriculum_report").equalsIgnoreCase("report")) {
                response.reset();
                response.setContentType("application/pdf");
                response.setHeader("Content-disposition", "attachment;filename=curriculum_report.pdf");
                try (ServletOutputStream sos = response.getOutputStream()) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    Document document = new Document();
                    PdfWriter writer = PdfWriter.getInstance(document, baos);
                    writer.setPageEvent(new HeaderFooterwithPageNo("", "Report generated on " +timestamp + ""));    
                    document.open();
                    document.setPageSize(PageSize.A4);
                    document.newPage();
                    String branch_id[] = request.getParameterValues("branch_id");
                    String branch = "0";
                    if (request.getParameterValues("branch_id") != null) {
                        for (String branch1 : branch_id) {
                            branch += ",'" + branch1 + "'";
                        }
                    }
                    String prg = "", s = "", s1 = "<html><body>";
                    int count = 0;
                    db.read1("SELECT CONCAT(mp.programme_code,' ',mb.branch_name) prg,rm.regulation_id reg ,regulation_name FROM curriculum.regulation_master rm INNER JOIN camps.master_branch mb ON mb.branch_id=rm.branch_id INNER JOIN camps.master_programme mp ON mp.programme_id=mb.programme_id WHERE rm.branch_id in(" + branch + ") AND rm.regulation_name=" + request.getParameter("regulation") + "");
                    while (db.rs1.next()) {
                        prg = "Curriculum- " + db.rs1.getString("regulation_name") + "-" + db.rs1.getString("prg") + "";
                        if (count == 0) {
                            s += "<table align=\"center\"><tr><td><h4>" + prg + "</h4></td></tr></table>";
                        } else {
                            s += "<table style=\"page-break-before: always;\" align=\"center\"><tr><td><h4>" + prg + "</h4></td></tr></table>";
                        }
                        db.read("SELECT CONCAT('<table align=\"left\"><tr><td  align=\"center\"><b>',IF(period<>'-',CONCAT('Semester',' ',period), 'Electives'),'</b></td></tr></table><table align=\"left\" border=1 style=\"border-collapse:collapse;\"><tr ><td align=\"center\"><b>Subject Code</b></td><td align=\"center\"><b>Subject Name</b></td></tr><tr><td>',GROUP_CONCAT(sub_code,'</td><td>',sub_name ORDER BY IF(period='-','viiii',period),order_no,sub_code SEPARATOR '</td></tr><tr><td>'),'</td></tr></table>' ) sub_code FROM ( (SELECT rsm.subject_id, rsm.regulation_id, IFNULL(sm.dis_code, sm.sub_code) sub_code, sm.sub_name, IFNULL(mspd.period, '-') period, rsm.order_no, st.type_name FROM curriculum.regulation_master rm INNER JOIN curriculum.regulation_subject_mapping rsm ON rm.regulation_id = rsm.regulation_id AND rm.regulation_id = " + db.rs1.getString("reg") + " AND rsm.status > 0 INNER JOIN curriculum.subject_master sm ON rsm.subject_id = sm.subject_id INNER JOIN curriculum.subject_type st ON st.st_id = rsm.st_id LEFT JOIN camps.master_programme_period_det mspd ON mspd.prog_period_id = rsm.prog_period_id ORDER BY IF(period = '-', 'viiii', period), dis_code, order_no) UNION (SELECT em.elective_id subject_id, '' regulation_id, '' sub_code, em.elective_name sub_name, mspd.period period, em.order_no, st.type_name FROM curriculum.regulation_master rm INNER JOIN curriculum.elective_master em ON em.regulation_id = rm.regulation_id AND rm.regulation_id = " + db.rs1.getString("reg") + " INNER JOIN curriculum.subject_type st ON st.st_id = em.st_id LEFT JOIN camps.master_programme_period_det mspd ON mspd.prog_period_id = em.prog_period_id ORDER BY IF(period = '-', 'viiii', period), order_no) ) a GROUP BY IF(period = '-', 'viiii', period) ORDER BY IF(period='-','viiii',period),order_no,sub_code");
                        while (db.rs.next()) {
                            s += db.rs.getString("sub_code");
                        }
                        count++;
                    }
                    //s+="</table>";
                    s1 += s + "</body></html>";

                    try {
                        InputStream is = new ByteArrayInputStream(s1.getBytes());
                        XMLWorkerHelper.getInstance().parseXHtml(writer, document, is);
                    } catch (Exception ex) {
                        log(ex.toString());
                    }  
                    document.close();
                    response.setContentLength(baos.size());

                    baos.writeTo(sos);
                    sos.flush();

                }

            }

        } catch (Exception e) {
            out.print(e);
        } finally {
            try {
                db.closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(student_list.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
