/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CAMPS.Staff;

import CAMPS.Connect.DBConnect;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class leave_Details extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        DBConnect db = new DBConnect();
        try (PrintWriter out = response.getWriter()) {
            try {
                db.getConnection();
                if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("loadleave_det")) {
                    db.insert("INSERT INTO camps.staff_leave_count (SELECT a.* FROM (SELECT sm.staff_id,slm.slm_id,CASE WHEN slm.reset_period='Yearly' THEN IF(slc.effective_date IS NULL,CONCAT(YEAR(NOW()),'-01-01'),slc.effective_date) WHEN slm.reset_period='Monthly' THEN  IF(slc.effective_date IS NULL,CONCAT(YEAR(NOW()),'-',MONTH(NOW()),'-01'),slc.effective_date) END f,CASE WHEN slm.reset_period='Yearly' THEN IF(slc.effective_date IS NULL,CONCAT(YEAR(NOW()),'-12-31') ,slc.effective_date+ INTERVAL 12 MONTH - INTERVAL 1 DAY) WHEN slm.reset_period='Monthly' THEN  IF(slc.effective_date IS NULL,LAST_DAY(NOW()),slc.effective_date+ INTERVAL 12 MONTH - INTERVAL 1 DAY) END t,slc.leave_count,CASE WHEN slm.reset_period='Yearly' THEN YEAR(IF(slc.effective_date IS NULL,NOW(),slc.effective_date)) WHEN slm.reset_period='Monthly' THEN DATE_FORMAT(IF(slc.effective_date IS NULL,NOW(),slc.effective_date),'%M-%Y')  END period,1,'" + session.getAttribute("user_id") + "' i,NOW() it,'" + session.getAttribute("user_id") + "' u,NOW() ut FROM camps.staff_leave_master slm INNER JOIN camps.staff_leave_condition slc ON slm.slm_id=slc.slm_id AND IF(slc.effective_date IS NULL,TRUE,slc.effective_date<=DATE(NOW()))  INNER JOIN camps.staff_master sm ON sm.staff_id='" + session.getAttribute("ss_id") + "' AND CASE WHEN slm.gender='Male' THEN sm.gender='Male' WHEN slm.gender='Female' THEN sm.gender='Female' ELSE TRUE END)a LEFT JOIN camps.staff_leave_count slct ON slct.slm_id=a.slm_id AND slct.staff_id=a.staff_id AND slct.period_name=a.period WHERE slct.slm_id IS NULL)");
                    db.read("SELECT slm.slm_id,slm.leave_name,slc.leave_count,COUNT(count_data.slm_id)*0.5 leave_avalied,(slc.leave_count-COUNT(count_data.slm_id)*0.5) balance,ifnull(slm.count_query,'')count_query FROM staff_leave_master slm INNER JOIN staff_leave_count slc ON ((slc.slm_id=slm.slm_id AND NOW() BETWEEN slc.from_date AND slc.to_date  AND slc.status>0) OR slm.count_query IS NOT NULL) AND slm.status>0 LEFT JOIN (SELECT slr.slm_id,slr.from_date,slr.from_session,slr.to_date,slr.to_session,ct.dt,ses FROM camps.calendar_table ct JOIN (SELECT 'FN' ses,1 orn UNION SELECT 'AN',2)ses INNER JOIN staff_leave_request slr ON ct.dt BETWEEN DATE(slr.from_date) AND DATE(slr.to_date) AND slr.status=1 AND slr.staff_id='" + session.getAttribute("ss_id") + "' AND (CASE WHEN DATE(slr.from_date)=ct.dt AND slr.from_session='AN' THEN slr.from_session=ses WHEN DATE(slr.to_date)=ct.dt AND slr.to_session='FN' THEN slr.to_session=ses ELSE TRUE END) ORDER BY ct.dt,orn) count_data ON count_data.dt BETWEEN slc.from_date AND slc.to_date AND slm.slm_id=count_data.slm_id where slc.staff_id='" + session.getAttribute("ss_id") + "' GROUP BY slm.slm_id");
                    String data = "<div class=\"body table-responsive\"><table width='100%' border='1' class=\"table table-condensed\" ><tr><th>Leave Type</th><th>Leave Authorized</th><th>Leave Availed</th><th>Balance</th></tr>";
                    while (db.rs.next()) {
                        if (db.rs.getString("count_query").equalsIgnoreCase("")) {
                            data += "<tr><td>" + db.rs.getString("leave_name") + "</td><td>" + db.rs.getString("leave_count") + "</td><td>" + db.rs.getString("leave_avalied") + "</td><td>" + db.rs.getString("balance") + "</td></tr>";
                        } else {
                            db.read1(db.rs.getString("count_query").replaceAll("__staff_id__", session.getAttribute("ss_id").toString()));
                            if (db.rs1.next()) {
                                data += "<tr><td>" + db.rs.getString("leave_name") + "</td><td>" + db.rs1.getString("leave_count") + "</td><td>-</td></tr>";
                            }
                        }
                    }
                    data += "</table></div>";
                    out.print(data);
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("load_leave_from_to")) {
                    db.read("SELECT slm.slm_id,slm.leave_name,slm.count_query,slc.leave_count,IFNULL(slm.form_model,'default') form_model,COUNT(count_data.slm_id)*0.5 leave_avalied,IF(slc.leave_count IS NULL,7, slc.leave_count-COUNT(count_data.slm_id)*0.5) remaining  FROM staff_leave_master slm LEFT JOIN staff_leave_count slc ON slc.slm_id=slm.slm_id AND NOW() BETWEEN slc.from_date AND slc.to_date AND slm.status>0 AND slc.status>0 LEFT JOIN (SELECT slr.slm_id,slr.from_date,slr.from_session,slr.to_date,slr.to_session,ct.dt,ses FROM camps.calendar_table ct JOIN (SELECT 'FN' ses,1 orn UNION SELECT 'AN',2)ses INNER JOIN staff_leave_request slr ON ct.dt BETWEEN DATE(slr.from_date) AND DATE(slr.to_date) AND slr.status=2 AND slr.staff_id='" + session.getAttribute("ss_id") + "' AND (CASE WHEN DATE(slr.from_date)=ct.dt AND slr.from_session='AN' THEN slr.from_session=ses WHEN DATE(slr.to_date)=ct.dt AND slr.to_session='FN' THEN slr.to_session=ses ELSE TRUE END) ORDER BY ct.dt,orn) count_data ON count_data.dt BETWEEN slc.from_date AND slc.to_date AND slm.slm_id=count_data.slm_id WHERE (slc.staff_id='" + session.getAttribute("ss_id") + "' OR slm.reset_period IS NULL) AND slm.slm_id='" + request.getParameter("leave_type") + "' GROUP BY slm.slm_id HAVING (leave_count-leave_avalied)>0 OR leave_count IS NULL");
                    String data = "";
                    if (db.rs.next()) {
                        switch (db.rs.getString("form_model")) {
                            case "one_day":
                                data = "<div class=\"row clearfix\"><div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">"
                                        + "<div class=\"form-group form-float\">"
                                        + "<div class=\"form-line focused\">"
                                        + "<input type=\"text\" pattern=\"[0-9]{2}-[0-9]{2}-[0-9]{4}\" name=\"from_date\" id=\"from_date\"  class=\"form-control\"  />"
                                        + "<label class=\"form-label\">Date</label>"
                                        + "<script>$('#from_date').bootstrapMaterialDatePicker({format: 'DD-MM-YYYY', weekStart: 0, time: false});</script></div>"
                                        + "</div></div></div></div>";
                                data += "<div class=\"row clearfix\"><div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">"
                                        + "<div class=\"form-group form-float\">"
                                        + "<div class=\"form-line focused\">"
                                        + "<input type=\"text\"  name=\"Reason\" id=\"Reason\"  class=\"form-control\" />"
                                        + "<label class=\"form-label\">Reason</label>"
                                        + "</div>"
                                        + "</div></div></div>";
                                break;
                            case "one_session":
                                data = "<div class=\"row clearfix\"><div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-6\">"
                                        + "<div class=\"form-group form-float\">"
                                        + "<div class=\"form-line focused\">"
                                        + "<input type=\"text\" pattern=\"[0-9]{2}-[0-9]{2}-[0-9]{4}\" name=\"from_date\" id=\"from_date\"  class=\"form-control\"  />"
                                        + "<label class=\"form-label\">Date</label>"
                                        + "<script>$('#from_date').bootstrapMaterialDatePicker({format: 'DD-MM-YYYY', weekStart: 0, time: false});</script></div>"
                                        + "</div></div>";
                                data += "<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-6\">"
                                        + "<div class=\"form-group form-float\">"
                                        + "<div class=\"form-line focused\">"
                                        + "<select id='from_ses' name='from_ses' class=\"form-control\"><option value=''><--Select--></option> <option value='FN'>FN</option><option value='AN'>AN</option></select> "
                                        + "<label class=\"form-label\">Session</label>"
                                        + "</div>"
                                        + "</div></div></div>";
                                data += "<div class=\"row clearfix\"><div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">"
                                        + "<div class=\"form-group form-float\">"
                                        + "<div class=\"form-line focused\">"
                                        + "<input type=\"text\"  name=\"Reason\" id=\"Reason\"  class=\"form-control\" />"
                                        + "<label class=\"form-label\">Reason</label>"
                                        + "</div>"
                                        + "</div></div></div>";
                                break;
                            default:
                                data = "<div class=\"row clearfix\"><div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-6\">"
                                        + "<div class=\"form-group form-float\">"
                                        + "<div class=\"form-line focused\">"
                                        + "<input type=\"text\" pattern=\"[0-9]{2}-[0-9]{2}-[0-9]{4}\" name=\"from_date\" id=\"from_date\"  class=\"form-control\"  />"
                                        + "<label class=\"form-label\">From Date</label>"
                                        + "<script>$('#from_date').bootstrapMaterialDatePicker({format: 'DD-MM-YYYY', weekStart: 0, time: false});</script></div>"
                                        + "</div></div>";
                                data += "<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-6\">"
                                        + "<div class=\"form-group form-float\">"
                                        + "<div class=\"form-line focused\">"
                                        + "<select id='from_ses' name='from_ses' class=\"form-control\"><option value=''><--Select--></option> <option value='FN'>FN</option><option value='AN'>AN</option></select> "
                                        + "<label class=\"form-label\">From Session</label>"
                                        + "</div>"
                                        + "</div></div></div>";
                                data += "<div class=\"row clearfix\"><div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-6\">"
                                        + "<div class=\"form-group form-float\">"
                                        + "<div class=\"form-line focused\">"
                                        + "<input type=\"text\" pattern=\"[0-9]{2}-[0-9]{2}-[0-9]{4}\" name=\"to_date\" id=\"to_date\"  class=\"form-control\"  />"
                                        + "<label class=\"form-label\">To Date</label>"
                                        + "<script>$('#to_date').bootstrapMaterialDatePicker({format: 'DD-MM-YYYY', weekStart: 0, time: false});</script></div>"
                                        + "</div></div>";
                                data += "<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-6 \">"
                                        + "<div class=\"form-group form-float\">"
                                        + "<div class=\"form-line focused \">"
                                        + "<select id='to_ses' name='to_ses' class=\"form-control\"><option value=''><--Select--></option> <option value='FN'>FN</option><option value='AN'>AN</option></select>"
                                        + "<label class=\"form-label\">To Session</label>"
                                        + "</div>"
                                        + "</div></div></div>";
                                data += "<div class=\"row clearfix\"><div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">"
                                        + "<div class=\"form-group form-float\">"
                                        + "<div class=\"form-line focused\">"
                                        + "<input type=\"text\" name=\"Reason\" id=\"Reason\"  class=\"form-control\"  />"
                                        + "<label class=\"form-label\">Reason</label>"
                                        + "</div>"
                                        + "</div></div></div>";
                                data += "<div class=\"row clearfix\"><div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">"
                                        + "<div class=\"form-group form-float\">"
                                        + "<div class=\"form-line focused\">"
                                        + "<input type=\"text\"  name=\"days1\" id=\"days1\"  class=\"form-control\" onclick='nodays()' readonly />"
                                        + "<label class=\"form-label\">Days</label>"
                                        + "</div>"
                                        + "</div></div></div>";
                        }
                        if (db.rs.getString("count_query") != null) {
                            db.read1(db.rs.getString("count_query").replaceAll("__staff_id__", session.getAttribute("ss_id").toString()));
                            if (db.rs1.next()) {
                                data += "<div class=\"row clearfix\"><div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 \">"
                                        + "<div class=\"form-group form-float\">"
                                        + "<div class=\"form-line focused \">"
                                        + "<select id='reference' name='reference' class=\"form-control\">" + db.rs1.getString("opt") + "</select>"
                                        + "<label class=\"form-label\">Refered From</label>"
                                        + "</div>"
                                        + "</div></div></div>";
                            }
                        }
                        data += "<div><center><button class=\"btn bg-orange waves-effect\" type=\"button\" value=\"submit\" name=\"submit_leave\" onclick='nodays()'>Submit</button></center></div>    ";
                    }
                    out.print(data);
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("load_leave_status")) {
                    db.read("SELECT  slr.slr_id,slm.leave_name,CONCAT(DATE_FORMAT(slr.from_date,'%d-%m-%Y'),'-',slr.from_session) from_date,CONCAT(DATE_FORMAT(slr.to_date,'%d-%m-%Y'),'-',slr.to_session) to_date,GROUP_CONCAT(IF(slra.status=1,CONCAT('<td bgcolor=\" #908f78 \">',itm.incharge_name,' approval Pending</td>'),IF(slra.status=2,CONCAT('<td bgcolor=\" #76a77e \">',itm.incharge_name,' approved</td>'),CONCAT('<td bgcolor=\"#fd8e76 \">',itm.incharge_name,' declined</td>'))) ORDER BY slm.leave_name,slaf.level SEPARATOR '') leave_status FROM staff_leave_request slr INNER JOIN camps.staff_leave_master slm ON slr.slm_id=slm.slm_id INNER JOIN staff_leave_request_approval slra ON slra.slr_id=slr.slr_id AND slr.status in (1,3) INNER JOIN staff_leave_approval_flow slaf ON slaf.slaf_id=slra.slaf_id  INNER JOIN incharge_type_master itm ON itm.itm_id=slaf.itm_id WHERE slr.staff_id='" + session.getAttribute("ss_id") + "' GROUP BY slr.slr_id ORDER BY slm.leave_name,slaf.level");
                    String data = "<div class=\"body table-responsive\"><table class=\"table table-condensed\" width='100%' border='1'><tr><th>Leave Type</th><th>Leave From</th><th>Leave To</th><th>Class Alter</th></tr>";
                    while (db.rs.next()) {
                        data += "<tr><td>" + db.rs.getString("leave_name") + "</td><td>" + db.rs.getString("from_date") + "</td><td>" + db.rs.getString("to_date") + "</td><td></td>" + db.rs.getString("leave_status") + "<td><button type=\"button\" class=\"btn btn-danger waves-effect\" onclick='cancel_leave(" + db.rs.getString("slr.slr_id") + ")'>Cancel</button></td></tr>";
                    }
                    data += "</table></div>";
                    out.print(data);

                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("load_hod_leave")) {
                    int i = 1;
                    String data = "";
                    String check_level = "";
                    if (Integer.parseInt(request.getParameter("level")) > 1) {
                        int level = Integer.parseInt(request.getParameter("level")) - 1;
                        check_level = "INNER JOIN (camps.staff_leave_request_approval slra3 INNER JOIN camps.staff_leave_approval_flow slaf3 ) ON   slra3.slr_id=slra.slr_id   AND slra3.status=2  AND slaf3.slaf_id=slra3.slaf_id AND slaf3.level=" + level;
                    }
                    String ig_staff = "and sm.staff_id not in('0'";
                    String query = "SELECT slr.slr_id,slaf.slaf_id,sm.staff_id,CONCAT(sm.first_name,'',sm.middle_name,'',sm.last_name,'-',sm.staff_id) staff_name,slm.leave_name,CONCAT(DATE_FORMAT(slr.from_date,'%d-%m-%Y'),'-',slr.from_session) from_date,CONCAT(DATE_FORMAT(slr.to_date,'%d-%m-%Y'),'-',slr.to_session) to_date,IFNULL(slr.reason,'') reason FROM camps.staff_leave_request slr INNER JOIN camps.staff_leave_request_approval slra ON slr.slr_id=slra.slr_id AND slr.status=1 AND slra.status=1 INNER JOIN staff_leave_approval_flow slaf ON slaf.slaf_id=slra.slaf_id AND slaf.level=" + request.getParameter("level") + " and slaf.status=1 INNER JOIN staff_master sm ON sm.staff_id=slr.staff_id INNER JOIN camps.staff_leave_master slm ON slm.slm_id=slr.slm_id " + check_level + " WHERE slr.staff_id IN ";  
                    //db.read(query + "(SELECT imm.member_id FROM camps.incharge_member_mapping imm WHERE imm.incharge_staff_id=1003 AND imm.itm_id=2 AND imm.status>0)");
                    db.read(query + "(SELECT imm.member_id FROM camps.incharge_member_mapping imm INNER JOIN staff_leave_approval_flow sla ON sla.itm_id=imm.itm_id AND sla.level=" + request.getParameter("level") + " WHERE imm.incharge_staff_id='" + session.getAttribute("ss_id") + "'  AND imm.status>0)");
                    data = "<div class=\"body table-responsive\"><table class=\"table table-condensed\" width='100%' border='1'><tr><th>S No</th><th>Staff Name</th><th>Leave Type</th><th>Leave From</th><th>Leave To</th><th>Reason</th><th>Alter Details</th><th></th></tr>";
                    while (db.rs.next()) {
                        data += "<tr><td>" + i++ + "</td><td>" + db.rs.getString("staff_name") + "</td><td>" + db.rs.getString("leave_name") + "</td><td>" + db.rs.getString("from_date") + "</td><td>" + db.rs.getString("to_date") + "</td><td>" + db.rs.getString("reason") + "</td><td></td><td><button type=\"button\" class=\"btn btn-success waves-effect\" onclick='approve_leave(" + db.rs.getString("slr.slr_id") + "," + db.rs.getString("slaf.slaf_id") + "," + request.getParameter("level") + ")'>Approve</button>&nbsp;&nbsp;&nbsp;<button type=\"button\" class=\"btn btn-danger waves-effect\" onclick='decline_leave(" + db.rs.getString("slr.slr_id") + "," + db.rs.getString("slaf.slaf_id") + "," + request.getParameter("level") + ")'>Decline</button></td></tr>";
                        ig_staff += ",'" + db.rs.getString("staff_id") + "'";
                    }
                    ig_staff += ")";
                    db.read("SELECT itm.member_query  FROM camps.incharge_type_master itm inner join camps.staff_leave_approval_flow slaf on slaf.itm_id=itm.itm_id WHERE slaf.level=" + request.getParameter("level") + " having itm.member_query is not null");
                    if (db.rs.next()) {
                        db.read1(query + db.rs.getString("member_query").replaceAll("__department_id__", session.getAttribute("depId").toString()).replaceAll("__staff_id__", session.getAttribute("ss_id").toString()) + ig_staff);
                        while (db.rs1.next()) {
                            data += "<tr><td>" + i++ + "</td><td>" + db.rs1.getString("staff_name") + "</td><td>" + db.rs1.getString("leave_name") + "</td><td>" + db.rs1.getString("from_date") + "</td><td>" + db.rs1.getString("to_date") + "</td><td>" + db.rs1.getString("reason") + "</td><td></td><td><button type=\"button\" class=\"btn btn-success waves-effect\" onclick='approve_leave(" + db.rs1.getString("slr.slr_id") + "," + db.rs1.getString("slaf.slaf_id") + "," + request.getParameter("level") + ")'>Approve</button>&nbsp;&nbsp;&nbsp;<button type=\"button\" class=\"btn btn-danger waves-effect\" onclick='decline_leave(" + db.rs1.getString("slr.slr_id") + "," + db.rs1.getString("slaf.slaf_id") + "," + request.getParameter("level") + ")'>Decline</button></td></tr>";
                        }
                    }
                    data += "</table></div>";
                    out.print(data);

                }
                if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("submit_leave1")) {
                    String leave_id = db.insertAndGetAutoGenId("INSERT ignore INTO camps.staff_leave_request (slr_id,slm_id,staff_id,from_date,to_date,from_session,to_session,reason,STATUS,inserted_by,inserted_date,updated_by,updated_date) VALUES (null,'" + request.getParameter("leave_type") + "','" + session.getAttribute("ss_id") + "',STR_TO_DATE('" + request.getParameter("from_date") + "','%d-%m-%Y'),STR_TO_DATE('" + request.getParameter("to_date") + "','%d-%m-%Y'),'" + request.getParameter("from_ses") + "','" + request.getParameter("to_ses") + "','" + request.getParameter("reason") + "',1,'" + session.getAttribute("user_id") + "',    now(),     '" + session.getAttribute("user_id") + "',  now()) on duplicate key update status=1,updated_by='" + session.getAttribute("user_id") + "',updated_date=now()");
                    if (db.iEffectedRows != 0) {
                        db.insert("INSERT ignore INTO camps.staff_leave_request_approval (SELECT " + leave_id + ",slaf.slaf_id,1,'" + session.getAttribute("user_id") + "',    now(),     '" + session.getAttribute("user_id") + "',  now() FROM camps.staff_leave_approval_flow slaf WHERE slaf.slm_id='" + request.getParameter("leave_type") + "' AND slaf.status=1) on duplicate key update status=1,updated_date=now(),updated_by=values(updated_by)");
                        out.print(1);
                    } else {
                        out.print(0);
                    }
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("cancel_leave")) {
                    db.update("UPDATE camps.staff_leave_request slr set slr.status=0,slr.updated_by='" + session.getAttribute("user_id") + "',slr.updated_date=NOW() where slr.slr_id=" + request.getParameter("leave_id") + "");
                    //db.update("UPDATE camps.staff_leave_request_approval slrp set slrp.status=0,slrp.updated_by='" + session.getAttribute("user_id") + "',slrp.updated_date=NOW() where slrp.slr_id=" + request.getParameter("leave_id") + "");
                    out.print(db.iEffectedRows > 0 ? 1 : 0);
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("approve_leave")) {
                    db.update("UPDATE camps.staff_leave_request_approval slaf set slaf.status=2,slaf.updated_by='" + session.getAttribute("user_id") + "',slaf.updated_date=NOW() where slaf.slaf_id=" + request.getParameter("slaf_id") + " and slaf.slr_id=" + request.getParameter("leave_id") + "");
                    db.read("SELECT MAX(slaf.level) max_level FROM camps.staff_leave_request slr INNER JOIN camps.staff_leave_approval_flow slaf ON slr.slm_id=slaf.slm_id WHERE slr.slr_id=" + request.getParameter("leave_id") + " having max_level=" + request.getParameter("level"));
                    if (db.rs.next()) {
                        db.update("UPDATE camps.staff_leave_request slr set slr.status=2,slr.updated_by='" + session.getAttribute("user_id") + "',slr.updated_date=NOW() where slr.slr_id=" + request.getParameter("leave_id") + "");
                    }
                    out.print(db.iEffectedRows > 0 ? 1 : 0);
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("decline_leave")) {  
                    db.update("UPDATE camps.staff_leave_request_approval slaf set slaf.status=3,slaf.updated_by='" + session.getAttribute("user_id") + "',slaf.updated_date=NOW() where slaf.slaf_id=" + request.getParameter("slaf_id") + " and slaf.slr_id=" + request.getParameter("leave_id") + "");    
                    db.update("UPDATE camps.staff_leave_request slr set slr.status=3,slr.updated_by='" + session.getAttribute("user_id") + "',slr.updated_date=NOW() where slr.slr_id=" + request.getParameter("leave_id") + "");

                    out.print(db.iEffectedRows > 0 ? 1 : 0);
                }
                else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("load_leave_report")) {
                    String staff_id="";
                   db.read1("SELECT DISTINCT(fia.fia_id) fia_id FROM admin.form_ia_role fir INNER JOIN admin.form_item_access fia ON fia.fia_id=fir.fia_id WHERE fir.role_id IN (" + session.getAttribute("roles") + ")  AND fia.fia_id in(5) AND fia.status>0 AND fir.status>0");
                   if(db.rs1.next())
                    {
                      staff_id="and slr.staff_id in ("+request.getParameter("staff_id")+")";
                    }
                   else
                   {
                       staff_id="and slr.staff_id in ("+session.getAttribute("ss_id")+")";
                   }
                    db.read("SELECT sm.staff_id,CONCAT(sm.first_name,'',sm.middle_name,'',sm.last_name) staff_name,md.dept_name,slm.leave_name,CONCAT(DATE_FORMAT(slr.from_date,'%d-%m-%Y'),'-',slr.from_session) from_date,CONCAT(DATE_FORMAT(slr.to_date,'%d-%m-%Y'),'-',slr.to_session) to_date,IFNULL(slr.reason,'') reason,IF(slr.status=1,'Pending',IF(slr.status=2,'Approved','Declined')) leave_status FROM camps.staff_leave_request slr INNER JOIN staff_leave_master slm ON slr.slm_id=slm.slm_id INNER JOIN staff_master sm ON sm.staff_id=slr.staff_id inner join camps.master_department md on md.department_id=sm.department_id WHERE slr.slm_id IN ("+request.getParameter("leave_type")+") AND slr.status IN ("+request.getParameter("approval_type")+") AND (( slr.from_date BETWEEN str_to_date('"+request.getParameter("from_date")+"','%d-%m-%Y') AND str_to_date('"+request.getParameter("to_date")+"','%d-%m-%Y')) OR ( slr.to_date BETWEEN  str_to_date('"+request.getParameter("from_date")+"','%d-%m-%Y') AND str_to_date('"+request.getParameter("to_date")+"','%d-%m-%Y'))) "+staff_id+"  order by sm.department_id,sm.staff_id,slm.leave_name ");
                    String data = "  <table class=\"table table-bordered table-striped table-hover dataTable js-exportable\"><thead><tr><td>Staff ID</td><td>Staff Name</td><td>Department</td><td>Leave Type</td><td>From</td><td>To</td><td>Reason</td><td>Status</td></tr></thead> <tfoot><tr><td>Staff ID</td><td>Staff Name</td><td>Department</td><td>Leave Type</td><td>From</td><td>To</td><td>Reason</td><td>Status</td></tr></tfoot><tbody>";
                    while (db.rs.next()) {
                        data += "<tr><td>" + db.rs.getString("staff_id") + "</td><td>" + db.rs.getString("staff_name") + "</td><td>" + db.rs.getString("dept_name") + "</td><td>" + db.rs.getString("leave_name") + "</td><td>" + db.rs.getString("from_date") + "</td><td>" + db.rs.getString("to_date") + "</td><td>" + db.rs.getString("reason") + "</td><td>" + db.rs.getString("leave_status") + "</td></tr>";
                    }
                    data += "</tbody></table>";
                    out.print(data);

                }

            } catch (Exception e) {
                out.print(e);
            } finally {
                try {
                    db.closeConnection();
                } catch (SQLException ex) {
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
