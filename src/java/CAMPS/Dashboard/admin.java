package CAMPS.Dashboard;
import CAMPS.Common.report_process;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
public class admin extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession ses = request.getSession();
        try (PrintWriter out = response.getWriter()) {
            if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("loadrole")) {
                 report_process rs=new report_process();
            ArrayList<String> attribute= new ArrayList<String>();
            attribute.add("1");
            out.print(rs.report_v1("1", attribute));
            }
            else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("loadresourse")) {
                 report_process rs=new report_process();
            ArrayList<String> attribute= new ArrayList<String>();
            attribute.add("1");
            out.print(rs.report_v1("2", attribute));
            }
            else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("loaduser")) {
                 report_process rs=new report_process();
            ArrayList<String> attribute= new ArrayList<String>();
            attribute.add("1");
            out.print(rs.report_v1("3", attribute));
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
