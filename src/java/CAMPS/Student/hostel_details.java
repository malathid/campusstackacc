/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CAMPS.Student;

import CAMPS.Connect.DBConnect;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class hostel_details extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        try (PrintWriter out = response.getWriter()) {
            DBConnect db = new DBConnect();
            String dataout = "";
            try {
                db.getConnection();
                if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("loadstudent_list")) {
                    db.read("SELECT sm.student_id,sp.roll_no,TRIM(CONCAT(IFNULL(sm.first_name,''),' ',IFNULL(sm.middle_name,''),' ',IFNULL(sm.last_name,''),' ')) NAME,CONCAT(hm.hostel_name,'-(Floor:',hrm.floor_no,', Room:',hrm.room_no,')') current FROM camps.student_master sm INNER JOIN camps.student_admission_master sam ON sam.student_id=sm.student_id INNER JOIN camps.student_promotion sp ON sam.sp_id=sp.sp_id LEFT JOIN (camps.hostel_room_occupancy hro INNER JOIN camps.hostel_room_master hrm ON hro.room_id=hrm.room_id INNER JOIN camps.hostel_master hm ON hm.hostel_id=hrm.hostel_id) ON sm.student_id=hro.student_id AND hro.status>0  WHERE sm.scholar='hosteller' AND sm.gender='" + request.getParameter("gender") + "' ORDER BY sm.student_id,sp.roll_no");
                    dataout = "<table width='100%'><tr><th>S No</th><th>Studnet ID</th><th>Roll No</th><th>Student Name</th><th>Current Room</th><th colspan='2'>New Room Allocation</th></tr>";
                    int i = 1;
                    String hnopt = "";
                    db.read1("SELECT GROUP_CONCAT('<option value=\"',hm.hostel_id,'\">',hm.hostel_name,'</option>') opt FROM hostel_master hm WHERE hm.gender='" + request.getParameter("gender") + "' AND hm.status>0");
                    if (db.rs1.next()) {
                        hnopt = db.rs1.getString("opt");
                    }
                    while (db.rs.next()) {
                        dataout += "<tr><td>" + i++ + "</td><td>" + db.rs.getString("student_id") + "</td><td>" + db.rs.getString("roll_no") + "</td><td>" + db.rs.getString("name") + "</td><td>" + db.rs.getString("current") + "</td><td><select class=\"form-control show-tick\" data-style=\"btn-warning\" id='hs_" + db.rs.getString("student_id") + "' onchange=\"loadroom('" + db.rs.getString("student_id") + "')\" ><option value=''><---Hostel---></option>" + hnopt + "</select></td><td><select id='rm_" + db.rs.getString("student_id") + "' data-style=\"btn-info\" class=\"form-control show-tick\" onchange=\"update_hostel('" + db.rs.getString("student_id") + "')\" ><option value=''><---Room---></option></select></td></tr>";
                    }
                    dataout += "</table><script>$.AdminBSB.select.activate();</script>";
                    out.print(dataout);
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("loadroom")) {
                    dataout="<option value=''><---Room---></option>";
                    db.read("SELECT hrm.floor_no,hrm.room_no,hrm.room_id,hrc.room_capacity,COUNT(hro.hro_id) occpu FROM camps.hostel_room_master hrm INNER JOIN camps.hostel_master hm ON hrm.hostel_id=hm.hostel_id  AND hm.hostel_id='" + request.getParameter("hostel_id") + "'  INNER JOIN camps.hostel_room_capacity hrc ON hrc.room_capacity_id=hrm.room_capacity_id AND hrc.status>0 LEFT JOIN camps.hostel_room_occupancy hro ON hro.room_id=hrm.room_id AND hro.status=1  GROUP BY hrm.room_no HAVING  occpu<hrc.room_capacity");
                    while (db.rs.next()) {
                        dataout += "<option value='" + db.rs.getString("room_id") + "'>Floor:" + db.rs.getString("floor_no") + "/Room:" + db.rs.getString("room_no") + "/Aval:" + (db.rs.getInt("room_capacity") - db.rs.getInt("occpu")) + "</option>";
                    }
                    out.print(dataout);
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("update_hostel")) {
                    db.update("UPDATE camps.hostel_room_occupancy hro INNER JOIN camps.hostel_room_occupancy_history hroh ON hro.hro_id=hroh.hro_id AND hro.status>0 AND hro.student_id='" + request.getParameter("student_id") + "' SET hro.status=0,hroh.check_out=DATE(NOW()),hro.updated_by='" + session.getAttribute("user_id") + "',hro.updated_date=NOW(),hroh.updated_by='" + session.getAttribute("user_id") + "',hroh.updated_date=NOW()");
                    String hro_id = db.insertAndGetAutoGenId("INSERT INTO camps.hostel_room_occupancy(room_id,member_type,student_id,STATUS,inserted_by,inserted_date,updated_by,updated_date) VALUES('" + request.getParameter("room_id") + "','Student','" + request.getParameter("student_id") + "',1,'" + session.getAttribute("user_id") + "',NOW(),'" + session.getAttribute("user_id") + "',NOW())");
                    db.insert("INSERT INTO camps.hostel_room_occupancy_history (hro_id,check_in,STATUS,inserted_by,inserted_date) VALUES('"+hro_id+"',DATE(NOW()),1,'" + session.getAttribute("user_id") + "',NOW())");
                    out.print(db.iEffectedRows>0?1:0);
                }
            } catch (Exception e) {
                out.print(e);
            } finally {
                try {
                    db.closeConnection();
                } catch (SQLException ex) {

                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
