/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CAMPS.studentAcad;

import CAMPS.Common.report_process;
import CAMPS.Connect.DBConnect;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class student_list extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        try (PrintWriter out = response.getWriter()) {
            DBConnect db = new DBConnect();
            try {
                db.getConnection();
                if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("loadSection")) {
                    db.read("SELECT IF(mpp.minimum_years=1,sp.section,CONCAT(mppd.year,' Yr ',mppd.period, ' Sem ' ,sp.section)) section FROM camps.student_promotion sp LEFT JOIN camps.master_programme_period_det mppd ON mppd.prog_period_id=sp.prog_period_id LEFT JOIN camps.master_programme_pattern mpp ON mpp.prog_pattern_id=mppd.prog_pattern_id  WHERE sp.status=1 AND sp.branch_id='" + request.getParameter("branch_id") + "' AND sp.term_id='" + request.getParameter("term_id") + "' GROUP BY mppd.year,mppd.period,sp.section ORDER BY mppd.year,mppd.period,sp.section");
                    out.print("<option value=\"\">-- Select Section --</option>");
                    while (db.rs.next()) {
                        out.print("<option value=\"" + db.rs.getString("section") + "\">" + db.rs.getString("section") + "</option>");
                    }
                }
                if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("loadSemeter")) {
                    db.read("SELECT CONCAT('<option value=\"',mt.term_id,'\" ', IF(mt.status=1,'selected',''),' >',mt.term_name,'</option>') val FROM camps.master_term mt WHERE mt.ay_id='" + request.getParameter("ay_id") + "' AND mt.status>0");
                    out.print("<option value=\"\">-- Select Section --</option>");
                    while (db.rs.next()) {
                        out.print(db.rs.getString("val"));
                    }
                }
                if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("loadStudent")) {
                    report_process rp = new report_process();
                    ArrayList<String> attribute = new ArrayList<>();
                    attribute.add(request.getParameter("ay_id"));
                    attribute.add(request.getParameter("term_id"));
                    attribute.add(request.getParameter("branch_id"));
                    attribute.add(request.getParameter("section").replaceAll(",", "','"));
                    out.print(rp.report_v1("7", attribute));
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("loadstaff_incharge")) {
                    db.read("select sm.staff_id,TRIM(CONCAT(IFNULL(sm.first_name,''),' ',IFNULL(sm.middle_name,''),' ',IFNULL(sm.last_name,''),' ')) staff_name from staff_master sm where sm.working_status='working' and sm.sc_id=1 and sm.department_id='"+request.getParameter("dept_id")+"'");
                    while (db.rs.next()) {
                        out.print("<option value='" + db.rs.getString("staff_id") + "'>" + db.rs.getString("staff_name") + "</option>");
                    }
                } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("insert_student_incharge")) {
                    String student_id[]=request.getParameter("student_id").split(",");
                    String student="'0'";
                    for(String student_id1:student_id){
                        student+=",'"+student_id1+"'";
                    }
                    db.insert("insert into camps.incharge_member_mapping (select '"+request.getParameter("incharge_id")+"','"+request.getParameter("staff_id")+"',sm.student_id,1,'"+session.getAttribute("user_id")+"',now(),'"+session.getAttribute("user_id")+"',now() from camps.student_master sm inner join camps.student_admission_master sam on sam.student_id=sm.student_id inner join camps.student_promotion sp on sp.sp_id=sam.sp_id where CAST(sm.student_id AS CHAR) in ("+student+") or sp.roll_no in ("+student+")) on duplicate key update incharge_staff_id=values(incharge_staff_id),status=1,updated_by=values(updated_by),updated_date=now()");
                    out.print(db.iEffectedRows > 0 ? 1 : 0);
                }else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("delete_student_incharge")) {
                    db.delete("update camps.incharge_member_mapping set status=0,updated_by='"+session.getAttribute("user_id")+"',updated_date=now() where member_id='"+request.getParameter("student_id")+"' and itm_id='"+request.getParameter("incharge_id")+"'");
                    out.print(db.iEffectedRows > 0 ? 1 : 0);
                 } else if (request.getParameter("option") != null && request.getParameter("option").equalsIgnoreCase("load_student_incharge")) {
                    report_process rp = new report_process();
                    ArrayList<String> attribute = new ArrayList<>();
                    attribute.add(request.getParameter("staff_id"));
                    attribute.add(request.getParameter("incharge_id"));
                    attribute.add(session.getAttribute("roles").toString());
                    out.print(rp.report_v1("10", attribute));
                }
            } catch (Exception e) {
                out.print(e);
            } finally {
                try {
                    db.closeConnection();
                } catch (SQLException ex) {
                    Logger.getLogger(student_list.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
