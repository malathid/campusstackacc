/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CAMPS.Common;

import CAMPS.Connect.DBConnect;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class form_process {

    public String form_v1(String fm_id, ArrayList<String> attribute) {
        DBConnect db = new DBConnect();
        String query = "", hiddendata_new_form = "", display_type = "", data = "", button = "";
        try {
            db.getConnection();
            db.read("SELECT fm.fm_id,fm.operation_type,fm.form_query,fm.action_url,fm.button_details FROM camps.form_master fm WHERE fm.status>0 AND fm.fm_id='" + fm_id + "'");
            if (db.rs.next()) {
                query = db.rs.getString("form_query");
//                display_type = db.rs.getString("display_type");
//                if (db.rs.getInt("replace_var_count") != attribute.size()) {
//                    return "";
//                }
                data += "<form id='form_" + db.rs.getString("fm_id") + "' method='post' action='" + db.rs.getString("action_url") + "'><input type='hidden' id='form_id' name='form_id' value='" + db.rs.getString("fm_id") + "' />";
                button += db.rs.getString("button_details");
            }
            for (int i = 1; i <= attribute.size(); i++) {
                query = query.replaceAll("__" + i + "__", attribute.get(i - 1));
                hiddendata_new_form += "<input type='hidden' name='filter_condition' value='" + attribute.get(i - 1) + "' /> ";
            }
            if (query != null && !"".equals(query)) {
                db.read(query);
                db.rs.next();
            }
            db.read1("SELECT fa_id,fa_name,fa_attribute,fa_type,fa_list_query,fa_validation,fa_function,script,fa_validation FROM camps.form_attribute fm WHERE fm.status>0 AND fm.fm_id='" + fm_id + "' order by fm.order_no");
            while (db.rs1.next()) {
                switch (db.rs1.getString("fa_type")) {
                    case "text":
                        data += "<div class=\"form-group form-float\">"
                                + "<div class=\"form-line\">"
                                + "<input type=\"text\" name=\"" + db.rs1.getString("fa_id") + "\" id=\"" + db.rs1.getString("fa_id") + "\" value='" + (db.rs.isAfterLast() ? "" : db.rs.getString(db.rs1.getString("fa_attribute"))) + "' class=\"form-control\" " + db.rs1.getString("fa_validation") + " />"
                                + "<label class=\"form-label\">" + db.rs1.getString("fa_name") + "</label>"
                                + "</div>"
                                + "</div>";
                        break;
                    case "label":
                        data += "<div class=\"form-group form-float\">"
                                + "<div class=\"form-line\">"
                                + "<input type=\"text\" name=\"" + db.rs1.getString("fa_id") + "\" id=\"" + db.rs1.getString("fa_id") + "\" readonly value='" + (db.rs.isAfterLast() ? "" : db.rs.getString(db.rs1.getString("fa_attribute"))) + "' class=\"form-control\" " + db.rs1.getString("fa_validation") + " />"
                                + "<label class=\"form-label\">" + db.rs1.getString("fa_name") + "</label>"
                                + "</div>"
                                + "</div>";
                        break;
                    case "date":
                        data += "<div class=\"form-group form-float\">"
                                + "<div class=\"form-line\">"
                                + "<input type=\"text\" pattern=\"[0-9]{2}-[0-9]{2}-[0-9]{4}\" name=\"" + db.rs1.getString("fa_id") + "\" id=\"" + db.rs1.getString("fa_id") + "\" value='" + (db.rs.isAfterLast() ? "" : db.rs.getString(db.rs1.getString("fa_attribute"))) + "' class=\"form-control\" " + db.rs1.getString("fa_validation") + " />"
                                + "<label class=\"form-label\">" + db.rs1.getString("fa_name") + "</label>"
                                + "<script>$('#" + db.rs1.getString("fa_id") + "').bootstrapMaterialDatePicker({format: 'DD-MM-YYYY', weekStart: 0, time: false});</script></div>"
                                + "</div>";
                        break;
                    case "select":
                        data += "<div class=\"form-group form-float\">"
                                + "<div class=\"form-line focused\">"
                                + " <select name=\"" + db.rs1.getString("fa_id") + "\" data-live-search=\"true\" id=\"" + db.rs1.getString("fa_id") + "\" class=\"form-control\" " + db.rs1.getString("fa_validation") + " " + db.rs1.getString("fa_function") + " ><option value=\"\"><--Select--></option>";
                        String list_query = db.rs1.getString("fa_list_query");
                        for (int i = 1; i <= attribute.size(); i++) {
                            list_query = list_query.replaceAll("__" + i + "__", attribute.get(i - 1));
                        }
                        db.read2(list_query);
                        while (db.rs2.next()) {
                            data += "<option value='" + db.rs2.getString("opt") + "'  " + (!db.rs.isAfterLast() && db.rs.getString(db.rs1.getString("fa_attribute")).equalsIgnoreCase(db.rs2.getString("opt")) ? " selected " : "") + "   >" + db.rs2.getString("dis") + "</option>";
                        }
                        data += "</select>"
                                + "<label class=\"form-label\">" + db.rs1.getString("fa_name") + "</label>"
                                + "</div>"
                                + "</div>";
                        break;
                    case "radio":
                        data += "<div class=\"form-group form-float\">"
                                + ""
                                + " ";
                        list_query = db.rs1.getString("fa_list_query");
                        for (int i = 1; i <= attribute.size(); i++) {
                            list_query = list_query.replaceAll("__" + i + "__", attribute.get(i - 1));
                        }
                        db.read2(list_query);
                        while (db.rs2.next()) {
                            data += "<input type=\"radio\" name=\"" + db.rs1.getString("fa_id") + "\" value='" + db.rs2.getString("opt") + "' id=\"" + db.rs1.getString("fa_id") + "_" + db.rs2.getString("opt") + "\" class=\"with-gap\" " + db.rs1.getString("fa_validation") + ">"
                                    + "<label for=\"" + db.rs1.getString("fa_id") + "_" + db.rs2.getString("opt") + "\" class=\"form-label\">" + db.rs2.getString("dis") + "</label>";
                        }
                        data += "</div>";
                }
                data += "<script>" + db.rs1.getString("script") + "</script>";
            }
            data += button + hiddendata_new_form + "</form>";
        } catch (Exception e) {
            return e.toString();
        } finally {

            try {
                db.closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(form_process.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return data;
    }
    public String form_grid_v1(String fm_id, ArrayList<String> attribute) {
        DBConnect db = new DBConnect();
        String query = "", hiddendata_new_form = "", data = "", refresh_function = "", action_url = "";
        try {
            db.getConnection();
            db.read("SELECT fm.fm_id,fm.operation_type,fm.form_query,fm.action_url,fm.button_details,IFNULL(refresh_function,'')refresh_function FROM camps.form_master fm WHERE fm.status>0 AND fm.fm_id='" + fm_id + "'");
            if (db.rs.next()) {
                query = db.rs.getString("form_query");
//                display_type = db.rs.getString("display_type");
//                if (db.rs.getInt("replace_var_count") != attribute.size()) {
//                    return "";
//                }

                refresh_function += db.rs.getString("refresh_function");
                action_url = db.rs.getString("action_url");

            }
            for (int i = 1; i <= attribute.size(); i++) {
                query = query.replaceAll("__" + i + "__", attribute.get(i - 1));
                refresh_function = refresh_function.replaceAll("__" + i + "__", attribute.get(i - 1));
                hiddendata_new_form += "<input type='hidden' name='filter_condition' value='" + attribute.get(i - 1) + "' /> ";
            }
            if (query != null && !"".equals(query)) {
                db.read(query);
            }
            data += "<form id='form_" + fm_id + "' method='post' action='" + action_url + "'><table width='100%' class='tbl' style=\"border-spacing: 2px;border-collapse: separate;\">";
            db.read1("SELECT fa_id,fa_name,fa_attribute,fa_type,fa_list_query,fa_validation,fa_function,script,fa_validation FROM camps.form_attribute fm WHERE fm.status>0 AND fm.fm_id='" + fm_id + "'");
            data += "<tr>";
            while (db.rs1.next()) {
                data += "<th>" + db.rs1.getString("fa_name") + "</th>";
            }
            data += "</tr>";
            int i = 0;
            while (db.rs.next()) {
                data += "<tr id='tr_"+fm_id+ "_" + db.rs.getString("id") + "'><form id='form_" + db.rs.getString("id") + "' method='post' action='" + action_url + "'><input type='hidden' id='form_id' name='form_id' value='" + fm_id + "' /><input type='hidden' id='update_id' name='update_id' value='" + db.rs.getString("id") + "' />";
                db.read1("SELECT fa_id,fa_name,fa_attribute,fa_type,fa_list_query,fa_validation,fa_function,script,fa_validation FROM camps.form_attribute fm WHERE fm.status>0 AND fm.fm_id='" + fm_id + "'");

                while (db.rs1.next()) {
                    data += "<td><div class=\"form-group form-float\"><div class=\"form-line\">";
                    switch (db.rs1.getString("fa_type")) {
                        case "text":
                            data += "<input type=\"text\" name=\"" + db.rs1.getString("fa_id") + "\" id=\"" + i + "_" + db.rs1.getString("fa_id") + "\" value='" + (db.rs.isAfterLast() ? "" : db.rs.getString(db.rs1.getString("fa_attribute"))) + "' class=\"form-control\" " + db.rs1.getString("fa_validation") + " />";
                            break;
                        case "label":
                            data += "<input type=\"text\" name=\"" + db.rs1.getString("fa_id") + "\" id=\"" + i + "_" + db.rs1.getString("fa_id") + "\" readonly value='" + (db.rs.isAfterLast() ? "" : db.rs.getString(db.rs1.getString("fa_attribute"))) + "' class=\"form-control\" " + db.rs1.getString("fa_validation") + " />";
                            break;
                        case "date":
                            data += "<input type=\"text\" pattern=\"[0-9]{2}-[0-9]{2}-[0-9]{4}\" name=\"" + db.rs1.getString("fa_id") + "\" id=\"" + i + "_" + db.rs1.getString("fa_id") + "\" value='" + (db.rs.isAfterLast() ? "" : db.rs.getString(db.rs1.getString("fa_attribute"))) + "' class=\"form-control\" " + db.rs1.getString("fa_validation") + " />"
                                    + "<script>$('#" + i + "_" + db.rs1.getString("fa_id") + "').bootstrapMaterialDatePicker({format: 'DD-MM-YYYY', weekStart: 0, time: false});</script>";
                            break;
                        case "select":
                            data += "<select name=\"" + db.rs1.getString("fa_id") + "\" id=\"" + i + "_" + db.rs1.getString("fa_id") + "\" data-live-search=\"true\" class=\"form-control\" " + db.rs1.getString("fa_validation") + " " + db.rs1.getString("fa_function") + " ><option value=\"\"><--Select--></option>";
                            db.read2(db.rs1.getString("fa_list_query"));
                            while (db.rs2.next()) {
                                data += "<option value='" + db.rs2.getString("opt") + "'  " + (!db.rs.isAfterLast() && db.rs.getString(db.rs1.getString("fa_attribute")).equalsIgnoreCase(db.rs2.getString("opt")) ? " selected " : "") + "   >" + db.rs2.getString("dis") + "</option>";
                            }
                            data += "</select>";
                            break;
                        case "radio":
                            data += "";
                            db.read2(db.rs1.getString("fa_list_query"));
                            while (db.rs2.next()) {
                                data += "<input type=\"radio\" name=\"" + db.rs1.getString("fa_id") + "\" value='" + db.rs2.getString("opt") + "' id=\"" + i + "_" + db.rs1.getString("fa_id") + "_" + db.rs2.getString("opt") + "\" class=\"with-gap\" " + db.rs1.getString("fa_validation") + ">"
                                        + "<label for=\"" + db.rs1.getString("fa_id") + "_" + db.rs2.getString("opt") + "\" class=\"form-label\">" + db.rs2.getString("dis") + "</label>";
                            }
                    }
                    data += "<script>" + db.rs1.getString("script") + "</script></div></div></td>";

                }
                i++;
                data += "<td><div class=\"form-group form-float\"><div class=\"\"><button class=\"btn btn-primary waves-effect\" name=\"edit\" id=\"edit_" + db.rs.getString("id") + "\" value='Update' onclick=\"edit_grid_" + fm_id + "('" + db.rs.getString("id") + "')\"  type=\"button\">Update</button> <button class=\"btn btn-primary waves-effect\" name=\"delete\" id=\"delete_" + db.rs.getString("id") + "\" value='Delete' onclick=\"delete_grid_" + fm_id + "('" + db.rs.getString("id") + "')\"  type=\"button\">Delete</button></form></div></div></td> </tr>";
            }
            data += "<tr id='tr_new_" + fm_id + "'><form id='form_new' method='post' action='" + action_url + "'><input type='hidden' id='form_id' name='form_id' value='" + fm_id + "' />";

            db.read1("SELECT fa_id,fa_name,fa_attribute,fa_type,fa_list_query,fa_validation,fa_function,script,fa_validation FROM camps.form_attribute fm WHERE fm.status>0 AND fm.fm_id='" + fm_id + "'");
            while (db.rs1.next()) {
                data += "<td><div class=\"form-group form-float\"><div class=\"form-line\">";
                switch (db.rs1.getString("fa_type")) {
                    case "text":
                        data += "<input type=\"text\" name=\"" + db.rs1.getString("fa_id") + "\" id=\"" + db.rs1.getString("fa_id") + "\" value='' class=\"form-control\" " + db.rs1.getString("fa_validation") + " />";
                        break;
                    case "label":
                        data += "<input type=\"text\" name=\"" + db.rs1.getString("fa_id") + "\" id=\"" + db.rs1.getString("fa_id") + "\" readonly value='' class=\"form-control\" " + db.rs1.getString("fa_validation") + " />";
                        break;
                    case "date":
                        data += "<input type=\"text\" pattern=\"[0-9]{2}-[0-9]{2}-[0-9]{4}\" name=\"" + db.rs1.getString("fa_id") + "\" id=\"" + db.rs1.getString("fa_id") + "\" value='' class=\"form-control\" " + db.rs1.getString("fa_validation") + " />"
                                + "<script>$('#" + db.rs1.getString("fa_id") + "').bootstrapMaterialDatePicker({format: 'DD-MM-YYYY', weekStart: 0, time: false});</script>";
                        break;
                    case "select":
                        data += "<select name=\"" + db.rs1.getString("fa_id") + "\" id=\"" + db.rs1.getString("fa_id") + "\" data-live-search=\"true\" class=\"form-control\" " + db.rs1.getString("fa_validation") + " " + db.rs1.getString("fa_function") + " ><option value=\"\"><--Select--></option>";
                        db.read2(db.rs1.getString("fa_list_query"));
                        while (db.rs2.next()) {
                            data += "<option value='" + db.rs2.getString("opt") + "'    >" + db.rs2.getString("dis") + "</option>";
                        }
                        data += "</select>";
                        break;
                    case "radio":
                        data += "";
                        db.read2(db.rs1.getString("fa_list_query"));
                        while (db.rs2.next()) {
                            data += "<input type=\"radio\" name=\"" + db.rs1.getString("fa_id") + "\" value='" + db.rs2.getString("opt") + "' id=\"" + db.rs1.getString("fa_id") + "_" + db.rs2.getString("opt") + "\" class=\"with-gap\" " + db.rs1.getString("fa_validation") + ">"
                                    + "<label for=\"" + db.rs1.getString("fa_id") + "_" + db.rs2.getString("opt") + "\" class=\"form-label\">" + db.rs2.getString("dis") + "</label>";
                        }
                }
                data += "<script>" + db.rs1.getString("script") + "</script></div></div></td>";
            }
            data += "<td ><div class=\"form-group form-float\"><div class=\"\"> " + hiddendata_new_form + "<button class=\"btn btn-primary waves-effect\" name=\"add\" id=\"add\" value='Add' onclick=\"add_grid_" + fm_id + "(" + fm_id + ")\"  type=\"button\">Add</button></div></div></td> </tr>";

            data += "</table></form><script> function edit_grid_" + fm_id + "(form){ if($('#tr_"+fm_id+ "_'+form +' :input').valid()){ var data_string=$('#tr_"+fm_id+ "_'+form +' :input,#tr_"+fm_id+ "_'+form +' :selected').serialize();$.ajax({   type: \"POST\",url: '" + action_url + "',data: data_string+'&option=edit_grid',success: function(data){ $('#result_process').load('../../CommonJSP/alert_message.jsp?am_c='+data);" + refresh_function + " }});}}function delete_grid_" + fm_id + "(form){ var data_string=$('#tr_"+fm_id+ "_'+form +' :input,#tr_"+fm_id+ "_'+form +' :selected').serialize(); $.ajax({   type: \"POST\",url: '" + action_url + "',data: data_string+'&option=delete_grid',success: function(data){$('#result_process').load('../../CommonJSP/alert_message.jsp?am_c='+data);" + refresh_function + " }});}function add_grid_" + fm_id + "(form){ if($('#tr_new_'+form +' :input').valid()){ var data_string=$('#tr_new_'+form +' :input,#tr_new select').serialize();$.ajax({   type: \"POST\",url: '" + action_url + "',data: data_string+'&option=add_grid',success: function(data){$('#result_process').load('../../CommonJSP/alert_message.jsp?am_c='+data);" + refresh_function + " }});}}</script>";
        } catch (Exception e) {
            return e.toString();
        } finally {

            try {
                db.closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(form_process.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return data;
    }

    public int form_Update_V1(HttpServletRequest request) {
        DBConnect db = new DBConnect();
        String query = "", display_type = "", data = "", button = "", filter_condition[];
        try {
            db.getConnection();
            db.read("SELECT insert_query FROM camps.form_master fm WHERE fm.fm_id='" + request.getParameter("form_id") + "'");
            if (db.rs.next()) {
                query = db.rs.getString("insert_query");
                db.read1("SELECT fa.fa_id,fa.fa_type FROM camps.form_attribute fa WHERE fa.fm_id='" + request.getParameter("form_id") + "' AND fa.status=2");
                while (db.rs1.next()) {
                    if (!request.getParameter(db.rs1.getString("fa_id")).equalsIgnoreCase("")) {

                        switch (db.rs1.getString("fa_type")) {
                            case "date":
                                query = query.replaceAll("__fa" + db.rs1.getString("fa_id") + "__", "STR_TO_DATE('" + request.getParameter(db.rs1.getString("fa_id")) + "','%d-%m-%Y')");

                                break;
                            default:
                                query = query.replaceAll("__fa" + db.rs1.getString("fa_id") + "__", request.getParameter(db.rs1.getString("fa_id")));
                                break;
                        }
                    } else {
                          switch (db.rs1.getString("fa_type")) {
                            case "date":
                                query = query.replaceAll("__fa" + db.rs1.getString("fa_id") + "__", "NULL");
                                break;
                            default:
                                query = query.replaceAll("'__fa" + db.rs1.getString("fa_id") + "__'", "NULL");
                                break;
                        }
                    }
                }
                filter_condition = request.getParameterValues("filter_condition");
                for (int i = 0; i < filter_condition.length; i++) {
                    query = query.replaceAll("__" + (i + 1) + "__", filter_condition[i]);
                }
                //query = query.replaceAll("__ses-user__",session.getAttribute("user_id").toString());
                db.update(query);
                return db.iEffectedRows > 0 ? 1 : 0;
            }
        } catch (Exception e) {
            return 0;
        } finally {

            try {
                db.closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(form_process.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    public int form_Grid_Add_V1(HttpServletRequest request, HttpSession session) {
        DBConnect db = new DBConnect();
        String query = "", display_type = "", data = "";
        String filter_condition[] = null;
        try {
            db.getConnection();
            db.read("SELECT QUERY FROM camps.form_query fq WHERE fq.fm_id='" + request.getParameter("form_id") + "' and fq.status>0 AND fq.type='insert'");
            if (db.rs.next()) {
                query = db.rs.getString("query");
                db.read1("SELECT fa.fa_id,fa.fa_type FROM camps.form_attribute fa WHERE fa.fm_id='" + request.getParameter("form_id") + "' and fa.status>0 ");
                while (db.rs1.next()) {
                    if (!request.getParameter(db.rs1.getString("fa_id")).equalsIgnoreCase("")) {
                        switch (db.rs1.getString("fa_type")) {
                            case "date":
                                query = query.replaceAll("__fa" + db.rs1.getString("fa_id") + "__", "STR_TO_DATE('" + request.getParameter(db.rs1.getString("fa_id")) + "','%d-%m-%Y')");

                                break;
                            default:
                                query = query.replaceAll("__fa" + db.rs1.getString("fa_id") + "__", request.getParameter(db.rs1.getString("fa_id")));
                                break;
                        }
                    } else {
                         switch (db.rs1.getString("fa_type")) {
                            case "date":
                                query = query.replaceAll("__fa" + db.rs1.getString("fa_id") + "__", "NULL");
                                break;
                            default:
                                query = query.replaceAll("'__fa" + db.rs1.getString("fa_id") + "__'", "NULL");
                                break;
                        }
                    }

                }
                filter_condition = request.getParameterValues("filter_condition");
                for (int i = 0; i < filter_condition.length; i++) {
                    query = query.replaceAll("__" + (i + 1) + "__", filter_condition[i]);
                }
                query = query.replaceAll("__ses-user__", session.getAttribute("user_id").toString());
                db.insert(query);
                return db.iEffectedRows > 0 ? 1 : 0;
            }
        } catch (Exception e) {
            return 0;
        } finally {

            try {
                db.closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(form_process.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    public int form_Grid_Update_V1(HttpServletRequest request) {
        DBConnect db = new DBConnect();
        String query = "", display_type = "", data = "", button = "";
        try {
            db.getConnection();
            db.read("SELECT insert_query FROM camps.form_master fm WHERE fm.fm_id='" + request.getParameter("form_id") + "'");
            if (db.rs.next()) {
                query = db.rs.getString("insert_query");
                db.read1("SELECT fa.fa_id,fa.fa_type FROM camps.form_attribute fa WHERE fa.fm_id='" + request.getParameter("form_id") + "' AND fa.status=2");
                while (db.rs1.next()) {
                    if (!request.getParameter(db.rs1.getString("fa_id")).equalsIgnoreCase("")) {
                        switch (db.rs1.getString("fa_type")) {
                            case "date":

                                query = query.replaceAll("__fa" + db.rs1.getString("fa_id") + "__", "STR_TO_DATE('" + request.getParameter(db.rs1.getString("fa_id")) + "','%d-%m-%Y')");

                                break;
                            default:
                                query = query.replaceAll("__fa" + db.rs1.getString("fa_id") + "__", request.getParameter(db.rs1.getString("fa_id")));
                                break;
                        }
                    } else {
                        switch (db.rs1.getString("fa_type")) {
                            case "date":
                                query = query.replaceAll("__fa" + db.rs1.getString("fa_id") + "__", "NULL");
                                break;
                            default:
                                query = query.replaceAll("'__fa" + db.rs1.getString("fa_id") + "__'", "NULL");
                                break;
                        }

                    }

                }
                query = query.replaceAll("__uid__", request.getParameter("update_id"));
                db.update(query);
                return db.iEffectedRows > 0 ? 1 : 0;
            }
        } catch (Exception e) {
            return 0;
        } finally {

            try {
                db.closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(form_process.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    public int form_Grid_Delete_V1(HttpServletRequest request, HttpSession session) {
        DBConnect db = new DBConnect();
        String query = "", display_type = "", data = "";
        String filter_condition[] = null;
        try {
            db.getConnection();
            db.read("SELECT QUERY FROM camps.form_query fq WHERE fq.fm_id='" + request.getParameter("form_id") + "' and fq.status>0 AND fq.type='delete'");
            if (db.rs.next()) {
                query = db.rs.getString("query");
                db.read1("SELECT fa.fa_id,fa.fa_type FROM camps.form_attribute fa WHERE fa.fm_id='" + request.getParameter("form_id") + "' and fa.status>0 ");
                while (db.rs1.next()) {
                    switch (db.rs1.getString("fa_type")) {
                        case "date":
                            query = query.replaceAll("__fa" + db.rs1.getString("fa_id") + "__", "STR_TO_DATE('" + request.getParameter(db.rs1.getString("fa_id")) + "','%d-%m-%Y')");
                            break;
                        default:
                            query = query.replaceAll("__fa" + db.rs1.getString("fa_id") + "__", request.getParameter(db.rs1.getString("fa_id")));
                            break;
                    }

                }
//                filter_condition=request.getParameterValues("filter_condition");
//                for(int i=0;i<filter_condition.length;i++){
//                    query = query.replaceAll("__"+(i+1)+"__", filter_condition[i]);
//                }
                query = query.replaceAll("__uid__", request.getParameter("update_id"));
                query = query.replaceAll("__ses-user__", session.getAttribute("user_id").toString());
                db.delete(query);
                return db.iEffectedRows > 0 ? 1 : 0;
            }
        } catch (Exception e) {
            return 0;
        } finally {

            try {
                db.closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(form_process.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

}
