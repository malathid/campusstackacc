<%-- 
    Document   : journal_entry
    Created on : Jun 20, 2019, 2:42:17 PM
    Author     : malathi
--%>
<%@include file="../../CommonJSP/pageHeader.jsp" %>
<script src="../../plugins/jquery-validation/jquery.validate.js" type="text/javascript"></script>
<script src="../../plugins/jquery-validation/additional-methods.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<script src="../../js/pages/CommonJSP/validation.js" type="text/javascript"></script>
<script src="../../plugins/autonumeric/autoNumeric.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        load_division();
        validate();

    });
    $(document).on('click', '.currency', function () {
        $(".currency").autoNumeric("init", {
            digitGroupSeparator: ',',
            decimalCharacter: '.',
        });
    });
    $(document).on('change', '.check', function () {
        if (!$(this).val() == "") {
            $(this).addClass('valueentered');
            var table = $('#example').DataTable();
        }
    });
    $(document).on('click', '[id=savecc]', function () {
        var myRows = {ccdat: []};
        var alignd = "";
        $("#ccdiv_" + $("#savecc").data('ad')).html("");
        var $target = $("#ccdiv_" + $("#savecc").data('ad'));
        $target.append($("<th>CostCenter</th><th>Amount</th>"));
        var th = new Array("CostCenter", "Amount", "ccid");
        $('#cctable>tbody>tr').each(function (j, tr) {
            if (j > 0) {
                var obj = {}, $tds = $(tr).find('td').children("input"), $row = $("<tr></tr>");
                for (var i = 0; i < th.length; i++) {
                    obj[th[i]] = $tds.eq(i).val();
                    if (i == 0) {
                        alignd = "left";
                    } else
                        alignd = "right";
                    if (i < 2) {
                        $row.append($("<td align='" + alignd + "'>" + $tds.eq(i).val() + "</td>")).appendTo($target);
                    }
                }
                myRows.ccdat.push(obj);
            }
        });
        $target.append($("<th>Total</th>")).append("<th style='text-align:right'>" + $('#cctable>tfoot>tr').find("input").val() + "</th>");
        //$("#ccdiv_1").html("<p>" + JSON.stringify(myRows) + "</p>");
//        var $target = $("#ccdiv_1");
//        $("#cctable tr").each(function () {
//            var $tdtr = $(this).children(),
//                    $row = $("<tr></tr>");
//            $row.append($tdtr.eq(0).clone()).append($tdtr.eq(1).clone()).appendTo($target);
//        });
        $('#myModal').modal('hide');

    });
    function load_cc(cc_id) {
        alert("ll");
        if ($("#jtm_" + cc_id).val() !== "" && $('#jm_detail>tbody>tr:last').attr('id') == cc_id) {

            var cloned = $('#jm_detail>tbody>tr:last').clone(true);
            var id = parseInt($('#jm_detail>tbody>tr:last').attr('id'));
            cloned.attr({id: id + 1});
            cloned.find("#jtm_" + id).removeAttr('onchange'); //.removeAttr('style').unbind();
            cloned.find("#jtm_" + id).attr({onchange: 'load_cc(' + (id + 1) + ');', id: "jtm_" + (id + 1)});
            cloned.find("#party_" + id).removeAttr('onchange'); //.removeAttr('style').unbind();
            cloned.find("#party_" + id).attr({onchange: 'load_cc(' + (id + 1) + ');', id: "party_" + (id + 1)});
            cloned.find("#chq_" + id).attr({onclick: 'load_detail(' + (id + 1) + ');', id: "chq_" + (id + 1)});
            cloned.find("#ccdiv_" + id).attr({onclick: 'load_detail(' + (id + 1) + ');', id: "ccdiv_" + (id + 1)});
            cloned.find("#chqdiv_" + id).attr({onclick: 'load_detail(' + (id + 1) + ');', id: "chqdiv_" + (id + 1)});
            cloned.find("#icondiv_" + id).attr({id: "icondiv_" + (id + 1)});
            cloned.find("#jdebit" + id).attr({id: "jdebit" + (id + 1), value: ''});
            cloned.find("#debitbill" + id).attr({id: "debitbill" + (id + 1), value: ''});
            cloned.find("#jcredit" + id).attr({id: "jcredit" + (id + 1), value: ''});
            cloned.find("#creditbill" + id).attr({id: "creditbill" + (id + 1), value: ''});
            cloned.find("#ccdiv_" + id).attr({id: "ccdiv_" + (id + 1)});
            $('#jm_detail>tbody').append(cloned);
            $('#ccdiv_' + (id + 1)).html("");
            $('#chqdiv_' + (id + 1)).html("");
            $('#icondiv_' + (id + 1)).html("");
            $("#jtm_" + (id + 1)).selectpicker('refresh');
            $("#jtm_" + (id)).selectpicker('refresh');


            // $('#jm_detail>tbody>tr:last>td:last').html("<img src=\"../../Images/cross.png\" onclick='deleterow(" + id + ")' class=\"delete_trans\">");

            //$("#viewcc_" + id).prop("disabled", false);

        } else {
            $('#otlm_' + cc_id).val($("#jtm_" + cc_id).find(':selected').data('credit'));
            $('#otlm_' + cc_id).attr('data-ad', $("#jtm_" + cc_id).find(':selected').data('ad'));
        }
        $("#jdebit" + cc_id).autoNumeric('destroy');
        $("#jcredit" + cc_id).autoNumeric('destroy');
        $("#jdebit" + cc_id).autoNumeric('init');
        $("#jcredit" + cc_id).autoNumeric('init');
        totcal();
        
        if ($("#jtm_" + cc_id).find(':selected').data('debit') == "") {
            $("#jdebit" + cc_id).attr('readonly', true);
        } else {
            $("#jdebit" + cc_id).attr('readonly', false);
        }
        if ($("#jtm_" + cc_id).find(':selected').data('credit') == "") {
            $("#jcredit" + cc_id).attr('readonly', true);
        } else {
            $("#jcredit" + cc_id).attr('readonly', false);
        }
        
//        if ($("#jtm_" + cc_id).find(':selected').data('debit') == "") {
//            $("#jdebit" + cc_id).attr('readonly', true);
//            if ($("#tt_id").val() <= 4 && $("#jtm_1").find(':selected').data('debit') == "") {
//                $("#jcredit" + cc_id).attr('readonly', true);
//            }
//        } else {
//            $("#jdebit" + cc_id).attr('readonly', false);
//        }
//        
//        if ($("#jtm_" + cc_id).find(':selected').data('credit') == "") {
//            $("#jcredit" + cc_id).attr('readonly', true);
//            if ($("#tt_id").val() <= 4 && $("#jtm_1").find(':selected').data('credit') == "") {
//                $("#jdebit" + cc_id).attr('readonly', true);
//            }
//        } else {
//            $("#jcredit" + cc_id).attr('readonly', false);
//        }
        var DataString = 'jtm_id=' + $('#jtm_' + cc_id).val() + '&om_id=' + $('#om_id').val() + '&id=' + cc_id + '&option=loadcc';
        $.ajax({url: "fin_accounts.do", data: DataString, type: "post",
            success: function (data) {
                $("#ccdiv_" + cc_id).html(data);
                 $("select").selectpicker('refresh');
            }
        });
    }
    function loadlm()
    {
        var DataString = 'om_id=' + $('#om_id').val() + '&tt_id=' + $('#tt_id').val() + '&option=loadlm';
        $.ajax({url: "fin_accounts.do", data: DataString, type: "post",
            success: function (data) {
                //  $('#tt_id option:not(:selected)').attr('disabled', true);
                $("#lmscript").html(data);
                $('select').selectpicker('refresh');
                if ($("#jtm_1").find("option").length == 2) {
                    $("#jtm_1").val($("#jtm_1 option").eq(1).val()).trigger('change');
                    $("#jtm_1").selectpicker('refresh');
                }
            }
        });
    }
    function load_btn(id) {
        var ad = id;
        $("#CCTab").html("");
        var DataString = 'option=cost_create_journal&om_id=' + $("#om_id").find('option:selected').data("omid");
        $.ajax({
            url: "fin_accounts.do", data: DataString, type: "post",
            success: function (data) {
                $("#CCTab").html(data);
                $("#savecc").data('ad', ad);

                $('#cc_select').selectpicker('refresh');

//                $('#CCTab .table').DataTable({
//                    "ordering": false,
//                    "processing": true,
//                    "lengthMenu": [[7, 10, -1], [7, 10, "All"]],
//                    "dom": 'lBfrtip',
//                    "responsive": true,
//                    "columnDefs": [{data: "cc_head",
//                            defaultContent: "",
//                            targets: 0,
//                            visible: true}, {data: "cc_name",
//                            targets: 1,
//                            visible: true}],
//                    "rowCallback": function (row, data, index) {
//                        if (index % 2 == 0) {
//                            $(row).removeClass('myodd myeven');
//                            $(row).addClass('myodd');
//                        } else {
//                            $(row).removeClass('myodd myeven');
//                            $(row).addClass('myeven');
//                        }
//                    },
//                });
            }
        });
        // $('input[name="bookId"]').val(id);
        $('#myModal').modal('show');
    }
    $(document).on('hide.bs.select', 'select[name^=cc_select]', function () {
        $('#cc_select :selected').each(function () {
            var dat = "<tr><td align=\"left\"><input type=\"text\" class=\"form-control\" style='width:100%;border: 1px solid #2196f3;' name='ccname' readonly data-id='" + $(this).val() + "' value='" + $(this).text() + "'/></td>";
            dat += "<td align=\"center\"><input type=\"text\" class=\"form-control currency check\" data-v-min=\"0\" data-m-dec=\"2\"  data-d-group=\"2\" style='width:100%;border: 1px solid #2196f3;text-align:right;' name=\"cc_jour\" value=\"\">";
            dat += "<td><input type=\"hidden\" value=\"" + $(this).val() + "\" /><button type=\"button\" name=\"ccDelete\" class=\"btn btn-default waves-effect\" data-acc=\"tt\" data-id=\"" + $(this).val() + "\">"
                    + "<i class=\"material-icons\" style=\"color: #ff4733 ; \">delete_forever</i></button></td></tr>";
            $('#cctable>tbody').append(dat);
            $(this).attr('selected', false).attr('disabled', true);
        });
        $(this).selectpicker('refresh');
    });
    $(document).on('click', 'button[name=ccDelete]', function () {
        $("#cc_select option[value='" + $(this).data('id') + "']").attr('disabled', false);
        $("#cc_select").selectpicker('refresh');
        $(this).closest("tr").remove();
    });
    $(document).on('click', 'button[name=ccReset]', function () {
        ccreset();
    });
    function ccreset() {
        $("#cc_select option:disabled").attr('disabled', false);
        $("#cc_select").selectpicker('refresh');
        $('#cctable>tbody').children("tr").remove();
        $('#cctable>tbody').append("<tr><td></td><td></td><td></td></tr>");
        $('#cc_jour_tot').val("");
    }
    $(document).on('change', 'input[name=cc_jour]', function () {
        var tot = 0;
        $('input[name=cc_jour]').each(function () {
            $(this).autoNumeric('init');
            tot += Number($(this).autoNumeric('get')) || 0;
        });
        $('#cc_jour_tot').autoNumeric('init');
        $('#cc_jour_tot').autoNumeric('set', tot);

    });
    function hiderow(rw) {
        $('#icondiv_' + rw).html('<br><a href="#" onclick="showrow(' + rw + ');return false;"><span class="glyphicon glyphicon-menu-down"></span></a>');
        $('#ccdiv_' + rw).hide();
        $('#chqdiv_' + rw).hide();
    }
    function showrow(rw) {
        $('#icondiv_' + rw).html('<br><a  href="#" onclick="hiderow(' + rw + ');return false;"><span class="glyphicon glyphicon-menu-up"></span></a>');
        $('#ccdiv_' + rw).show();
        $('#chqdiv_' + rw).show();
    }
    function load_detail(rw) {
        $('#icondiv_' + rw).html('<br><a href="#" onclick="hiderow(' + rw + ');return false;"><span class="glyphicon glyphicon-menu-up"></span></a>');
        $('#ccdiv_' + rw).html("cc div detailscc div detailscc div detailscc div detailscc div detailscc div detailscc div detailscc div detailscc div detailscc div detailscc div detailscc div detailscc div details");
        $('#chqdiv_' + rw).html("cc div detailscc div detailscc div detailscc div detailscc div detailscc div detailscc div detailscc div detailscc div detailscc div detailscc div detailscc div detailscc div details");
        var cloned = $('#jm_detail>tbody>tr:last').clone(true);
        var id = parseInt($('#jm_detail>tbody>tr:last').attr('id'));
        cloned.attr({id: id + 1});
        cloned.find("#jtm_" + id).removeAttr('onchange'); //.removeAttr('style').unbind();
        cloned.find("#jtm_" + id).attr({onchange: 'load_cc(' + (id + 1) + ');', id: "jtm_" + (id + 1)});
        cloned.find("#party_" + id).removeAttr('onchange'); //.removeAttr('style').unbind();
        cloned.find("#party_" + id).attr({onchange: 'load_cc(' + (id + 1) + ');', id: "party_" + (id + 1)});
        cloned.find("#chq_" + id).attr({onclick: 'load_detail(' + (id + 1) + ');', id: "chq_" + (id + 1)});
        cloned.find("#ccdiv_" + id).attr({onclick: 'load_detail(' + (id + 1) + ');', id: "ccdiv_" + (id + 1)});
        cloned.find("#chqdiv_" + id).attr({onclick: 'load_detail(' + (id + 1) + ');', id: "chqdiv_" + (id + 1)});
        cloned.find("#icondiv_" + id).attr({id: "icondiv_" + (id + 1)});
        cloned.find("#jdebit" + id).attr({id: "jdebit" + (id + 1), value: ''});
        cloned.find("#debitbill" + id).attr({id: "debitbill" + (id + 1), value: ''});
        cloned.find("#jcredit" + id).attr({id: "jcredit" + (id + 1), value: ''});
        cloned.find("#creditbill" + id).attr({id: "creditbill" + (id + 1), value: ''});
        cloned.find("#ccdiv_" + id).attr({id: "ccdiv_" + (id + 1)});
        $('#jm_detail>tbody').append(cloned);
        $('#ccdiv_' + (id + 1)).html("");
        $('#chqdiv_' + (id + 1)).html("");
        $('#icondiv_' + (id + 1)).html("");
    }
    function load_division() {
        $.ajax({
            url: "fin_accounts.do", data: "option=load_division", type: "post",
            success: function (data) {
                $("#om_id").html(data);
                $('#om_id').selectpicker('refresh');
                if ($("#om_id option").length == 2) {
                    $("#om_id :nth-child(2)").prop('selected', true).change();
                }

            }
        });
    }
    function loadtrans(inp) {
        ////  alert(inp+"::"+$('#jm_id').val()) ;
        //  $('#progress_gif').show();
        if (inp === -100) {
            var DataString = 'jm_id=' + $('#jm_id').val() + '&om_id=' + $('#om_id').val() + '&ay_id=' + $('#ay_id').val() + '&option=loadtrans';
            $('#jm_no').val($('#jm_id').val());
        } else {
            var DataString = 'jm_id=' + inp + '&om_id=' + $('#om_id').val() + '&ay_id=' + $('#ay_id').val() + '&option=loadtrans';
            $("#jm_no").val(inp);
            $('#jm_id').val(inp);
        }
        $.ajax({url: "fin_accounts.do", data: DataString, type: "post",
            success: function (data) {
                $("#load_trans").html(data);
                $('.selectpicker').selectpicker();
                // $("#tt_date").bootstrapMaterialDatePicker('refresh');
                // if ($("#form_validation1").valid()){
                //  totcal();
                // }
                //disablejtm();
                //   $('#progress_gif').hide();
//                if ($('#jm_id').val() !== '') {
//                loaddebit();
//            }
            }
        });
    }
    function totcal() {
        var tot = 0;
        $('[name=jcredit]').each(function () {
            $(this).autoNumeric('init');
            tot += Number($(this).autoNumeric('get')) || 0;
        });
        if ($("#tt_id").val() <= 4 && $("#jtm_1").find(':selected').data('credit') == "") {
            $('#jdebit1').val(parseFloat(tot).toFixed(2));
            //  $('#jdebit1').attr('readonly',true);
        }
        $('[name=totcredit]').val(parseFloat(tot).toFixed(2));
        tot = 0;
        $('[name=jdebit]').each(function () {
            $(this).autoNumeric('init');
            tot += Number($(this).autoNumeric('get')) || 0;
        });
        if ($("#tt_id").val() <= 4 && $("#jtm_1").find(':selected').data('debit') == "") {
            $('#jcredit1').val(parseFloat(tot).toFixed(2));
            //   $('#jcredit1').attr('readonly',true);
        }
        $('[name=totdebit]').val(parseFloat(tot).toFixed(2));
    }
    function loadjtm() {
        var DataString = 'om_id=' + $('#om_id').val() + '&ay_id=' + $('#ay_id').val() + '&option=loadjtm';
        $.ajax({url: "fin_accounts.do", data: DataString, type: "post",
            success: function (data) {
                $("#jm_div").html(data);
                $("#jm_div").addClass("form-line focused");
                $('#jm_id').selectpicker('refresh');
                loadtrans(-100);
            }
        });
    }


</script>
<style>
    input[name=jcredit]:-moz-read-only,input[name=jdebit]:-moz-read-only { /* For Firefox */
        background-color: #d4def1;
    }
    input[name=jcredit]:read-only,input[name=jdebit]:read-only {
        background-color: #d4def1;
    }
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align:middle;
        padding-right: 10px;
        padding-left: 10px;
        padding-bottom: 5px;
        padding-top: 5px;
    }
    .card .body{
        padding:10px;
    }
    section.content{
        margin: 45px 15px 0 315px;
    }
    .card .header{
        border-bottom:0px;
    }
    .panel-col-purple-grey{
        background-color: #007bb7; 
        color: #fff;
    }
    .btn-col-teal-bg{
        background-color: #d5f8f5; 
        color: #000;
    }
    .dropdown-menu {
        width:80%;

    }
    .dropdown-menu li a {
        word-wrap: break-word;
        white-space: normal;
    }

    .bootstrap-select.btn-group .dropdown-toggle .caret {
        right: -8px;
    }
    table {
        border-collapse: collapse;
    }

    select {
        -webkit-appearance: none;
        -moz-appearance: none;
        text-indent: 1px;
        text-overflow: '';
    }
    .form-group .form-line:after {
        border-bottom:1px solid #67d3c9;
    }
    .form-group .form-line {
        border-bottom: 1px solid #67d3c9;
    }
    .modal {
        text-align: center;
    }
    @media screen and (min-width: 768px) { 
        .modal:before {
            display: inline-block;
            vertical-align: middle;
            content: " ";
            height: 100%;
        }
    }
    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
        overflow-y: scroll; max-height:90%; 
    }
    table.dataTable tbody tr.myeven{
        background-color:#e3eee3;
    }
    table.dataTable tbody tr.myodd{
        background-color:#FFFFFF;
    }
    div .vspace1em {
        clear: both;
        height: 1em;
    }
    .card .body .col-xs-12,.body .col-sm-4{
        margin-bottom:0px;
    }
</style>
<%    DBConnect db = new DBConnect();
    try {

        db.getConnection();
%>
<section class="content">
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="col-xs-12 col-sm-12 align-left ">
                        <div class="col-xs-12 col-sm-8 align-left ">
                            <div class="form-group form-float">
                                <h2>JOURNAL ENTRY </h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-12">
                            <div class="col-xs-12 col-sm-4 align-left ">
                                <div class="form-group form-float">
                                    <label class="form-label" style="font-weight: normal;color:#aaa;">Accounting Year</label>
                                    <div class="form-line">
                                        <select class="form-control selectpicker show-tick" data-style="btn-col-teal-bg" data-actions-box="true" name="ay_id" id="ay_id"  data-live-search="true" required onchange="loadjtm()">              
                                            <%
                                                db.read("SELECT ay_id,ay_desc,IF(current_year=1,'selected','') sel FROM accounts.accounting_year a where current_year>0  order by ay_id desc");
                                                while (db.rs.next()) {
                                                    out.print("<option value=" + db.rs.getString("ay_id") + " " + db.rs.getString("sel") + ">" + db.rs.getString("ay_desc") + "</option>");
                                                }
                                            %>
                                        </select> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 align-left ">
                                <div class="form-group form-float">
                                    <label class="form-label" style="font-weight: normal;color:#aaa;">Division</label>
                                    <div class="form-line focused" style='border-bottom: none'> 
                                        <select class="form-control show-tick selectpicker" data-style="btn-col-teal-bg" data-actions-box="true"  name="om_id" id="om_id" onchange="loadjtm()">
                                        </select>
                                    </div> 
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 align-left ">
                                <div class="form-group form-float" style='border-bottom: none'>
                                    <label class="form-label" style="font-weight: normal;color:#aaa;">Transaction</label>
                                    <div id="jm_div"> 
                                    </div>  
                                </div>
                            </div>  
                        </div>

                        <form id="form_validation1" method="POST">
                            <div id="load_trans"> 
                            </div>
                            <div class="col-xs-12 col-sm-12 align-center " style="padding-top: 8px;">
                                <button type='submit' class="btn  waves-effect btn-col-teal-bg"  style="background-color: #d5f8f5;border: 1px solid #67d3c9;"  id="submit" name='submit' value='Submit' onclick="disable_submit()">Save</button>
                                <button class="btn  waves-effect btn-col-teal-bg" style="background-color: #d5f8f5;border: 1px solid #67d3c9;" name='clear' value='Reset' onclick='loadjtm()'  type="button">Reset</button>
                                <script>loadjtm();</script><div id="lmscript"></div>                            
                            </div>
                        </form>


                        <!--                        <div class='col-xs-12 col-sm-12 align-left' style='padding-left: 30px;padding-right: 30px;'><div class='table-responsive'>
                                                        <table width='100%'><thead><tr><th width:'25%'>Ledger</th><th  width='25%'>Narration</th><th width='10%'>Debit</th><th width='10%'>Credit</th><th  width='5%'>Cost Center</th><th  width='5%'>Bill/Ref. Date</th><th width='5%'>Cheque.No</th><th  width='5%'>Bill/Ref. No</th><th  width='5%'>Cheque.Date</th></tr></thead>
                                                            <tbody><tr id='1'><td style='width:25%'><select id='jtm_1' name='jtm' style='width:100%' onchange='load_cc(1)' class='form-control selectpicker show-tick' data-live-search='true'>
                                                                            <option value=''>--Select Journal--</option></select> <input type='hidden' id='otlm_" + i_count + "' name='otlm' data-ad=''/></td>
                                                                    <td style='width:25%'><input type='text' id='jdesc1' name='jdesc' style='width:100%' /> </td>
                                                                    <td style='width:10%'><input type='text' id='jdebit1' style='width:100%;' name='jdebit' clss='currency' data-v-min='0' data-m-dec='2'  data-d-group='2'/>
                                                                        <table class='tbl' id='debitbill1' ></table></td>
                                                                    <td style='width:10%'><input type='text'  id='jcredit1' name='jcredit' style='width:100%' class='currency' data-v-min='0' data-m-dec='2'  data-d-group='2'/>
                                                                        <table class='tbl' id='creditbill1' ></table> </td>
                                                                    <td style='width:5%'><input type='button' style='width:100%' name='viewcc' id='viewcc_1' value='cc' onclick='viewcc1(1)'/>
                                                                        <table class='tbl' id='ccdet1' style='display:none;'>
                                                                            <tr id='1_1'><td><select class='form-control selectpicker show-tick' data-style='btn-col-teal-bg' data-actions-box='true' data-live-search='true' required  id='cc_1_1' name='cc_1' onchange='add_ccitem("1 ", "1 ")'></select></td>
                                                                                <td><input type='text' name='camt_1' id='camt_1_1'/></td></tr>
                                                                        </table></td> 
                                                                    <td style='width:5%'><input type='text' id='billno1' name='billno' style='width:100%'/></td>
                                                                    <td style='width:5%'><input type='text' id='billdt1' name='billdt' style='width:100%'/></td>
                                                                    <td><input type='text' id='chqno1' name='chqno' style='width:100%'/></td>
                                                                    <td style='width:5%'><input type='text' id='chqdt1' name='chqdt' style='width:100%'/></td>
                                                                </tr></tbody>
                                                        </table></div>
                                                </div>-->
                        <!--                        <div class='col-xs-12 col-sm-12 align-left' style='padding-left: 30px;padding-right: 30px;'><div class='table-responsive'>
                                                        <table width='100%' id='jm_detail' class='table-bordered'><thead><tr><th width='33%'>Ledger</th><th width='32%'>Party</th><th width='5%'>Details</th><th width='15%'>Debit</th><th width='15%'>Credit</th></tr></thead>
                                                            <tbody><tr id='1'>
                                                                    <td style='vertical-align: top; padding: 5px;' >
                                                                        <select id='jtm_1' name='jtm' style='width:70%' onchange='load_cc(1)' class='form-control selectpicker show-tick' data-live-search='true'><option value=''>--Select Journal--</option></select>
                                                                        <div> <table  id='ccdiv_1'></table></div>
                                                                    </td> <td style='vertical-align: top; padding: 5px;' >   <select id='party_1' name='party' style='width:70%' class='form-control selectpicker show-tick' data-live-search='true'><option value=''>--Select party--</option></select>
                                                                        <input type='hidden' id='otlm_" + i_count + "' name='otlm' data-ad=''/><div id='chqdiv_1'></div></td>
                                                                    <td style='vertical-align: top; padding: 5px;' >
                                                                        <button type="button" style='width:100%;background-color: #eaecd5;border: 1px solid #4caf50;' class="btn" onclick="load_btn(1)" data-book-id="44444" name='chq' id='chq_1' data-toggle="modal">CC</button>
                                                                        <div class='align-center' id='icondiv_1'></div>
                                                                    </td>
                                                                    </td>
                                                                    <td style='vertical-align: top;padding: 5px;'><input type='text' id='jdebit1' style='width:100%;border: 1px solid #2196f3;text-align:right; ' name='jdebit' class='currency form-control' data-v-min='0' data-m-dec='2'  data-d-group='2'/><table class='tbl' id='debitbill1' ></table></td>
                                                                    <td style='vertical-align: top;padding: 5px;'><input type='text'  id='jcredit1' name='jcredit' style='width:100%;border: 1px solid #2196f3;text-align:right; ' class='currency form-control' data-v-min='0' data-m-dec='2'  data-d-group='2'/> <table class='tbl' id='creditbill1' ></table></td>
                                                                </tr> 
                        
                                                            </tbody>
                                                            <tfoot align=\"right\"><tr><th style='border:0;'></th><th style='border:0;'></th><th style='border:0;'></th><th style='vertical-align: top;padding: 5px;'><input type='text' style='width:100%;border: 1px solid #9c27b0;' class='currency form-control' readonly name='totdebit' id='totdebit'/></th><th style='vertical-align: top;padding: 5px;'><input type='text' style='width:100%;border: 1px solid #9c27b0;' class='currency form-control' readonly  name='totcredit' id='totcredit' class='validate[required,equals[totdebit]]' /> </th></tr></tfoot>
                                                        </table>
                                                    </div>
                                                </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="overflow-y: scroll; max-height:85%;  margin-top: 50px; margin-bottom:50px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="ccreset()"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Cost Center</h4>
                </div>
                <div class="modal-body">
                    <div id="CCTab"> </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="ccreset()">Close</button>
                    <button type="button" class="btn btn-primary" id='savecc' data-ad="-1">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<% } catch (Exception e) {
    } finally {
        db.closeConnection();
    }
%>
<%@include file="../../CommonJSP/pageFooter.jsp" %>