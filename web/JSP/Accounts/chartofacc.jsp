<%-- 
    Document   : chartofacc
    Created on : May 8, 2019, 10:46:26 AM
    Author     : malathi
--%>

<%@include file="../../CommonJSP/pageHeader.jsp" %>
<script src="../../plugins/jquery-validation/jquery.validate.js" type="text/javascript"></script>
<script src="../../plugins/jquery-validation/additional-methods.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<script src="../../js/pages/CommonJSP/validation.js" type="text/javascript"></script>

<script>
    $(document).ready(function () {
        load_division();
        $('[data-toggle="tabajax"]:first').click();
        $.extend(true, $.fn.dataTable.defaults, {
            columnDefs: [
                {
                    targets: '_all',
                    defaultContent: '-'
                }
            ]
        });
        validate();
    });
    function load_division() {
        $.ajax({
            url: "fin_accounts.do", data: "option=load_division", type: "post",
            success: function (data) {
                $("#om_id").html(data);
                if ($("#om_id option").length == 2) {
                    $("#om_id :nth-child(2)").prop('selected', true).change();
                }
                $('#om_id').selectpicker('refresh');
            }
        });
    }
    $(document).on('change', '[name=om_id]', function () {
        loadledger();
        loadcc();
        load_ledger_cc();
        loadttuser();
    });
    function loadledger() {
        var DataString = 'option=loadledger&om_id=' + $("#om_id").find('option:selected').data("omid");
        $.ajax({
            url: "fin_accounts.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#ledger_list').html(data);
                $('#ledger_list .table').DataTable({
                    "processing": true,
                    "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
                    "dom": 'lBfrtip',
                    "responsive": true,
                    "columnDefs": [
                        {targets: [1], orderable: false},
                        {targets: [0], orderable: false}
                    ],
                });
            }
        });
    }
    function loadcc() {
        var DataString = 'option=cost_create_table&om_id=' + $("#om_id").find('option:selected').data("omid");
        $.ajax({
            url: "fin_accounts.do", data: DataString, type: "post",
            success: function (data)
            {
                var groupColumn = 0;
                $('#cc_tab_div').html(data);
                $('#cc_div .table').DataTable({
                    "ordering": false,
                    "processing": true,
                    "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
                    "dom": 'lBfrtip',
                    "responsive": true,

//                    "columnDefs": [
//                        {"visible": false, "targets": groupColumn}
//                    ],
//                    "order": [[groupColumn, 'asc']],
//                    "displayLength": 25,
//                    "drawCallback": function (settings) {
//                        var api = this.api();
//                        var rows = api.rows({page: 'current'}).nodes();
//                        var last = null;
//                        api.column(groupColumn, {page: 'current'}).data().each(function (group, i) {
//                            if (last !== group) {
//                                $(rows).eq(i).before(
//                                        '<tr class="group"><td colspan="2">' + group + '</td></tr>'
//                                        );
//                                last = group;
//                            }
//                        });
//                    }
                });
            }
        });
    }

    function load_ledger_cc() {
        $.ajax({
            url: "fin_accounts.do", data: "option=load_ledger_cc&om_id=" + $("#om_id").find('option:selected').data("omid") + "&om_id=" + $("#om_id").find('option:selected').val(), type: "post",
            success: function (data) {
                $("#lc_map").html(data);
                $("#ledger_cc_entry,#costcenter_cc_entry,#staff_id_cc").selectpicker();
                ledger_cc_report();
            }
        });
    }
    function ledger_cc_report() {
        $.ajax({
            url: "fin_accounts.do", data: "option=load_ledger_cc_table&om_id=" + $("#om_id").find('option:selected').val(), type: "post",
            success: function (data) {
                $("#lm_cc_list").html(data);
              }
        });
    }

    $(document).on('click', '[id=cclmSave]', function () {
        alert();
        if ($("#form_validation4").valid()) {
            $.ajax({
                url: "fin_accounts.do", data: "option=add_cc_ledger&om_id=" + $("#om_id").find('option:selected').val() + "&lm_id=" + $("#ledger_cc_entry").val() + "&cc_id=" + $("#costcenter_cc_entry").val() + "&staff=" + $("#staff_id_cc").val(), type: "post",
                success: function (data) {
                    $("#ledger_cc_entry,#costcenter_cc_entry,#staff_id_cc").selectpicker('val', '-1');
                    ledger_cc_report();
                }
            });
        }
    });

    $(document).on('click', '[id=cclmreset]', function () {
        $("#ledger_cc_entry,#costcenter_cc_entry,#staff_id_cc").selectpicker('val', '-1');
    });
    function ledger_tt_lm_map() {
        $.ajax({
            url: "fin_accounts.do", data: "option=staff_tt_lm_map", type: "post",
            success: function (data) {
                $("#staff_map1").html(data);
                $("#staff_map2").html(data);
                $("#staff_map1,#staff_map2").selectpicker('refresh');
            }
        });
        $.ajax({
            url: "fin_accounts.do", data: "option=ledger_tt_lm_map&om_id=" + $("#om_id").find('option:selected').data("omid") + '&tt_id=' + $("#tt_map").val(), type: "post",
            success: function (data) {
                $("#ledger_map1").html(data);
                $("#ledger_map2").html(data);
                $("#ledger_map1,#ledger_map2").selectpicker('refresh');
                ledger_tt_map_report1();
            }
        });
    }
    function ledger_tt_map_report1() {
        $.ajax({
            url: "fin_accounts.do", data: "option=load_ledger_tt_map_report1&om_id=" + $("#om_id").find('option:selected').val() + '&tt=' + $("#tt_map").val(), type: "post",
            success: function (data) {
                $("#jl_report").html(data);
            }
        });
    }
    $(document).on('click', '[id=jlsubmit]', function (e) {
        alert($("#ledger_map1").val());
        if (($("#tt_map").val().length > 0) && (($("#ledger_map1").val() != null && $("#ledger_map1").val().length > 0) || ($("#ledger_map2").val() != null && $("#ledger_map2").val().length > 0))) {
            $.ajax({
                url: "fin_accounts.do", data: "option=ledger_tt_mappping&tt_id=" + $("#tt_map").val() + '&lm_id2=' + $("#ledger_map2").val() + '&lm_id1=' + $("#ledger_map1").val() + '&dm_id=' + $("#om_id").find('option:selected').val() + "&staff1=" + $("#staff_map1").val() + "&staff2=" + $("#staff_map2").val(), type: "post",
                success: function (data) {
                    $('#ledger_map2>option').removeAttr('selected');
                    $('#ledger_map1>option').removeAttr('selected');
                    ledger_tt_map_report1();
                    ledger_tt_lm_map();
                }
            });
        } else {
            alert("Please Enter all the fields..!");
        }
    });
    function loadttuserlist() {
        var DataString = 'option=tt_staff_table&om_id=' + $("#om_id").find('option:selected').data("omid");
        $.ajax({
            url: "fin_accounts.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#tt_user_list').html(data);
                $('#tt_user_list .table').DataTable({
                    "processing": true,
                    "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
                    "dom": 'lBfrtip',
                    "responsive": true,
                    "columnDefs": [
                        {targets: [0], orderable: false}
                    ],
                });
            }
        });
    }
    function loadttuser() {
        $.ajax({
            url: "fin_accounts.do", data: 'option=tt_staff_data&om_id=' + $("#om_id").find('option:selected').data("omid"), type: "post",
            success: function (data)
            {
                $('#tt_user').html(data);
                $('.selectpicker').selectpicker();
                loadttuserlist();
            },
            error: function (data) {
                console.log("Read tt data failed!!");
            }
        });
    }
    $(document).on('click', '[id=ttuserSave]', function (e) {
        if ($("#form_validation2").valid()) {
            $.ajax({
                url: "fin_accounts.do", data: 'option=tt_staff_save&om_id=' + $("#om_id").find('option:selected').data("omid") + '&staff=' + $("#staff").val() + '&tt_id=' + $("#tt_staff").val(), type: "post", type: "post",
                success: function (data)
                {
                    loadttuserlist();
                    reset_tt();
                },
                error: function (data) {
                    console.log("Save tt data failed!!");
                }
            });
        }
    });
    $(document).on('click', '[name=delete_staff_tt]', function (e) {
        if ($("#form_validation2").valid()) {
            $.ajax({
                url: "fin_accounts.do", data: 'option=tt_staff_save&om_id=' + $("#om_id").find('option:selected').data("omid") + '&staff=' + $("#staff").val() + '&tt_id=' + $("#tt_staff").val(), type: "post", type: "post",
                success: function (data)
                {
                    loadttuserlist();
                },
                error: function (data) {
                    console.log("Save tt data failed!!");
                }
            });
        }
    });
    $(document).on('click', '[name=btn_addcc]', function (e) {
        $.ajax({
            url: "fin_accounts.do", data: 'option=read_cc&om_id=' + $("#om_id").find('option:selected').data("omid"), type: "post",
            success: function (data)
            {
                $('#save_modal_div').html(data);
                $('#saveModal').modal();
                $('.selectpicker').selectpicker();
            },
            error: function (data) {
                console.log("Read cc data failed!!");
            }
        });
        var fClose = function () {
            modal.modal("hide");
        };
        var onConfirm = function () {
            if ($("#ccname").val() != "" && $("#ccdesc").val() != "" && $("#ccgrp").val() != "" && ($("#ccfin").val() != "" || $("#storecc").val() != "")) {
                alert("slkdjf");
                $.ajax({
                    url: "fin_accounts.do", data: 'option=save_cc&lm_id=', type: "post",
                    success: function (data)
                    {
                        loadledger();
                    },
                    error: function (data) {
                        console.log("Error in saving!");
                    }
                });
            } else {
                alert("fill necessary fields");
            }
        }
        var modal = $("#saveModal");
        modal.modal("show");
        $("#saveOk").unbind().one('click', onConfirm).one('click', fClose);
        $("#saveCancel").unbind().one("click", fClose);
    });
    $(document).on('click', '[name=btnDelete]', function (e) {
        var id = $(this).data('id');
        var acc = $(this).data('acc');
        var fClose = function () {
            modal.modal("hide");
        };
        var onConfirm = function () {
            if (acc == "ledger") {
                $.ajax({
                    url: "fin_accounts.do", data: 'option=delete_ledger&lm_id=' + id, type: "post",
                    success: function (data)
                    {
                        loadledger();
                    },
                    error: function (data) {
                        console.log("deleted!");
                    }
                });
            } else if (acc === "cc") {
                alert(acc);
                $.ajax({
                    url: "fin_accounts.do", data: 'option=delete_cc&cc_id=' + id, type: "post",
                    success: function (data)
                    {
                        loadcc();
                    },
                    error: function (data) {
                        console.log("deleted!");
                    }
                });
            } else if (acc === "tt") {
                $.ajax({
                    url: "fin_accounts.do", data: "option=delete_staff_tt&id=" + id, type: "post",
                    success: function () {
                        loadttuserlist();
                    }
                });
            } else if (acc === "jl") {
                $.ajax({
                    url: "fin_accounts.do", data: "option=delete_tt_lm_map&id=" + id, type: "post",
                    success: function () {
                        ledger_tt_map_report1();
                    }
                });
            } else if (acc === "lcc") {
                $.ajax({
                    url: "fin_accounts.do", data: "option=delete_oclm&id=" + id, type: "post",
                    success: function (data) {
                        ledger_cc_report();
                    }
                });
            }
        }
        var modal = $("#confirmModal");
        modal.modal("show");
        $("#confirmMessage").empty().append("Confirm v Delete?");
        $("#confirmOk").unbind().one('click', onConfirm).one('click', fClose);
        $("#confirmCancel").unbind().one("click", fClose);
    });
    $(document).on('click', '[id=ccreset]', function (e) {
        $("#ccname").val("");
        $("#ccdesc").val("");
        $('#ccgrp').selectpicker('val', '-1');
        $('#ccfin').prop('checked', true);
        $('#storecc').prop('checked', false);
    });
    $(document).on('click', '[id=ttreset]', function (e) {
        reset_tt();
    });
    function reset_tt() {
        $('#tt_staff').selectpicker('val', '-1');
        $('#staff').selectpicker('val', '-1');
    }
    $(document).on('click', '[id=ccsubmit]', function (e) {
        if ($("#form_validation").valid() && ($('#ccfin').is(':checked') || $('#storecc').is(':checked'))) {
            $.ajax({
                url: "fin_accounts.do", data: 'option=save_cc&cc_name=' + $("#ccname").val() + '&cc_desc=' + $("#ccdesc").val() + '&p_cc=' + $("#ccgrp").val() + '&store_cc=' + ($('#storecc').is(':checked') ? 1 : 0) + '&acc_cc=' + ($('#ccfin').is(':checked') ? 1 : 0) + '&om_id=' + $("#om_id").val(), type: "post",
                success: function (data)
                {
                    loadcc();
                },
                error: function (data) {
                    console.log("Error in saving ");
                }
            });
        }
    });
    $(document).on('click', '[name=btnEdit]', function (e) {
        var id = $(this).data('id');
        $.ajax({
            url: "fin_accounts.do", data: 'option=edit_ledger&lm_id=' + id, type: "post",
            success: function (data) {
                $('#modal_data_div').html(data);
                $('#defaultModal').modal();
            },
            error: function (e) {
                console.log("There was an error with your request...");
            }
        });
        var fClose = function () {
            $('#modal_data_div').html("");
            modal.modal("hide");
        };
        var onConfirm = function () {
            var jcoll = [];
            $.each($("input[name^='ftype_']"), function () {
                var farry = $(this).attr('name').split('_');
                var jdat = {};
                jdat['id'] = farry[1]
                jdat['val'] = $(this).val();
                if (!($(this).val() === (""))) {
                    jcoll.push(jdat);
                }
            });
            $.each($("select[name^='stype_']"), function () {
                var sarry = $(this).attr('name').split('_');
                var jdat = {};
                jdat['id'] = sarry[1]
                jdat['val'] = $(this).val();
                alert($(this).val());
                if (!($(this).val() === ("--Select--"))) {
                    jcoll.push(jdat);
                }
            });
            var jsondata = JSON.stringify(jcoll);
            $.ajax({
                url: "fin_accounts.do", data: 'option=update_ledger&lm_id=' + id + '&jsdata=' + jsondata, type: "post",
                success: function (data)
                {
                    $('#modal_data_div').html("");
                    loadledger();
                },
                error: function (data) {
                    $('#modal_data_div').html("");
                    console.log("Updated");
                }
            });
        }
        var modal = $("#editModal");
        modal.modal("show");
        $("#editOk").unbind().one('click', onConfirm).one('click', fClose);
        $("#editCancel").unbind().one("click", fClose);
    });</script>

<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align:middle;
        padding-right: 10px;
        padding-left: 10px;
        padding-bottom: 5px;
        padding-top: 5px;
    }
    .card .body{
        padding:10px;
    }
    section.content{
        margin: 45px 15px 0 315px;
    }
    .card .header{
        border-bottom:0px;
    }
    .panel-col-purple-grey{
        background-color: #007bb7; 
        color: #fff;
    }
    .btn-col-teal-bg{
        background-color: #00b597; 
        color: #fff;
    }
</style>

<%    DBConnect db = new DBConnect();
    try {

        db.getConnection();
%>
<section class="content">
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="col-xs-12 col-sm-12 align-left ">
                        <div class="col-xs-12 col-sm-8 align-left ">
                            <div class="form-group form-float">
                                <h2>CHART OF ACCOUNTS  </h2>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 align-left ">
                            <div class="form-group form-float">
                                 <label class="form-label" style="font-weight: normal;color:#aaa;">Division</label>
                                <div class="form-line focused"> 
                                    <select class="form-control show-tick selectpicker" data-style="btn-col-teal-bg" data-actions-box="true"  name="om_id" id="om_id">
                                    </select>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tab-col-teal" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#ledger_tab" data-toggle="tab">
                                LEDGER
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#cc_tab" data-toggle="tab">
                                COST CENTER
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#jl_tab" data-toggle="tab">
                                JOURNAL-LEDGER MAPPING
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#lc_tab" data-toggle="tab">
                                LEDGER-COST CENTER MAPPING
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#ju_tab" data-toggle="tab">
                                JOURNAL-USER MAPPING
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane in active fade" id="ledger_tab">
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group full-body" id="accordion_11" role="tablist" aria-multiselectable="true">
                                        <div class="panel">
                                            <div class="panel-heading  panel-col-purple-grey" role="tab" id="headingOne_11">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" href="#collapseOne_11" aria-expanded="true" aria-controls="collapseOne_11">
                                                        <i class="material-icons">library_add</i>Add Ledger</a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_11" class="panel-collapse collapse in"  role="tabpanel" aria-labelledby="headingOne_11">
                                                <div class="panel-body" >
                                                    <form id="form_validation2" method="POST">
                                                        <div class="col-xs-12 col-sm-12 align-left" id='add_ledger'>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group full-body" id="accordion_12" role="tablist" aria-multiselectable="true">
                                        <div class="panel">
                                            <div class="panel-heading  panel-col-purple-grey" role="tab" id="headingOne_12">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" href="#collapseOne_12" aria-expanded="true" aria-controls="collapseOne_12">
                                                        <i class="material-icons">list_alt</i>Ledger List</a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_12" class="panel-collapse collapse in"  role="tabpanel" aria-labelledby="headingOne_12">
                                                <div class="panel-body" id='ledger_list'>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="cc_tab">
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group full-body" id="accordion_21" role="tablist" aria-multiselectable="true">
                                        <div class="panel">
                                            <div class="panel-heading  panel-col-purple-grey" role="tab" id="headingOne_21">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" href="#collapseOne_21" aria-expanded="true" aria-controls="collapseOne_21">
                                                        <i class="material-icons">library_add</i>Add Cost Center</a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_21" class="panel-collapse collapse in"  role="tabpanel" aria-labelledby="headingOne_21">
                                                <div class="panel-body">
                                                    <form id="form_validation" method="POST">
                                                        <div class="col-xs-12 col-sm-12 align-left ">
                                                            <div class="col-xs-12 col-sm-6 align-left ">
                                                                <div class="form-group form-float">
                                                                    <div class="form-line">
                                                                        <input type="text" class="form-control" name="ccname" id="ccname" required>
                                                                        <label class="form-label">Name </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 align-left ">
                                                                <div class="form-group form-float">
                                                                    <div class="form-line">
                                                                        <input type="text" class="form-control" name="ccdesc" id="ccdesc" required>
                                                                        <label class="form-label">Description</label>
                                                                    </div>
                                                                </div>
                                                            </div></div>
                                                        <div class="col-xs-12 col-sm-12 align-left ">
                                                            <div class="col-xs-12 col-sm-6 align-left ">
                                                                <div class="form-group form-float">
                                                                    <label class="form-label" style="font-weight: normal;color:#aaa;">CC Group</label>
                                                                    <div class="form-line">
                                                                        <select class="form-control selectpicker show-tick" name="ccgrp" id="ccgrp" data-live-search="true" required>
                                                                            <option  style="display:none"></option>
                                                                            <% db.read1("SELECT  pd.parent_id,cc.`ccm_name` AS parentname, GROUP_CONCAT(CONCAT(pd.ccm_id,':',REPLACE(pd.ccm_name,',',';'))) child FROM (SELECT ccm_id,ccm_name,parent_id,`status` FROM accounts.cc_master ccm  ORDER BY parent_id, ccm_id) pd JOIN (SELECT @pv:='1' FROM accounts.cc_master WHERE ccm_id='1' AND STATUS>0) ini JOIN accounts.cc_master cc ON cc.ccm_id=pd.parent_id AND cc.`status` >0  WHERE FIND_IN_SET(pd.parent_id, @pv) AND LENGTH(@pv := CONCAT(@pv, ',', pd.ccm_id)) AND pd.status>0 GROUP BY pd.parent_id");
                                                                                while (db.rs1.next()) {
                                                                            %> <optgroup label=" <% out.print(db.rs1.getString("parentname")); %> ">
                                                                                <% String[] ch = db.rs1.getString("child").split(",");
                                                                                    for (int i = 0; i < ch.length; i++) {
                                                                                        String[] chdat = ch[i].split(":");
                                                                                %>
                                                                                <option value=" <%  out.print(chdat[0]); %> "> <%  out.print(chdat[1]);%> 
                                                                                </option>
                                                                                <%}%>
                                                                            </optgroup>
                                                                            <%}%>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3 align-left ">
                                                                <div class="form-group">
                                                                    <input type="checkbox" name="tocc" id="ccfin" class="with-gap" checked>
                                                                    <label for="ccfin" class="m-l-20">Finance</label>
                                                                    <input type="checkbox" name="tocc" id="storecc" class="with-gap">
                                                                    <label for="storecc" class="m-l-20">Stock of Stores</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3 align-right ">
                                                                <button class="btn  waves-effect btn-col-teal-bg" id="ccsubmit" type="button"><bold>Save</bold></button>
                                                                <button class="btn  waves-effect btn-col-teal-bg" id="ccreset" type="button"><bold>Reset</bold></button>
                                                            </div>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix" >
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group full-body" id="accordion_22" role="tablist" aria-multiselectable="true">
                                        <div class="panel">
                                            <div class="panel-heading  panel-col-purple-grey" role="tab" id="headingOne_22">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" href="#collapseOne_22" aria-expanded="true" aria-controls="collapseOne_22">
                                                        <i class="material-icons">list_alt</i>Cost Center List</a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_9" class="panel-collapse collapse in"  role="tabpanel" aria-labelledby="headingOne_9">
                                                <div class="panel-body" id='cc_tab_div'>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="ju_tab">
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group full-body" id="accordion_31" role="tablist" aria-multiselectable="true">
                                        <div class="panel">
                                            <div class="panel-heading  panel-col-purple-grey" role="tab" id="headingOne_31">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" href="#collapseOne_31" aria-expanded="true" aria-controls="collapseOne_31">
                                                        <i class="material-icons">settings_applications</i> Journal-User Mapping</a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_31" class="panel-collapse collapse in"  role="tabpanel" aria-labelledby="headingOne_31">
                                                <div class="panel-body" >
                                                    <form id="form_validation2" method="POST">
                                                        <div class="col-xs-12 col-sm-12 align-left" id='tt_user'></div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group full-body" id="accordion_32" role="tablist" aria-multiselectable="true">
                                        <div class="panel">
                                            <div class="panel-heading  panel-col-purple-grey" role="tab" id="headingOne_32">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" href="#collapseOne_32" aria-expanded="true" aria-controls="collapseOne_32">
                                                        <i class="material-icons">list_alt</i>Journal-User List </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_32" class="panel-collapse collapse in"  role="tabpanel" aria-labelledby="headingOne_32">
                                                <div class="panel-body" id='tt_user_list'>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="lc_tab">
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group full-body" id="accordion_41" role="tablist" aria-multiselectable="true">
                                        <div class="panel">
                                            <div class="panel-heading  panel-col-purple-grey" role="tab" id="headingOne_41">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" href="#collapseOne_41" aria-expanded="true" aria-controls="collapseOne_41">
                                                        <i class="material-icons">settings_applications</i>Ledger-Cost Center Mapping</a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_41" class="panel-collapse collapse in"  role="tabpanel" aria-labelledby="headingOne_41">
                                                <form id="form_validation4" method="POST">
                                                    <div class="panel-body" id="lc_map">

                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group full-body" id="accordion_42" role="tablist" aria-multiselectable="true">
                                        <div class="panel">
                                            <div class="panel-heading  panel-col-purple-grey" role="tab" id="headingOne_42">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" href="#collapseOne_42" aria-expanded="true" aria-controls="collapseOne_42">
                                                        <i class="material-icons">list_alt</i>Ledger-Cost Center List </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_42" class="panel-collapse collapse in"  role="tabpanel" aria-labelledby="headingOne_42">
                                                <div class="panel-body" id='lm_cc_list'>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="jl_tab">
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group full-body" id="accordion_51" role="tablist" aria-multiselectable="true">
                                        <div class="panel">
                                            <div class="panel-heading  panel-col-purple-grey" role="tab" id="headingOne_51">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" href="#collapseOne_51" aria-expanded="true" aria-controls="collapseOne_51">
                                                        <i class="material-icons">settings_applications</i>Journal-Ledger Mapping</a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_51" class="panel-collapse collapse in"  role="tabpanel" aria-labelledby="headingOne_51">
                                                <form id="form_validation3" method="POST">
                                                    <div class="panel-body">

                                                        <div class="col-xs-12 col-sm-12 align-left ">
                                                            <div class="col-xs-12 col-sm-4 align-left ">
                                                                <div class="form-group form-float">
                                                                    <label class="form-label" style="font-weight: normal;color:#aaa;">Transaction Type</label>
                                                                    <div class="form-line">
                                                                        <select class="form-control selectpicker show-tick" id="tt_map" name="tt_map"  data-live-search="true" onchange="ledger_tt_lm_map()" required>
                                                                            <option  style="display:none"></option>
                                                                            <% db.read1("SELECT tt_id,tt_desc FROM accounts.transaction_type WHERE STATUS>0");
                                                                                while (db.rs1.next()) {
                                                                            %> 
                                                                            <option value="<%  out.print(db.rs1.getString("tt_id")); %>"> <%  out.print(db.rs1.getString("tt_desc"));%></option>
                                                                            <%}%>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 align-left ">
                                                                <div class="col-xs-12 col-sm-12 align-left ">
                                                                    <div class="form-group form-float">
                                                                        <label class="form-label" style="font-weight: normal;color:#aaa;">Debit Ledgers</label>
                                                                        <div class="form-line">
                                                                            <select class="form-control selectpicker show-tick" id='ledger_map1' name='ledger_map1' multiple data-live-search="true" required>
                                                                                <option  style="display:none"></option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-12 align-left ">
                                                                    <div class="form-group form-float">
                                                                        <label class="form-label" style="font-weight: normal;color:#aaa;">Debit Staff-Id</label>
                                                                        <div class="form-line">
                                                                            <select class="form-control selectpicker show-tick" id='staff_map1' name='staff_map1' multiple data-live-search="true" required>
                                                                                <option  style="display:none"></option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 align-left ">
                                                                <div class="col-xs-12 col-sm-12 align-left ">
                                                                    <div class="form-group form-float">
                                                                        <label class="form-label" style="font-weight: normal;color:#aaa;">Credit Ledgers</label>
                                                                        <div class="form-line">
                                                                            <select class="form-control selectpicker show-tick" id='ledger_map2' name='ledger_map2' multiple data-live-search="true" required>
                                                                                <option  style="display:none"></option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-12 align-left ">
                                                                    <div class="form-group form-float">
                                                                        <label class="form-label" style="font-weight: normal;color:#aaa;">Credit Staff-Id</label>
                                                                        <div class="form-line">
                                                                            <select class="form-control selectpicker show-tick" id='staff_map2' name='staff_map2' multiple data-live-search="true" required>
                                                                                <option  style="display:none"></option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 align-center ">
                                                            <button class="btn  waves-effect btn-col-teal-bg" id="jlsubmit" type="button">Save</button>
                                                            <button class="btn  waves-effect btn-col-teal-bg" id="jlreset" type="button">Reset</button>
                                                        </div>

                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group full-body" id="accordion_52" role="tablist" aria-multiselectable="true">
                                        <div class="panel">
                                            <div class="panel-heading panel-col-purple-grey"  role="tab" id="headingOne_52">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" href="#collapseOne_52" aria-expanded="true" aria-controls="collapseOne_52">
                                                        <i class="material-icons">list_alt</i>Journal-Ledger List </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_52" class="panel-collapse collapse in"  role="tabpanel" aria-labelledby="headingOne_52">
                                                <div class="panel-body" id='jl_report'>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                                           
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="saveModal" style="display: none; z-index: 1050;">
        <div class="modal-dialog" >
            <form id="form_validation1" method="POST"> 
                <div class="modal-content" >
                    <div class="modal-body" id="save_modal_div" ></div>
                    <div class="modal-footer">
                        <button class="btn btn-primary waves-effect" type="button" id="saveOk">SUBMIT</button>
                        <button class="btn btn-primary waves-effect" type="button" id="saveCancel">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="editModal" style="display: none; z-index: 1050;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="modal_data_div">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="editOk">Update</button>
                    <button type="button" class="btn btn-default" id="editCancel">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="confirmModal" style="display: none; z-index: 1050;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="confirmMessage">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="confirmOk">Delete</button>
                    <button type="button" class="btn btn-default" id="confirmCancel">Cancel</button>
                </div>
            </div>
        </div>
    </div>




</section>
<% } catch (Exception e) {
    } finally {
        db.closeConnection();
    }
%>


<%@include file="../../CommonJSP/pageFooter.jsp" %>
