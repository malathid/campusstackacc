<%@page import="CAMPS.Admin.SessionCounter"%>
<%@include file="../../CommonJSP/pageHeader.jsp" %>
<script type="text/javascript" src="../../js/pages/dashboard/admin.js"></script>
 <!-- Jquery DataTable Plugin Js -->
    <script src="../../plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="../../plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="../../plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="../../plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="../../plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="../../plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="../../plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="../../plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="../../plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
<section class="content">
    <div class="row clearfix">
        <% DBConnect db = new DBConnect();
            try {
                db.getConnection();
                db.read("SELECT * FROM (SELECT COUNT(rm.resource_id)resource FROM admin.resource_master rm) rm, (SELECT COUNT(ro.role_id)role FROM admin.role_master ro) ro,(SELECT COUNT(um.user_id)USER FROM admin.user_master um WHERE um.status>0) um");
                if (db.rs.next()) {
        %>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-pink hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">playlist_add_check</i>
                </div>
                <div class="content">
                    <div class="text">Role</div>
                    <div class="number count-to" data-from="0" data-to="<%= db.rs.getString("role") %>" data-speed="15" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-cyan hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">help</i>
                </div>
                <div class="content">
                    <div class="text">Resource</div>
                    <div class="number count-to" data-from="0" data-to="<%= db.rs.getString("resource") %>" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-light-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">forum</i>
                </div>
                <div class="content">
                    <div class="text">User</div>
                    <div class="number count-to" data-from="0" data-to="<%= db.rs.getString("user") %>" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-orange hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">person_add</i>
                </div>
                <div class="content">
                    <div class="text">VISITORS</div>
                    <div class="number count-to" data-from="0" data-to="<%= SessionCounter.getActiveSessions()%>" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
                
        <%
                }
            } catch (Exception e) {

            } finally {
                db.closeConnection();
            }%>
    </div>
    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel-group full-body" id="accordion_18" role="tablist" aria-multiselectable="true">
                 <div class="panel panel-col-blue">
                    <div class="panel-heading" role="tab" id="headingFour_1">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" href="#collapseFour_1" onclick='loadrole()' aria-expanded="false" aria-controls="collapseFour_1">
                                <i class="material-icons">today</i> Role Report
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFour_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour_1">
                        <div class="panel-body table-responsive" id="collapseFour_1pd">

                        </div>
                    </div>
                </div>                                
                 <div class="panel panel-col-grey">
                    <div class="panel-heading" role="tab" id="headingFour_2">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" href="#collapseFour_2" onclick='loadresourse()' aria-expanded="false" aria-controls="collapseFour_2">
                                <i class="material-icons">today</i> Resource Report
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFour_2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour_2">
                        <div class="panel-body table-responsive" id="collapseFour_2pd">

                        </div>
                    </div>
                </div>  
                <div class="panel panel-col-teal">
                    <div class="panel-heading" role="tab" id="headingFour_3">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" href="#collapseFour_3" onclick='loaduser()' aria-expanded="false" aria-controls="collapseFour_3">
                                <i class="material-icons">today</i> Attendance
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFour_3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour_3">
                        <div class="panel-body table-responsive" id="collapseFour_3pd">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<%@include file="../../CommonJSP/pageFooter.jsp" %>