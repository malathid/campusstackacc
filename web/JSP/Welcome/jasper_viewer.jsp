 <%int pageIndex = 0;
        int lastPageIndex = 0;
        StringBuffer sbuffer = new StringBuffer();
        try {
            JasperPrint jasperPrint = (JasperPrint) session.getAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE);
            if (request.getParameter("reload") != null || jasperPrint == null) {
//JRDataSource ds =

//jasperPrint = null;
//JasperViewer.viewReport(jp,false);
//session.setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jasperPrint);
            }
//JRHtmlExporter exporter = new JRHtmlExporter();
            JRExporter exporter = new HtmlExporter();

            if (jasperPrint.getPages() != null) {
                lastPageIndex = jasperPrint.getPages().size() - 1;
            }

            String pageStr = request.getParameter("page");
            try {
                pageIndex = Integer.parseInt(pageStr);
            } catch (Exception e) {
            }

            if (pageIndex < 0) {
                pageIndex = 0;
            }

            if (pageIndex > lastPageIndex) {
                pageIndex = lastPageIndex;
            }

            //  ImageServlet img = new ImageServlet();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STRING_BUFFER, sbuffer);
            exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, "image?image=");
            exporter.setParameter(JRExporterParameter.PAGE_INDEX, Integer.valueOf(pageIndex));
            exporter.setParameter(JRHtmlExporterParameter.HTML_HEADER, "");
            exporter.setParameter(JRHtmlExporterParameter.BETWEEN_PAGES_HTML, "");
            exporter.setParameter(JRHtmlExporterParameter.HTML_FOOTER, "");

            exporter.exportReport();
            if (request.getParameter("save") != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ServletOutputStream sos = response.getOutputStream();
                String fName;
                if (request.getParameter("save").equalsIgnoreCase("print")) {
                    JRPdfExporter pdfexporter = new JRPdfExporter();
                    pdfexporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                    pdfexporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);

                    pdfexporter.setParameter(JRPdfExporterParameter.PDF_JAVASCRIPT, "this.print({bUI: false, bSilent: true, bShrinkToFit: false});");
                    pdfexporter.setParameter(JRPdfExporterParameter.PDF_JAVASCRIPT, "this.closeDoc;");
                    pdfexporter.exportReport();
                    fName = jasperPrint.getName() + ".pdf";
                    //response.
                    response.setContentType("application/pdf");
                    //response.setHeader("Content-type", "application/pdf");
                    response.setContentLength(baos.size());
                }
                if (request.getParameter("save").equalsIgnoreCase("pdf")) {

                    JRPdfExporter pdfexporter = new JRPdfExporter();
                    pdfexporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                    pdfexporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);

                    //pdfexporter.setParameter(JRPdfExporterParameter.PDF_JAVASCRIPT, "this.print({bUI: false, bSilent: true, bShrinkToFit: false});this.closeDoc;");
                    pdfexporter.exportReport();
                    fName = jasperPrint.getName() + ".pdf";
                    response.setContentType("application/pdf");
                    response.setHeader("Content-disposition", "attachment;filename=" + fName);
                    response.setContentLength(baos.size());

                }
                if (request.getParameter("save").equalsIgnoreCase("xls")) {
                    JRXlsExporter xlsexporter = new JRXlsExporter();
                    xlsexporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                    xlsexporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, baos);
                    xlsexporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
                    xlsexporter.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, Boolean.FALSE);
                    xlsexporter.setParameter(JRXlsExporterParameter.IS_IGNORE_CELL_BACKGROUND, Boolean.TRUE);
                    xlsexporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
                    xlsexporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
                    xlsexporter.exportReport();
                    fName = jasperPrint.getName() + ".xls";
                    response.setContentType("application/vnd.ms-excel");
                    response.setHeader("Content-disposition", "attachment;filename=" + fName);
                    response.setContentLength(baos.size());
                }
                if (request.getParameter("save").equalsIgnoreCase("doc")) {
                    JRRtfExporter docexporter = new JRRtfExporter();
                    docexporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                    docexporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, baos);
                    docexporter.exportReport();
                    fName = jasperPrint.getName() + ".doc";
                    response.setContentType("application/vnd.ms-excel");
                    response.setHeader("Content-disposition", "attachment;filename=" + fName);
                    response.setContentLength(baos.size());
                }

                baos.writeTo(sos);
                sos.flush();
                baos.flush();
                sos.close();
                baos.close();
            }
        } catch (Exception ex) {
            out.print(ex); 
        }
    %> 
<%@include file="../../CommonJSP/pageHeader.jsp" %>
<%@page import="net.sf.jasperreports.engine.JRExporter"%>
<%@page import="net.sf.jasperreports.engine.export.JRRtfExporter"%>
<%@page import="net.sf.jasperreports.engine.export.JRXlsExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.export.JRXlsExporter"%>
<%@page import="net.sf.jasperreports.engine.export.JRPdfExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.export.JRPdfExporter"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="net.sf.jasperreports.engine.JasperExportManager"%>
<%@page import="net.sf.jasperreports.j2ee.servlets.ImageServlet"%>
<%@page import="net.sf.jasperreports.engine.export.JRHtmlExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.JRExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.JasperPrint"%>
<%@page import="net.sf.jasperreports.engine.JRDataSource"%>
<%@page import="net.sf.jasperreports.view.JasperDesignViewer"%>
<%@page import="net.sf.jasperreports.view.JasperViewer"%>
<%@page import="net.sf.jasperreports.engine.export.*" %>
<section class="content">
    
    <div align="center" >
    <table width="750" align="center">
   <tr>
    <td align="center">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
  <td align="center">
    <table width="616" height="60" border="0" cellpadding="0" cellspacing="0">
      <tr>
        
        
        <%
	if (pageIndex > 0)
	{
%>
        <td width="23"><a href="viewer.jsp?page=0"><i class="material-icons">skip_previous</i></a></td>
        <td width="23"><a href="viewer.jsp?page=<%=pageIndex - 1%>"><i class="material-icons">navigate_before</i></a></td>
  <%
	}
	else
	{
%>
        <td width="23"><i class="material-icons">skip_previous</i></td>
        <td width="23"><i class="material-icons">navigate_before</i></td>
  <%
	}

	if (pageIndex < lastPageIndex)
	{
%>
        <td width="23"><a href="viewer.jsp?page=<%=pageIndex + 1%>"><i class="material-icons">navigate_next</i></a></td>
        <td width="23"><a href="viewer.jsp?page=<%=lastPageIndex%>"><i class="material-icons">skip_next</i></a></td>
  <%
	}
	else
	{
%>
        <td width="23"><i class="material-icons">navigate_next</i></td>
        <td width="23"><i class="material-icons">skip_next</i></td>
  <%
	}
%><td width="321" >&nbsp;</td>
        <script>
          function printa()
          {
              aa=window.open('printreport.jsp','print');
            
                   }
      </script>
        <td width="49" ><a href="javascript:void(0)" onClick="window.open('jasper_viewer.jsp?save=print','print','location=1,status=1,scrollbars=1,width=750,height=500');"><i class="material-icons">local_printshop</i></a></td>
        <td width="49" ><a href="jasper_viewer.jsp?save=pdf"><i class="material-icons">picture_as_pdf</i></a></td>
        <td width="57" ><a href="jasper_viewer.jsp?save=xls"><i class="material-icons">library_books</i></td>
        <td width="54" ><a href="jasper_viewer.jsp?save=doc"><i class="material-icons">library_add</i></a></td>
      </tr>
      </table>
      </td>
  </tr>
<tr>
  <td align="center" style="background:#FFF">
    
    <%=sbuffer.toString()%>
    
  </td>
  </tr>
</table></td>
  </tr>
</table></div>
</section>
<%@include file="../../CommonJSP/pageFooter.jsp" %>