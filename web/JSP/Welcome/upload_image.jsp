 <html>
     <head>
         <script>
            $(document).ready(function () {

    $("#photo_form1_<%=request.getParameter("user_id")%>").click(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();

        // Get form
        var form = $('#photo_form_<%=request.getParameter("user_id")%>')[0];

		// Create an FormData object 
        var data = new FormData(form);

		// If you want to add an extra field for the FormData
        data.append("option", "upload_photo");

		// disabled the submit button
        $("#btnSubmit").prop("disabled", true);
<%String host_string = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";%>
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "<%=host_string%>Welcome/image_process.do",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {

                $("#result").text(data);
                load_stu(<%=request.getParameter("user_id")%>);
                console.log("SUCCESS : ", data);
                $("#btnSubmit").prop("disabled", false);

            },
            error: function (e) {

                $("#result").text(e.responseText);
                console.log("ERROR : ", e);
                $("#btnSubmit").prop("disabled", false);

            }
        });

    });

});
 function load_user_photo(user_id) {
        var DataString = 'user_id='+user_id+'&user_type=<%=request.getParameter("user_type")%>&option=load_photo';
        $.ajax({
            url: "<%=host_string%>Welcome/image_process.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#image_'+user_id).html(data);                
            }
        });
    }
load_user_photo(<%=request.getParameter("user_id")%>);
         </script>
     </head>
<body>
    <div id="image_<%=request.getParameter("user_id")%>"></div>  
<form name="photo_form_<%=request.getParameter("user_id")%>" id="photo_form_<%=request.getParameter("user_id")%>" method="post" action="<%=host_string%>Welcome/image_process.do"  enctype="multipart/form-data">
<input type="file" name="myimg">
<input type="hidden" name="user_id" value='<%=request.getParameter("user_id")%>' />
<input type="hidden" name="user_type" value='<%=request.getParameter("user_type")%>' />
<input type="button" name="photo_form1" id="photo_form1_<%=request.getParameter("user_id")%>" value="Submit" class="btn btn-primary waves-effect"> 
</form>
</body>
</html> 