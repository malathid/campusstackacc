<%@include file="../../CommonJSP/pageHeader.jsp"%>
<script src="../../plugins/jquery-validation/jquery.validate.js"></script>
<script src="../../plugins/jquery-validation/additional-methods.js"></script>
<script src="../../js/pages/CommonJSP/validation.js"></script>
<script src="../../plugins/bootstrap-notify/bootstrap-notify.js"></script>
<script src="../../plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
<%    DBConnect db = new DBConnect();
    try {

        db.getConnection();

%>
<script>
    function loadstudent_list() {
        if ($('#branch_id').val() !== "" && $('#gender').val() !== "") {
            var DataString = 'branch_id=' + $('#branch_id').val() + '&gender=' + $('#gender').val() + '&option=loadstudent_list';
            $.ajax({
                url: "hostel_details.do", data: DataString, type: "post",
                success: function (data)
                {
                    $('#loadstu_det').html(data);                    
                }
            });
        }
    }

    function loadroom(student_id) {
        var DataString = 'hostel_id=' + $('#hs_' + student_id).val() + '&option=loadroom';
        $.ajax({
            url: "hostel_details.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#rm_'+student_id).html(data).selectpicker('refresh');
               
            }
        });

    }
     function update_hostel(student_id) {
        var DataString = 'room_id=' + $('#rm_' + student_id).val() + '&student_id=' + student_id + '&option=update_hostel';
        $.ajax({
            url: "hostel_details.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#result_process').load('../../CommonJSP/alert_message.jsp?am_c=' + data);
                loadstudent_list();
               
            }
        });

    }
</script>
<section class="content">
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Student Filter 
                    </h2>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                            
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <select class="form-control show-tick" name="branch_id" id="branch_id" onchange="loadstudent_list()">
                                                <option value="">-- Select Class --</option>
                                                <%                                                    db.read("SELECT bm.branch_id,CONCAT(IFNULL(mp.programme_code,''),' ',bm.branch_name) branch_name FROM camps.master_branch bm INNER JOIN camps.master_programme mp ON mp.programme_id=bm.programme_id  WHERE bm.status=1");
                                                    while (db.rs.next()) {
                                                        out.print("<option value=\"" + db.rs.getString("branch_id") + "\">" + db.rs.getString("branch_name") + "</option>");
                                                    }
                                                %>
                                            </select><label class="form-label">Class</label>
                                        </div>                            
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <select class="form-control show-tick" name="gender" id="gender" onchange="loadstudent_list()">
                                                <option value="">-- Select Class --</option>
                                                <option value='Male'>Male</option>
                                                <option value='Female'>Female</option>
                                            </select><label class="form-label">Gender</label>
                                        </div>                            
                                    </div>

                                </div>  

                            </div>
                        </div>

                    </div>
                </div>
            </div>                
        </div>
    </div>
    <div id='print_area' class="row clearfix">
        <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
            <div class="panel-group full-body" id="accordion_19" role="tablist" aria-multiselectable="true">
                <div class="panel panel-col-light-green">
                    <div class="panel-heading" role="tab" id="headingOne_19">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#collapseOne_19" aria-expanded="true" aria-controls="collapseOne_19">
                                <i class="material-icons">perm_contact_calendar</i> Allocate Hostel </a>
                        </h4>
                    </div>
                    <div id="collapseOne_19" class="panel-collapse collapse in"  role="tabpanel" aria-labelledby="headingOne_19">
                        <div class="panel-body" id='loadstu_det'>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<% } catch (Exception e) {
    } finally {
        db.closeConnection();
    }
%>

<%@include file="../../CommonJSP/pageFooter.jsp" %>