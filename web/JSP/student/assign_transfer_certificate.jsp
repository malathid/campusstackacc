<%@include file="../../CommonJSP/pageHeader.jsp" %>
<script>
    function loadSemeter() {
        var DataString = 'ay_id=' + $('#ay_id').val() + '&option=loadSemeter';
        $.ajax({
            url: "student_details.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#term_id').html(data).selectpicker('refresh');
                ;
            }
        });
    }
    function loadStudent() {
        var DataString = 'term_id=' + $('#term_id').val() + '&branch_id=' + $('#branch_id').val() + '&student_status=' + $('#student_status').val() + '&option=loadStudent_TC';
        $.ajax({
            url: "student_details.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#loadstu_det').html(data);
                ;
            }
        });
    }
    function checkall_tcselect() {
        $('[name=tc_select]').prop('checked', true);
    }
    $(document).ready(function () {
        $('#classdate,#tcdate').bootstrapMaterialDatePicker({format: 'DD-MM-YYYY', weekStart: 0, time: false});
    });
</script>
<%    DBConnect db = new DBConnect();
    try {

        db.getConnection();

%>
<section class="content">
    <form id="form1" method="post" action="student_details.do" >
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Student Filter 
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <select class="form-control show-tick" name="ay_id" id="ay_id" onchange="loadSemeter()">
                                                    <option value="">-- Select Acad Year --</option>
                                                    <%                                            db.read("SELECT CONCAT('<option value=\"',may.ay_id,'\" ', IF(may.cur_year=1,'selected',''),' >',may.acc_year,'</option>') val FROM camps.master_academic_year may");
                                                        while (db.rs.next()) {
                                                            out.print(db.rs.getString("val"));
                                                        }
                                                    %>
                                                </select><label class="form-label">Acad Year</label>
                                            </div>                            
                                        </div>

                                    </div>
                                    <div class="col-sm-6">
                                        <!--                                    <div class="form-group form-float">
                                                                                <div class="form-line">
                                                                                    <select class="form-control show-tick" name="term_id" id="term_id">
                                                                                <option value="">-- Select Term --</option>
                                        
                                                                            </select><label class="form-label">Term</label>
                                                                                </div>                            
                                                                            </div>-->
                                        <div class="form-group form-float">
                                            <div class="form-line focused">
                                                <select class="form-control show-tick" name="term_id" id="term_id">
                                                    <option value="">-- Select Term --</option>
                                                </select><label class="form-label">Term</label>
                                            </div>                            
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group form-float">
                                            <div class="form-line focused">
                                                <select class="form-control show-tick" data-actions-box="true" multiple  name="branch_id" id="branch_id" onchange="loadStudent()">                                                
                                                    <%
                                                        db.read("SELECT bm.branch_id,CONCAT(IFNULL(mp.programme_code,''),' ',bm.branch_name) branch_name FROM camps.master_branch bm INNER JOIN camps.master_programme mp ON mp.programme_id=bm.programme_id  WHERE bm.status=1");
                                                        while (db.rs.next()) {
                                                            out.print("<option value=\"" + db.rs.getString("branch_id") + "\">" + db.rs.getString("branch_name") + "</option>");
                                                        }
                                                    %>
                                                </select><label class="form-label">Class</label>
                                            </div>                            
                                        </div>

                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-float">
                                            <div class="form-line focused">
                                                <select class="form-control show-tick" name="student_status" id="student_status" onchange="loadStudent()">
                                                    <option value="">-- Select Status --</option>
                                                    <option value="Completed">-- Completed --</option>
                                                </select><label class="form-label">Student Status</label>
                                            </div>                            
                                        </div>

                                    </div>

                                </div>
                                                <center>
                                       <button class="btn btn-primary waves-effect" formnovalidate type="submit" value="SUBMIT123" name="insert_tc_details">Generate TC</button>
                                    </center>
                            </div>

                        </div>
                    </div>
                </div>
            </div></div>
        <div class="row clearfix">
            <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                <div class="panel-group full-body" id="accordion_19" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-col-teal">
                        <div class="panel-heading" role="tab" id="headingOne_19">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#collapseOne_19" aria-expanded="true" aria-controls="collapseOne_19">
                                    <i class="material-icons">perm_contact_calendar</i> Student List</a>
                            </h4>
                        </div>
                        <div id="collapseOne_19" class="panel-collapse collapse in"  role="tabpanel" aria-labelledby="headingOne_19">
                            <div class="panel-body" id='loadstu_det'>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            TC Details
                            <%
                                db.read("SELECT IFNULL(MAX(stc.stc_id),0)+1 tcnumber FROM camps.student_transfer_certificate stc ");
                                if (db.rs.next()) {
                                    out.print(" ( Next TC NO:" + db.rs.getString("tcnumber") + ") ");
                                }
                            %> 
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="classdate" id="classdate" required>
                                                <label class="form-label">Last Class Attended Date</label>
                                            </div>                            
                                        </div>

                                    </div>
                                    <div class="col-sm-6">                                
                                        <div class="form-group form-float">
                                            <div class="form-line ">  
                                                <input type="text" class="form-control" name="tcdate" id="tcdate" required>
                                                <label class="form-label">Date of TC</label>
                                            </div>                            
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group form-float">
                                            <div class="form-line focused">   
                                                <select class="form-control show-tick" name="cc" id="cc">
                                                    <option value="">-- Select Status --</option>
                                                    <option value="Good">Good</option>
                                                    <option value="Bad">Bad</option>
                                                </select><label class="form-label">Conduct and Character</label>
                                            </div>                            
                                        </div>

                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-float">
                                            <div class="form-line focused">
                                                <select class="form-control show-tick" name="promotion" id="promotion" >
                                                    <option value="">-- Select Promotion --</option>
                                                    <option value="Yes">Yes</option>
                                                    <option value="No">No</option>
                                                    <option value="Refer Mark Sheet">Refer Mark Sheet</option>
                                                </select> <label class="form-label">Promotion</label>
                                            </div>                            
                                        </div>

                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group form-float form-group-lg">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="reason">
                                                <label class="form-label">Reason (If Duplicate)</label>
                                            </div>
                                        </div>

                                    </div>
                                    <center>
                                        <button class="btn btn-primary waves-effect" type="submit" value="SUBMIT" name="insert_tc_details">SUBMIT</button>                                        
                                    </center>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        

    </form>
</section>
<% } catch (Exception e) {
        out.print(e);
    } finally {
        db.closeConnection();
    }
%>
<script>
    loadSemeter();
</script>
<%@include file="../../CommonJSP/pageFooter.jsp" %>