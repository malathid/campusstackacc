<%@include file="../../CommonJSP/pageHeader.jsp" %>
<script src="../../plugins/bootstrap-notify/bootstrap-notify.js"></script>
<script src="../../plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="../../plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="../../plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="../../plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="../../plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="../../plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="../../plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="../../plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="../../plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
<script>

    function load_staff() {
        var DataString = 'dept_id=' + $('#dept_id').val() + '&option=loadstaff1';
        $.ajax({
            url: "staff_Profile.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#staff_id').html(data).selectpicker('refresh');
            }
        });
    }
    function load_report() {
        var title = 'Staff Leave Report from ' + $('#from_date').val() + ' to ' + $('#to_date').val();
        var DataString = 'staff_id=' + $('#staff_id').val() + '&leave_type=' + $('#leave_type').val() + '&approval_type=' + $('#approval_type').val() + '&from_date=' + $('#from_date').val() + '&to_date=' + $('#to_date').val() + '&option=load_leave_report';
        $.ajax({
            url: "leave_Details.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#load_leave').html(data)
                $('#load_leave .table').DataTable({
                    "processing": true,
                    "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
                    dom: 'lBfrtip',
                    responsive: true,
                    buttons: [
                        'copy', 'csv', 'excel', {
                            extend: 'pdf',
                            title: title,
                            orientation: 'portrait',
                            pageSize: 'A4'
                        }, {
                            extend: 'print',
                            title: title,
                            orientation: 'portrait',
                            pageSize: 'A4'
                        }
                    ]

                });
//                $('#load_leave tfoot td').each(function () {
//                    var title = $(this).text();
//                    $(this).html('<input type="text" placeholder="Search ' + title + '" />');
//                });
//
//                // DataTable
//                var table = $('#load_leave').DataTable();
//                
//                // Apply the search
//                table.columns().every(function () {
//                    var that = this;
//
//                    $('input', this.footer()).on('keyup change', function () {
//                        if (that.search() !== this.value) {
//                            that
//                                    .search(this.value)
//                                    .draw();
//                        }
//                    });
//                });

            }
        });
    }

</script>

<%    DBConnect db = new DBConnect();
    try {

        db.getConnection();

%>

<section class="content">
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Filter Options 
                    </h2>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <%db.read("SELECT DISTINCT(fia.fia_id) fia_id FROM admin.form_ia_role fir INNER JOIN admin.form_item_access fia ON fia.fia_id=fir.fia_id WHERE fir.role_id IN (" + session.getAttribute("roles") + ")  AND fia.fia_id in(5) AND fia.status>0 AND fir.status>0");
                            if (db.rs.next()) {%>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select class="form-control show-tick" name="dept_id"  data-live-search="true" data-actions-box="true" multiple id="dept_id" onchange="load_staff()" >

                                                <%
                                                    db.read1("SELECT DISTINCT(fia.fia_id) fia_id FROM admin.form_ia_role fir INNER JOIN admin.form_item_access fia ON fia.fia_id=fir.fia_id WHERE fir.role_id IN (" + session.getAttribute("roles") + ")  AND fia.fia_id in(6) AND fia.status>0 AND fir.status>0");
                                                    if (db.rs1.next()) {
                                                        db.read("SELECT CONCAT('<option value=\"',md.department_id,'\" >',md.dept_name,'</option>') val FROM camps.master_department md WHERE md.status>0 and md.department_id="+session.getAttribute("depId")+"");
                                                    } else {
                                                        db.read("SELECT CONCAT('<option value=\"',md.department_id,'\" >',md.dept_name,'</option>') val FROM camps.master_department md WHERE md.status>0");
                                                    }
                                                    while (db.rs.next()) {
                                                        out.print(db.rs.getString("val"));
                                                    }
                                                %>
                                            </select><label class="form-label">Department</label>
                                        </div>                            
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select class="form-control show-tick" name="staff_id" data-live-search="true" data-actions-box="true" multiple id="staff_id" onchange="load_report()" >

                                            </select><label class="form-label">Staff Name</label>
                                        </div> 

                                    </div>

                                </div>


                            </div>
                        </div>
                        <%}%>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">                             
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select class="form-control show-tick" data-actions-box="true" data-live-search="true" multiple name="leave_type" id="leave_type"  onchange="load_report()">
                                                <%
                                                    db.read("SELECT CONCAT('<option value=\"',slm.slm_id,'\" >',slm.leave_name,'</option>') val FROM staff_leave_master slm ORDER BY slm.leave_name");
                                                    while (db.rs.next()) {
                                                        out.print(db.rs.getString("val"));
                                                    }
                                                %>
                                            </select><label class="form-label">Leave Type</label>
                                        </div>                            
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select class="form-control show-tick" data-actions-box="true" multiple data-live-search="true" name="approval_type" id="approval_type"  onchange="load_report()">
                                                <option value="1">Pending</option>
                                                <option value="2">Approved</option>
                                                <option value="3">Declined</option>
                                            </select><label class="form-label">Approval Type</label>
                                        </div>                            
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">                             
                                <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'>
                                    <div class='form-group form-float'>
                                        <div class='form-line focused'>
                                            <input type='text' pattern='[0-9]{2}-[0-9]{2}-[0-9]{4}' name='from_date' id='from_date'  class='form-control'  onchange="load_report()" />
                                            <label class='form-label'>From Date</label>
                                            <script>$('#from_date').bootstrapMaterialDatePicker({format: 'DD-MM-YYYY', weekStart: 0, time: false});</script></div>
                                    </div></div>

                                <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'>
                                    <div class='form-group form-float'>
                                        <div class='form-line focused'>
                                            <input type='text' pattern='[0-9]{2}-[0-9]{2}-[0-9]{4}' name='to_date' id='to_date'  class='form-control'  onchange="load_report()" />
                                            <label class='form-label'>To Date</label>
                                            <script>$('#to_date').bootstrapMaterialDatePicker({format: 'DD-MM-YYYY', weekStart: 0, time: false});</script></div>
                                    </div></div>

                            </div>
                        </div>
                        <!--                        <center>
                                                    <button  class="btn btn-primary waves-effect" name="option" id="option" value='submit_report' onclick='submit_report()' type="button">SUBMIT</button>
                                                </center>-->
                    </div>
                </div>
            </div>
        </div>



        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">

                    <div class="body"><div class="table-responsive"  id='load_leave'>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</section>


<% } catch (Exception e) {
    } finally {
        db.closeConnection();
    }
%>


<%@include file="../../CommonJSP/pageFooter.jsp" %>