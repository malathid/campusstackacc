<%@include file="../../CommonJSP/pageHeader.jsp" %>

<script src="../../plugins/bootstrap-notify/bootstrap-notify.js"></script>
<script src="../../plugins/printthis/printThis.js"></script>

<%    DBConnect db = new DBConnect();
    try {

        db.getConnection();

%>
<section class="content">
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Staff Log
                    </h2>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <%
                                   db.read("SELECT date_format(fn_date,'%d-%m-%Y')fn_date,fn_date fn_date1,IFNULL(GROUP_CONCAT( DISTINCT CASE WHEN leave_name IS NOT NULL AND lses='FN' THEN leave_name WHEN ses IS NOT NULL AND ses='FN' AND late='0' THEN DATE_FORMAT(device_date,'%r') WHEN ses IS NOT NULL AND ses='FN' AND late='1' THEN CONCAT(DATE_FORMAT(device_date,'%r'),'(Late)')  END ) ,'LOP')fn,IFNULL(GROUP_CONCAT(CASE WHEN leave_name IS NOT NULL AND lses='AN' THEN leave_name WHEN ses IS NOT NULL AND  ses='AN' AND late='0' THEN DATE_FORMAT(device_date,'%r') WHEN ses IS NOT NULL AND ses='AN' AND late='1' THEN CONCAT(DATE_FORMAT(device_date,'%r'),'(Late)')  END ) ,'LOP')an  FROM (SELECT a.staff_id,fn_date,ul.device_date,CASE WHEN TIME(ul.device_date) BETWEEN fn_from AND fn_to THEN 'FN' WHEN TIME(ul.device_date) BETWEEN an_from AND an_to THEN 'AN' WHEN TIME(ul.device_date) BETWEEN fn_to AND fn_to + INTERVAL 30 MINUTE THEN 'FN' WHEN TIME(ul.device_date) BETWEEN an_to AND an_to + INTERVAL 30 MINUTE  THEN 'AN' END ses, CASE WHEN TIME(ul.device_date) BETWEEN fn_to AND fn_to + INTERVAL 30 MINUTE THEN '1' WHEN TIME(ul.device_date) BETWEEN an_to AND an_to + INTERVAL 30 MINUTE  THEN '1' ELSE '0'  END late,slm.leave_name,b.ses lses FROM (SELECT sm.staff_id,ct.dt fn_date,CASE WHEN sbsa.sbsm_id IS NOT NULL THEN sbsm.fn_time - INTERVAL sbsm.fn_before MINUTE WHEN sbs.staff_id IS NOT NULL THEN sbs.fn_time - INTERVAL sbs.fn_before MINUTE ELSE sbt.fn_time - INTERVAL sbt.fn_before MINUTE END fn_from,CASE WHEN sbsa.sbsm_id IS NOT NULL THEN sbsm.fn_time  WHEN sbs.staff_id IS NOT NULL THEN sbs.fn_time  ELSE sbt.fn_time END fn_to,IF(sbsm.end_day='next day',ct.dt+ INTERVAL 1 DAY,ct.dt)an_date,CASE WHEN sbsa.sbsm_id IS NOT NULL THEN sbsm.an_time - INTERVAL sbsm.an_before MINUTE WHEN sbs.staff_id IS NOT NULL THEN sbs.an_time - INTERVAL sbs.an_before MINUTE ELSE sbt.an_time - INTERVAL sbt.an_before MINUTE END an_from,CASE WHEN sbsa.sbsm_id IS NOT NULL THEN sbsm.an_time  WHEN sbs.staff_id IS NOT NULL THEN sbs.an_time  ELSE sbt.an_time END an_to FROM camps.calendar_table ct INNER JOIN camps.staff_master sm ON sm.working_status='working' AND ct.dt BETWEEN DATE(NOW()- INTERVAL 50 DAY) AND DATE(NOW()) AND sm.staff_id='"+session.getAttribute("ss_id")+"' LEFT JOIN (camps.staff_biometric_shift_allocation sbsa INNER JOIN camps.staff_biometric_shift_master sbsm ON sbsa.sbsm_id=sbsm.sbsm_id AND sbsm.status>0) ON ct.dt BETWEEN sbsa.from_date AND sbsa.to_date AND sbsa.staff_id=sm.staff_id LEFT JOIN camps.staff_biometric_special sbs ON sbs.staff_id=sm.staff_id AND sbs.status>0 LEFT JOIN camps.staff_biometric_timing sbt ON CASE WHEN sbt.day_timing=ct.dayName THEN sbt.day_timing=ct.dayName ELSE sbt.day_timing='all days' END) a JOIN (SELECT 'FN' ses UNION SELECT 'AN')b LEFT JOIN biometric.user_log ul ON (DATE(ul.device_date)=fn_date OR DATE(ul.device_date)=an_date) AND ul.user_id=a.staff_id LEFT JOIN (camps.staff_leave_request slr INNER JOIN camps.staff_leave_master slm ON slm.slm_id=slr.slm_id AND slr.status=2) ON slr.staff_id=a.staff_id AND a.fn_date BETWEEN DATE(slr.from_date) AND DATE(slr.to_date) AND CASE WHEN a.fn_date=DATE(slr.from_date) AND slr.from_session='AN' THEN slr.from_session=b.ses WHEN a.fn_date=DATE(slr.to_date) AND slr.to_session='FN' THEN slr.to_session=b.ses ELSE TRUE END )a GROUP BY a.fn_date ORDER BY fn_date1 DESC");
                                    String data="<table class='tbl' width='100%'><tr><th>Date</th><th>Forenoon</th><th>Afternoon</th></tr>";
                                    while(db.rs.next()){
                                        data+="<tr><td align='center'>"+db.rs.getString("fn_date") +"</td><td align='center'>"+db.rs.getString("fn") +"</td><td align='center'>"+db.rs.getString("an") +"</td></tr>";
                                    }
                                    data+="</table>";
                                    out.print(data);
                                %>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <center><input type="button" onclick='printout()'/></center>
</section>
<% } catch (Exception e) {
    } finally {
        db.closeConnection();
    }
%>


<%@include file="../../CommonJSP/pageFooter.jsp" %>