<%@include file="../../CommonJSP/pageHeader.jsp" %>
<script src="../../plugins/jquery-validation/jquery.validate.js"></script>
<script src="../../plugins/jquery-validation/additional-methods.js"></script>
<script src="../../js/pages/CommonJSP/validation.js"></script>
<script src="../../plugins/bootstrap-notify/bootstrap-notify.js"></script>
<script src="../../plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
<script src="../../plugins/printthis/printThis.js"></script>
<script>
    function load_leave_details() {
        var DataString = 'option=loadleave_det';
        $.ajax({
            url: "leave_Details.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#loadleave_det').html(data);
            }
        });
    }
    function load_leave_from_to() {
        var DataString = 'leave_type=' + $('#leave_type').val() + '&option=load_leave_from_to';
        $.ajax({
            url: "leave_Details.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#load_leave_from_to').html(data);
                validate();
            }
        });
    }
    function load_leave_status() {
        var DataString = '&option=load_leave_status';
        $.ajax({
            url: "leave_Details.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#load_leave_status').html(data);

            }
        });
    }
    function cancel_leave(leave_id) {
        var DataString = 'leave_id=' + leave_id + '&option=cancel_leave';
        $.ajax({
            url: "leave_Details.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#result_process').load('../../CommonJSP/alert_message.jsp?am_c=' + data);
                load_leave_details();
                load_leave_from_to();
                load_leave_status();
            }
        });
    }
    function submit_leave1() {
        if ($('#days1').val() !== ''&& $('#leave_type').val()!=='' && $('#from_date').val()!=='' && $('#to_date').val()!=='' && $('#from_ses').val()!==''&& $('#to_ses').val()!==''&& $('#Reason').val()!=='') {
            var DataString = 'leave_type=' + $('#leave_type').val() + '&from_date=' + $('#from_date').val() + '&to_date=' + $('#to_date').val() + '&from_ses=' + $('#from_ses').val() + '&to_ses=' + $('#to_ses').val() + '&reason=' + $('#Reason').val() + '&option=submit_leave1';
            $.ajax({
                url: "leave_Details.do", data: DataString, type: "post",
                success: function (data)
                {
                    $('#result_process').load('../../CommonJSP/alert_message.jsp?am_c=' + data);
                    $('#leave_type').val("");
                    load_leave_details();
                    load_leave_from_to();
                    load_leave_status();
                }
            });
        } else
        {
            showNotification("alert-danger", "Check all Fields", "top", "center", "", "", false, "glyphicon glyphicon-remove-circle");
        }
    }
    function reformatDate(dateStr)
    {

        dArr = dateStr.split("-");  // ex input "2010-01-18"
        return dArr[2] + "/" + dArr[1] + "/" + dArr[0]; //ex out: "18/01/10"
    }
    function nodays() {
        if ($("#from_date").val() != '' && $("#to_date").val() != '' && $("#from_ses").val() != '' && $("#to_ses").val() != '')
        {
            if ($("#from_date").val() <= $("#to_date").val())
            {
                var val1;
                var val2;
                var diffvar;
                var noofdays;
                if ($("#from_ses").val() == "FN" || $("#from_ses").val() == "AN") {
                    val1 = 0.5;
                } else {
                }
                if ($("#to_ses").val() == "FN" || $("#to_ses").val() == "AN") {
                    val2 = 0.5;
                } else {
                }
                if ($("#from_date").val() == $("#to_date").val()) {
                    if ($("#to_ses").val() == $("#from_ses").val()) {
                        noofdays = val1;
                    } else {
                        if ($("#from_ses").val() == "AN" && $("#to_ses").val() == "FN") {
                            alert("Invalid session selection");
                        } else {
                            noofdays = 1;
                        }
                    }
                } else {
                    var d1 = reformatDate($("#from_date").val());
                    var d2 = reformatDate($("#to_date").val());
                    var diffvar = ((new Date(d2) - new Date(d1)) / 86400000);
                    if ($("#to_ses").val() == $("#from_ses").val()) {
                        noofdays = val1 + diffvar;
                    } else {
                        if ($("#from_ses").val() == "AN" && $("#to_ses").val() == "FN") {
                            noofdays = val1 + diffvar + val2 - 1;
                        } else {
                            noofdays = val1 + diffvar + val2;
                        }
                    }
                }
                if (noofdays <= $("#leave_type :checked").data("balance"))
                {
                    $('#days1').val(noofdays);
                    submit_leave1();
                } else
                {
                    showNotification("alert-danger", "Leave Balance Exceeded!!", "top", "center", "", "", false, "glyphicon glyphicon-remove-circle");
                }

            } else {
                showNotification("alert-danger", "Date Mismatch", "top", "center", "", "", false, "glyphicon glyphicon-remove-circle");
                $('#days1').val("");
            }
        } else
        {
            showNotification("alert-danger", "Don't Try to Leave Apply Without Dates!!", "top", "center", "", "", false, "glyphicon glyphicon-remove-circle");
            $('#days1').val("");

        }

    }
    load_leave_details();  
    load_leave_from_to();
    load_leave_status();
</script>

<%    DBConnect db = new DBConnect();
    try {

        db.getConnection();

%>

<section class="content">
    <form id="form1" id="form_validation" method="post" action="leave_Details.do" >
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Staff Details
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id='print_area' class="row clearfix">
            <div class="col-xs-13 ol-sm-13 col-md-13 col-lg-13">
                <div class="panel-group full-body" id="accordion_19" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-col-purple">
                        <div class="panel-heading" role="tab" id="headingOne_19">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#collapseOne_19" aria-expanded="true" aria-controls="collapseOne_19">
                                    <i class="material-icons">perm_contact_calendar</i> Leave Details </a>
                            </h4>
                        </div>
                        <div id="collapseOne_19" class="panel-collapse collapse in"  role="tabpanel" aria-labelledby="headingOne_19">
                            <div class="panel-body" id='loadleave_det'>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-14 ol-sm-14 col-md-14 col-lg-14">
                <div class="panel-group full-body" id="accordion_20" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-col-light-blue">
                        <div class="panel-heading" role="tab" id="headingOne_20">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#collapseOne_20" aria-expanded="true" aria-controls="collapseOne_20">
                                    <i class="material-icons">touch_app</i> Apply Leave </a>
                            </h4>
                        </div>
                        <div id="collapseOne_20" class="panel-collapse collapse in"  role="tabpanel" aria-labelledby="headingOne_20">
                            <div class="panel-body" >
                                <!--  <form id="from1">-->
                                <div class="form-group form-float" >
                                    <div class="form-line focused">
                                        <select class="form-control" required name='leave_type' id='leave_type' onchange="load_leave_from_to()">
                                            <option value=''><--Select--></option>
                                            <%                                                db.read("SELECT slm.slm_id,slm.count_query,slm.leave_name,slc.leave_count,COUNT(count_data.slm_id)*0.5 leave_avalied,if(slc.leave_count is null,7, slc.leave_count-COUNT(count_data.slm_id)*0.5) remaining  FROM staff_leave_master slm left JOIN staff_leave_count slc ON slc.slm_id=slm.slm_id AND NOW() BETWEEN slc.from_date AND slc.to_date AND slm.status>0 AND slc.status>0 LEFT JOIN (SELECT slr.slm_id,slr.from_date,slr.from_session,slr.to_date,slr.to_session,ct.dt,ses FROM camps.calendar_table ct JOIN (SELECT 'FN' ses,1 orn UNION SELECT 'AN',2)ses INNER JOIN staff_leave_request slr ON ct.dt BETWEEN DATE(slr.from_date) AND DATE(slr.to_date) AND slr.status=1 AND slr.staff_id='" + session.getAttribute("ss_id") + "' AND (CASE WHEN DATE(slr.from_date)=ct.dt AND slr.from_session='AN' THEN slr.from_session=ses WHEN DATE(slr.to_date)=ct.dt AND slr.to_session='FN' THEN slr.to_session=ses ELSE TRUE END) ORDER BY ct.dt,orn) count_data ON count_data.dt BETWEEN slc.from_date AND slc.to_date AND slm.slm_id=count_data.slm_id where slc.staff_id='" + session.getAttribute("ss_id") + "' or slm.reset_period is null GROUP BY slm.slm_id having (leave_count-leave_avalied)>0 or leave_count is null");
                                                while (db.rs.next()) {
                                                    if (db.rs.getString("count_query") != null) {
                                                        db.read1(db.rs.getString("count_query").replaceAll("__staff_id__", session.getAttribute("ss_id").toString()));
                                                        if (db.rs1.next() && db.rs1.getInt("leave_count") > 0) {
                                                            out.print("<option value='" + db.rs.getString("slm_id") + "' data-balance='" + db.rs1.getString("leave_count") + "' >" + db.rs.getString("leave_name") + "</option>");
                                                        }
                                                    } else {
                                                        out.print("<option value='" + db.rs.getString("slm_id") + "' data-balance='" + db.rs.getString("remaining") + "' >" + db.rs.getString("leave_name") + "</option>");
                                                    }
                                                }
                                            %>
                                        </select><label class="form-label">Leave Type</label>                                    
                                    </div>
                                </div>
                                <div id="load_leave_from_to"></div>


                                <!--</form>-->
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-15 ol-sm-15 col-md-15 col-lg-15">
                <div class="panel-group full-body" id="accordion_21" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-col-deep-purple">
                        <div class="panel-heading" role="tab" id="headingOne_21">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#collapseOne_21" aria-expanded="true" aria-controls="collapseOne_21">
                                    <i class="material-icons">view_comfy</i> Approval details </a>
                            </h4>
                        </div>
                        <div id="collapseOne_21" class="panel-collapse collapse in"  role="tabpanel" aria-labelledby="headingOne_21">
                            <div class="panel-body" id='load_leave_status'>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>      
    </form>

</section>

<% } catch (Exception e) {
    } finally {
        db.closeConnection();
    }
%>
<div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="largeModalLabel">Student Details</h4>
            </div>
            <div class="modal-body" id="student_det">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect">SAVE CHANGES</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<%@include file="../../CommonJSP/pageFooter.jsp" %>
