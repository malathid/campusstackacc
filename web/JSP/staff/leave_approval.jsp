<%@include file="../../CommonJSP/pageHeader.jsp" %>
<script src="../../plugins/jquery-validation/jquery.validate.js"></script>
<script src="../../plugins/jquery-validation/additional-methods.js"></script>
<script src="../../js/pages/CommonJSP/validation.js"></script>
<script src="../../plugins/bootstrap-notify/bootstrap-notify.js"></script>
<script src="../../plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
<script src="../../plugins/printthis/printThis.js"></script>
<script>
     function load_leave_details() {
        var DataString = 'level='+$('#level').val()+'&option=load_hod_leave';
        $.ajax({
            url: "leave_Details.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#load_leave_details').html(data);

            }
        });
    }
    function decline_leave(leave_id) {
        var DataString = 'leave_id=' + leave_id + '&option=decline_leave';
        $.ajax({
            url: "leave_Details.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#result_process').load('../../CommonJSP/alert_message.jsp?am_c=' + data);
                 load_leave_details();
            }
        });
    }
    function approve_leave(leave_id,slaf_id,level) {
        var DataString = 'leave_id=' + leave_id +'&slaf_id=' + slaf_id + '&level=' + level + '&option=approve_leave';
       $.ajax({
            url: "leave_Details.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#result_process').load('../../CommonJSP/alert_message.jsp?am_c=' + data);
                 load_leave_details();
            }
        });
    }
     function decline_leave(leave_id,slaf_id,level) {
        var DataString = 'leave_id=' + leave_id +'&slaf_id=' + slaf_id + '&level=' + level + '&option=decline_leave';
        $.ajax({
            url: "leave_Details.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#result_process').load('../../CommonJSP/alert_message.jsp?am_c=' + data);
                 load_leave_details();
            }
        });
    }
 </script>

<%    DBConnect db = new DBConnect();
    try {

        db.getConnection();

%>

<section class="content">
    <form id="form1" id="form_validation" method="post" action="leave_Details.do" >
        <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                       Select Level
                    </h2>
                </div>
                <div class="body" >
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                            
                            <div class="row">
                                <div class="col-sm-3" >
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <select class="form-control show-tick" name="level" id="level" onchange="load_leave_details()">
                                                <option value="">-- Select Level --</option>
                                                <%                                                   // db.read("SELECT DISTINCT(slaf.level),itm.incharge_name FROM camps.staff_leave_approval_flow slaf INNER JOIN camps.incharge_type_master itm ON itm.itm_id=slaf.itm_id WHERE slaf.status>0");
                                                    db.read("SELECT CONCAT('Level ',slaf.level,'-',itm.fia_name) incharge_name,slaf.level FROM admin.user_master um INNER JOIN admin.form_ia_role fia  ON FIND_IN_SET(fia.role_id,um.roles) AND fia.status>0 INNER JOIN admin.form_item_access itm ON itm.fia_id=fia.fia_id INNER JOIN camps.staff_leave_approval_flow slaf ON slaf.fia_id=fia.fia_id AND slaf.status>0 INNER JOIN camps.staff_leave_master slm ON slm.slm_id=slaf.slm_id WHERE um.staff_id='" + session.getAttribute("ss_id") + "' GROUP BY slaf.level  ORDER BY slaf.level");
                                                    while (db.rs.next()) {
                                                        out.print("<option value=\"" + db.rs.getString("level") + "\">" + db.rs.getString("incharge_name") + "</option>");
                                                    }
                                                %>
                                            </select><label class="form-label">Approval Level</label>
                                        </div>                            
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>                
        </div>
    </div>
         
               <div id='print_area' class="row clearfix">
                   
            <div class="col-xs-15 ol-sm-15 col-md-15 col-lg-15">
                <div class="panel-group full-body" id="accordion_21" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-col-deep-purple">
                        <div class="panel-heading" role="tab" id="headingOne_21">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#collapseOne_21" aria-expanded="true" aria-controls="collapseOne_21">
                                    <i class="material-icons">traffic</i>Leave Approval </a>
                            </h4>
                        </div>
                        <div id="collapseOne_21" class="panel-collapse collapse in"  role="tabpanel" aria-labelledby="headingOne_21">
                            <div class="panel-body" id='load_leave_details'>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>     
    </form>

</section>

<% } catch (Exception e) {
    } finally {
        db.closeConnection();
    }
%>


<%@include file="../../CommonJSP/pageFooter.jsp" %>
