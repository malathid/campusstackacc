<%@include file="../../CommonJSP/pageHeader.jsp" %>

<script src="../../plugins/bootstrap-notify/bootstrap-notify.js"></script>
<script>
    function loadSection() {
        var DataString = 'branch_id=' + $('#branch_id').val() + '&term_id=' + $('#term_id').val() + '&option=loadSection';
        $.ajax({
            url: "student_list.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#section').html(data).selectpicker('refresh');
                ;
            }
        });
    }
    function loadSemeter() {
        var DataString = 'ay_id=' + $('#ay_id').val() + '&option=loadSemeter';
        $.ajax({
            url: "student_list.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#term_id').html(data).selectpicker('refresh');
                ;
            }
        });
    }
    function loadStudent() {
        var DataString = 'branch_id=' + $('#branch_id').val() + '&term_id=' + $('#term_id').val() + '&ay_id=' + $('#ay_id').val() + '&section=' + $('#section').val() + '&option=loadStudent';
        $.ajax({
            url: "student_list.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#loadstu_det').html(data);
                ;
            }
        });
    }
</script>
<%    DBConnect db = new DBConnect();
    try {

        db.getConnection();

%>
<section class="content">
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Student Filter 
                    </h2>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select class="form-control show-tick" name="ay_id" id="ay_id" onchange="loadSemeter()">
                                                <option value="">-- Select Acad Year --</option>
                                                <%                                            db.read("SELECT CONCAT('<option value=\"',may.ay_id,'\" ', IF(may.cur_year=1,'selected',''),' >',may.acc_year,'</option>') val FROM camps.master_academic_year may");
                                                    while (db.rs.next()) {
                                                        out.print(db.rs.getString("val"));
                                                    }
                                                %>
                                            </select><label class="form-label">Acad Year</label>
                                        </div>                            
                                    </div>

                                </div>
                                <div class="col-sm-6">
<!--                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select class="form-control show-tick" name="term_id" id="term_id">
                                        <option value="">-- Select Term --</option>

                                    </select><label class="form-label">Term</label>
                                        </div>                            
                                    </div>-->
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <select class="form-control show-tick" name="term_id" id="term_id">
                                                <option value="">-- Select Term --</option>
                                            </select><label class="form-label">Term</label>
                                        </div>                            
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <select class="form-control show-tick" name="branch_id" id="branch_id" onchange="loadSection()">
                                                <option value="">-- Select Class --</option>
                                                <%
                                                    db.read("SELECT bm.branch_id,CONCAT(IFNULL(mp.programme_code,''),' ',bm.branch_name) branch_name FROM camps.master_branch bm INNER JOIN camps.master_programme mp ON mp.programme_id=bm.programme_id  WHERE bm.status=1");
                                                    while (db.rs.next()) {
                                                        out.print("<option value=\"" + db.rs.getString("branch_id") + "\">" + db.rs.getString("branch_name") + "</option>");
                                                    }
                                                %>
                                            </select><label class="form-label">Class</label>
                                        </div>                            
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select class="form-control show-tick" name="section" multiple id="section" onchange="loadStudent()">
                                                <option value="">-- Select Section --</option>

                                            </select><label class="form-label">Section</label>
                                        </div>                            
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
            <div class="panel-group full-body" id="accordion_19" role="tablist" aria-multiselectable="true">
                <div class="panel panel-col-teal">
                    <div class="panel-heading" role="tab" id="headingOne_19">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#collapseOne_19" aria-expanded="true" aria-controls="collapseOne_19">
                                <i class="material-icons">perm_contact_calendar</i> Student List </a>
                        </h4>
                    </div>
                    <div id="collapseOne_19" class="panel-collapse collapse in"  role="tabpanel" aria-labelledby="headingOne_19">
                        <div class="panel-body" id='loadstu_det'>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<% } catch (Exception e) {
    } finally {
        db.closeConnection();
    }
%>
<script>
    loadSemeter();
</script>

<%@include file="../../CommonJSP/pageFooter.jsp" %>