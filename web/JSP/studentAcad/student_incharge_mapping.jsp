<%@include file="../../CommonJSP/pageHeader.jsp" %>
<script src="../../plugins/bootstrap-notify/bootstrap-notify.js"></script>
<script>

    function load_staff() {
        var DataString = 'dept_id=' + $('#dept_id').val() + '&option=loadstaff_incharge';
        $.ajax({
            url: "student_list.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#staff_id').html(" <option value=''>-- Select Staff --</option>" + data).selectpicker('refresh');
            }
        });
    }
    function insert_student_incharge() {
        var DataString = 'staff_id=' + $('#staff_id').val() + '&incharge_id=' + $('#incharge_id').val() + '&student_id=' + $('#student_id').val() + '&option=insert_student_incharge';
        $.ajax({
            url: "student_list.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#result_process').load('../../CommonJSP/alert_message.jsp?am_c=' + data);
                load_student_det();
            }
        });
    }
    function delete_student_incharge(student_id, incharge_id) {
        var DataString = 'student_id=' + student_id + '&incharge_id=' + incharge_id + '&option=delete_student_incharge';
        $.ajax({
            url: "student_list.do", data: DataString, type: "post",
            success: function (data)
            {
                $('#result_process').load('../../CommonJSP/alert_message.jsp?am_c=' + data);
                load_student_det();
            }
        });
    }
    function load_student_det() {
        if ($('#staff_id').val() !== "" & $('#incharge_id').val() !== "") {
            var DataString = 'staff_id=' + $('#staff_id').val() + '&incharge_id=' + $('#incharge_id').val() + '&option=load_student_incharge';
            $.ajax({
                url: "student_list.do", data: DataString, type: "post",
                success: function (data)
                {
                    $('#loadstudent_det').html(data);
                }
            });
        }
    }
</script>
<%    DBConnect db = new DBConnect();
    try {

        db.getConnection();

%>
<section class="content">
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        In-charge Filter 
                    </h2>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select class="form-control show-tick" name="dept_id"  id="dept_id" onchange="load_staff()" >

                                                <option value="">-- Select Department --</option>
                                                <%                                                    db.read("SELECT CONCAT('<option value=\"',md.department_id,'\" >',md.dept_name,'</option>') val FROM camps.master_department md WHERE md.status>0");
                                                    while (db.rs.next()) {
                                                        out.print(db.rs.getString("val"));
                                                    }
                                                %>
                                            </select><label class="form-label">Department</label>
                                        </div>                            
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select class="form-control show-tick" name="staff_id" id="staff_id" onchange="load_student_det()" >

                                                <option value="">-- Select Department --</option>

                                            </select><label class="form-label">Staff Name</label>
                                        </div> 

                                    </div>

                                </div>


                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">                             
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select class="form-control show-tick" name="incharge_id" id="incharge_id" onchange="load_student_det()" >

                                                <option value="">-- Select In-charge --</option>
                                                <%
                                                    db.read("SELECT CONCAT('<option value=\"',itm.itm_id,'\" >',itm.incharge_name,'</option>') val,itm.itm_id,itm.incharge_name FROM camps.incharge_type_master itm WHERE itm.incharge_group=1 AND itm.status>0");
                                                    while (db.rs.next()) {
                                                        out.print(db.rs.getString("val"));
                                                    }
                                                %>
                                            </select><label class="form-label">Incharge Name</label>
                                        </div>                            
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line"> 
                                            <input class="form-control" id="student_id" type="text">                                       
                                            <label class="form-label">Student Id</label>
                                        </div>                            
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <button class="btn btn-primary waves-effect" name="option" id="option" value='updatepersonal_edit' onclick='insert_student_incharge()' type="button">SUBMIT</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
            <div class="panel-group full-body" id="accordion_19" role="tablist" aria-multiselectable="true">
                <div class="panel panel-col-blue-grey">
                    <div class="panel-heading" role="tab" id="headingOne_19">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#collapseOne_19" aria-expanded="true" aria-controls="collapseOne_19">
                                <i class="material-icons">perm_contact_calendar</i> Student List </a>
                        </h4>
                    </div>
                    <div id="collapseOne_19" class="panel-collapse collapse in"  role="tabpanel" aria-labelledby="headingOne_19">
                        <div class="panel-body" id='loadstudent_det'>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<% } catch (Exception e) {
    } finally {
        db.closeConnection();
    }
%>


<%@include file="../../CommonJSP/pageFooter.jsp" %>